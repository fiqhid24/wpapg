<?php

/**
 * subdomain list classes
 *
 * @link       https://www.fiqhidayat.com
 * @since      1.0.0
 *
 * @package    Wpapg
 * @subpackage Wpapg/includes
 */

class Wpapg_Addon_Domain
{

	/**
	 * define inserted args
	 * @var [type]
	 */
	private $args = array();

	/**
	 * define error
	 * @var [type]
	 */
	private $error = array();

	/**
	 * data
	 * @var [type]
	 */
	public $data = array();

	/**
	 * column
	 * @var [type]
	 */
	private $column = array(
		'ID',
		'domain',
		'page_id',
		'status'
	);

	/**
	 * Construction
	 */
	public function __construct($domain = false)
	{

		global $wpdb;

		if (is_object($domain) && isset($domain->ID) || is_array($domain) && isset($domain['ID'])) :

			$this->data = is_object($domain) ? $domain : (object) $domain;

		elseif (is_int($domain) && $domain >= 1 || is_string($domain) && intval($domain) >= 1) :

			$domain_id = intval($domain);

			$d = wp_cache_get($domain_id, 'wpapg_addon_domain');

			if ($d) {
				$this->data = $d;
			} else {

				$table = $wpdb->prefix . WPAPG_ADDON_DOMAIN_TABLE;

				$d = $wpdb->get_row($wpdb->prepare("SELECT * FROM $table WHERE ID = %d LIMIT 1", intval($domain)));

				if ($d) {
					wp_cache_add($domain_id, $d, 'wpapg_addon_domain');
					$this->data = $d;
				}
			}

		endif;
	}

	/**
	 * getter
	 * @param  string $key [description]
	 * @return [type]      [description]
	 */
	public function __get($key)
	{

		$data = get_object_vars($this->data);

		if (array_key_exists($key, $data))
			return $data[$key];

		return NULL;
	}

	/**
	 * set domain
	 * @param string $string [description]
	 */
	public function set_domain($string)
	{

		if (empty($string)) :
			$this->error['domain'] = 'Domain must be defined';
		else :

			preg_match('/(?:[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?\.)+[a-z0-9][a-z0-9-]{0,61}[a-z]/', $string, $match);

			if (!isset($match[0])) :
				$this->error['domain'] = 'Invalid Domain Name';
			else :
				if (checkdnsrr($match[0], 'A')) :
					$this->args['domain'] = $match[0];
				else :
					$this->error['domain'] = 'Unregistered Domain Name';
				endif;
			endif;

		endif;

		return $this;
	}

	/**
	 * set page id
	 * @param int $page_id [description]
	 */
	public function set_page($page_id)
	{

		$page = get_post(intval($page_id));

		if (!empty($page_id) && $page) :
			$this->args['page_id'] = $page->ID;
		else :
			$this->args['page_id'] = NULL;
		endif;

		return $this;
	}

	/**
	 * set status
	 * @param string $status [description]
	 */
	public function set_status($status = 'unconnected')
	{

		if (in_array($status, ['connected', 'unconnected']))
			$this->args['status'] = $status;

		return $this;
	}

	/**
	 * insert to database
	 * @return [type] [description]
	 */
	public function insert()
	{

		global $wpdb;

		unset($this->args['status']);

		if (isset($this->error['domain']))
			return new WP_Error('error', $this->error['domain']);

		if (!isset($this->args['domain']))
			return new WP_Error('error', 'domain is required');

		$table = $wpdb->prefix . WPAPG_ADDON_DOMAIN_TABLE;
		$wpdb->insert($table, $this->args);

		return $wpdb->insert_id;
	}

	/**
	 * update;
	 * @return [type] [description]
	 */
	public function update()
	{

		global $wpdb;

		$table = $wpdb->prefix . WPAPG_ADDON_DOMAIN_TABLE;
		$updated = $wpdb->update($table, $this->args, get_object_vars($this->data));

		if (false === $updated)
			return new WP_Error('error', __('Update error', 'wpapg'));

		wp_cache_delete($this->ID, 'wpapg_addon_domain');

		return $this->ID;
	}

	/**
	 * delete
	 * @return [type] [description]
	 */
	public function delete()
	{

		global $wpdb;

		$table = $wpdb->prefix . WPAPG_ADDON_DOMAIN_TABLE;
		$deleted = $wpdb->delete($table, ['ID' => $this->ID]);

		if (false === $deleted)
			return new WP_Error('error', __('Update error', 'wpapg'));

		wp_cache_delete($this->ID, 'wpapg_addon_domain');
		return true;
	}

	/**
	 * check if current domain is whitlist domain
	 * @var [type]
	 */
	public static function check()
	{

		global $wpapg;

		if (!strpos(site_url(), wpapg_get_domain())) :

			$regex = '/([a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/';
			$host = parse_url(site_url(), PHP_URL_HOST);

			preg_match($regex, $host, $hasil);

			$domain = isset($hasil[0]) ? $hasil[0] : '';

			$addon_domain = wpapg_get_addon_domain_by('domain', $domain);

			if ($addon_domain) :
				$wpapg['addon_domain'] = $addon_domain;
			else :

				if (isset($wpapg['options']['siteurl'])) :
					header('location: ' . esc_url($wpapg['options']['siteurl']));
				endif;
			endif;
		endif;
	}

	/**
	 * set page on addon domian access
	 * @return [type] [description]
	 */
	public static function access()
	{
		global $wpapg, $wp_query, $post;

		if (isset($wpapg['addon_domain']) && $wpapg['addon_domain']) :

			$addon_domain = $wpapg['addon_domain'];
			$p = get_post(intval($addon_domain->page_id));

			if ($addon_domain->page_id != NULL && $p) :
				$wp_query->queried_object = $p;
				$wp_query->is_singular = 1;
				$wp_query->is_page = 1;
				$wp_query->queried_object_id = $p->ID;
				$wp_query->post_count = 1;
				$wp_query->current_post = -1;
				$wp_query->posts = array($p);
				$wp_query->post = $p;
				$post = $p;
			endif;
		endif;
	}

	/**
	 * domain validation check
	 * @var [type]
	 */
	public static function validation()
	{

		if (isset($_SERVER['REQUEST_URI']) && '/wpapg-addon-domain-check' == $_SERVER['REQUEST_URI']) :
			echo 'valid';
			exit;
		endif;

		return;
	}
}
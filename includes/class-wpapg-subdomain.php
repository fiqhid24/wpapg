<?php

/**
 * subdomain list classes
 *
 * @link       https://www.fiqhidayat.com
 * @since      1.0.0
 *
 * @package    Wpapg
 * @subpackage Wpapg/includes
 */

class Wpapg_Subdomain
{

    /**
     * define inserted args
     * @var [type]
     */
    private $args = array();

    /**
     * define error
     * @var [type]
     */
    private $error = array();

    /**
     * data
     * @var [type]
     */
    public $data = array();

    /**
     * Construction
     */
    public function __construct($subdomain = false)
    {

        global $wpdb;

        if (is_object($subdomain) && isset($subdomain->ID) || is_array($subdomain) && isset($subdomain['ID'])) :

            $this->data = is_object($subdomain) ? $subdomain : (object) $subdomain;

        elseif (is_int($subdomain) && $subdomain >= 1 || is_string($subdomain) && intval($subdomain) >= 1) :

            $subdomain_id = intval($subdomain);

            $d = wp_cache_get($subdomain_id, 'wpapg_subdomain');

            if ($d) {
                $this->data = $d;
            } else {

                $table = $wpdb->prefix . WPAPG_SUBDOMAIN_TABLE;

                $d = $wpdb->get_row($wpdb->prepare("SELECT * FROM $table WHERE ID = %d LIMIT 1", intval($subdomain_id)));

                if ($d) {
                    wp_cache_add($subdomain_id, $d, 'wpapg_subdomain');
                    $this->data = $d;
                }
            }

        endif;

        if ($this->data && isset($this->data->data) && $this->data->data) :
            $this->data->data = maybe_unserialize($this->data->data);
        endif;
    }

    /**
     * getter
     * @param  string $key [description]
     * @return [type]      [description]
     */
    public function __get($key)
    {

        $data = get_object_vars($this->data);

        if (array_key_exists($key, $data))
            return $data[$key];

        return NULL;
    }

    /**
     * set page id
     * @param int $page_id [description]
     */
    public function set_page($page_id)
    {

        $page = get_page(intval($page_id));

        if ($page) :
            $this->args['page_id'] = $page->ID;
        else :
            $this->error['page_id'] = 'Invalid Page';
        endif;

        return $this;
    }


    /**
     * set user
     * @param int $user_id [description]
     */
    public function set_user_id($user_id)
    {

        $user = get_user_by('ID', intval($user_id));

        if ($user) :
            $this->args['user_id'] = $user->ID;
        endif;

        return $this;
    }

    /**
     * set subdomain
     * @param string $string [description]
     */
    public function set_subdomain($string)
    {

        global $wpdb;

        if (!isset($this->args['page_id'])) return;

        $subdomain = sanitize_text_field($string);

        if (empty($subdomain))
            $this->error['subdomain'] = 'Subdomain must be defined';


        //makesure if subdomain only alfanumeric characters
        if (!ctype_alnum($subdomain))
            $this->error['subdomain'] = 'STOP !, Please insert lowercase alfanumeric only for subdomain';

        //makesure to remove unallowed characters
        $subdomain = preg_replace('/[^a-zA-Z0-9]/', '', $subdomain);
        $subdomain = strtolower($subdomain);

        $limit_min = wpapg_get_option('limit_min_subdomain', 5);
        $limit_max = wpapg_get_option('limit_max_subdomain', 20);

        //check if subdomain equal to limit charater
        if (strlen($subdomain) < $limit_min || strlen($subdomain) > $limit_max)
            $this->error['subdomain'] = 'subdomain length must be equal between ' . $limit_min . ' and ' . $limit_max . ' Character';

        $table = $wpdb->prefix . WPAPG_SUBDOMAIN_TABLE;
        $s = $wpdb->get_row($wpdb->prepare("SELECT * FROM $table WHERE subdomain = %s AND page_id = %s LIMIT 1", sanitize_text_field($subdomain), intval($this->args['page_id'])));

        if ($s)
            $this->error['subdomain'] = 'Subdomain "' . $subdomain . '" unavailable, please choose another subdomain';

        $this->args['subdomain'] = $subdomain;

        return $this;
    }

    /**
     * set status
     * @param string $status [description]
     */
    public function set_data($value = array())
    {

        if (!isset($this->args['page_id']) || empty($this->args['page_id'])) return $this;

        $shortcode = get_post_meta($this->args['page_id'], 'wpapg_shortcode', true);

        $data = array();
        foreach ((array) $shortcode as $key => $val) :

            if (!isset($value[$key])) continue;

            if ($val['type'] == 'image' && is_array($value[$key]) && isset($value[$key]['tmp_name'])) :

                $upload = wp_upload_bits($value[$key]['name'], null, file_get_contents($value[$key]['tmp_name']));

                if (is_wp_error($upload)) :
                    $this->error['data'] = 'Upload error, Please try again or contact administrator';
                    break;
                else :
                    $data[$key] = array(
                        'label' => $val['label'],
                        'value' => isset($upload['url']) ? esc_url_raw($upload['url']) : '',
                    );
                endif;
            else :
                $v = is_array($value[$key]) ? implode(',', $value[$key]) : $value[$key];

                /**
                 * make sure all input not contain harmfull code
                 * @var string
                 */
                $regex = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,6}(\/\S*)?/";
                if (preg_match($regex, $v, $url)) :
                    $clean_url = esc_url_raw($url[0]);
                    $v = preg_replace($regex, $clean_url, $v);

                    $data[$key] = array(
                        'label' => $val['label'],
                        'value' => $v,
                    );
                else :
                    $data[$key] = array(
                        'label' => $val['label'],
                        'value' => sanitize_text_field($v),
                    );
                endif;

            endif;

        endforeach;

        $this->args['data'] = maybe_serialize($data);

        return $this;
    }

    /**
     * set status
     * @param string $status [description]
     */
    public function set_status($status = true)
    {

        $status = $status ? 'active' : 'blocked';
        $this->args['status'] = $status;

        return $this;
    }

    /**
     * insert to database
     * @return [type] [description]
     */
    public function insert()
    {

        global $wpdb;

        unset($this->args['status']);

        if (isset($this->args['user_id']) && $user = get_userdata($this->args['user_id'])) :
            if (!in_array('administrator', $user->roles)) {
                $this->args['subdomain'] = $user->user_login;
            }
        endif;

        if (isset($this->error['page_id']))
            return new WP_Error('error', $this->error['page_id']);

        if (!isset($this->args['subdomain']))
            return new WP_Error('error', 'subdomain is required');

        if (isset($this->error['subdomain']))
            return new WP_Error('error', $this->error['subdomain']);

        if (isset($this->error['data']))
            return new WP_Error('error', $this->error['data']);

        $table = $wpdb->prefix . WPAPG_SUBDOMAIN_TABLE;
        $wpdb->insert($table, $this->args);

        return $wpdb->insert_id;
    }

    /**
     * update;
     * @return [type] [description]
     */
    public function update()
    {

        global $wpdb;

        if (isset($this->args['user_id']) && $user = get_userdata($this->args['user_id'])) :
            if (!in_array('administrator', $user->roles)) {
                $this->args['subdomain'] = $user->user_login;
            }
        endif;

        $this->args['updated_at'] = date('Y-m-d H:i:s', time());

        if (isset($this->error['data']))
            return new WP_Error('error', $this->error['data']);

        $table = $wpdb->prefix . WPAPG_SUBDOMAIN_TABLE;
        $updated = $wpdb->update($table, $this->args, ['ID' => $this->ID]);

        if (false === $updated)
            return new WP_Error('error', __('Update error', 'wpapg'));

        wp_cache_delete($this->ID, 'wpapg_subdomain');

        return $this->ID;
    }

    /**
     * delete
     * @return [type] [description]
     */
    public function delete()
    {

        global $wpdb;

        $table = $wpdb->prefix . WPAPG_SUBDOMAIN_TABLE;
        $deleted = $wpdb->delete($table, ['ID' => $this->ID]);

        if (false === $deleted)
            return new WP_Error('error', __('Delete error', 'wpapg'));

        wp_cache_delete($this->ID, 'wpapg_subdomain');

        return true;
    }

    /**
     * check if current domain is whitlist domain
     * @var [type]
     */
    public static function check_subdomain($string, $page_id = 0)
    {
        global $wpdb;

        $subdomain = sanitize_text_field($string);

        if (empty($subdomain))
            return new WP_Error('error', 'Subdomain must be defined');


        //makesure if subdomain only alfanumeric characters
        if (!ctype_alnum($subdomain))
            return new WP_Error('error', 'STOP !, Please insert lowercase alfanumeric only for subdomain');

        //makesure to remove unallowed characters
        $subdomain = preg_replace('/[^a-zA-Z0-9]/', '', $subdomain);
        $subdomain = strtolower($subdomain);

        $limit_min = wpapg_get_option('limit_min_subdomain', 5);
        $limit_max = wpapg_get_option('limit_max_subdomain', 20);

        //check if subdomain equal to limit charater
        if (strlen($subdomain) < $limit_min || strlen($subdomain) > $limit_max)
            return new WP_Error('error', 'subdomain length must be equal between ' . $limit_min . ' and ' . $limit_max . ' Character');

        //check if subdomain already in use by username
        if (username_exists($subdomain))
            return new WP_Error('error', 'Subdomain "' . $subdomain . '" unavailable, please choose another subdomain');

        //check if username or subdomain already in use
        $table = $wpdb->prefix . WPAPG_SUBDOMAIN_TABLE;
        $s = $wpdb->get_row($wpdb->prepare("SELECT * FROM $table WHERE subdomain = %s AND page_id = %s LIMIT 1", $subdomain, $page_id));

        if ($s)
            return new WP_Error('error', 'Subdomain "' . $subdomain . '" unavailable, please choose another subdomain');

        return $subdomain;
    }

    /**
     * set shortcode
     */
    public static function set_shortcode()
    {
        global $wpapg, $wp_query;

        if (is_admin()) return;

        $global_shortcode_default = get_option('wpapg_global_shortcode');
        $page_shortcode_default = get_post_meta(get_the_ID(), 'wpapg_shortcode', true);

        $shortcode_default = wp_parse_args($global_shortcode_default, $page_shortcode_default);

        $wpapg['shortcode_default'] = $shortcode_default;
        $wpapg['shortcode'] = $wpapg['shortcode_default'];

        // $enable = get_post_meta(get_the_ID(), 'wpapg_shortcode_enable', true);
        // if( !$enable ) return;

        $subdomain = wpapg_get_current_subdomain();

        if (!$subdomain) return;

        $global_shortcode = get_option('wpapg_global_shortcode_' . $subdomain);

        if ($global_shortcode) :
            $wpapg['shortcode'] = wp_parse_args($global_shortcode, $wpapg['shortcode']);
        endif;

        $args = array(
            'subdomain' => $subdomain,
            'page_id' => get_the_ID(),
        );

        $query = new Wpapg_Subdomain_Query($args);

        $sd = $query->results();

        if (!isset($sd[0])) return;

        if ($sd[0]->status == 'blocked') :
            header('location: ' . esc_url(wpapg_get_page_permalink(get_the_ID())));
            exit;
        endif;

        $d = $global_shortcode ? wp_parse_args($global_shortcode, $sd[0]->data) : $sd[0]->data;

        $wpapg['shortcode'] = wp_parse_args($d, $wpapg['shortcode']);

        return;
    }

    /**
     * set locker for link affiliate cookie and pixel
     */
    public static function set_locker()
    {
        global $wpapg;

        $link_affiliates = array();
        $pixel_ids = array();
        foreach ((array)$wpapg['shortcode_default'] as $key => $val) :
            if ($key == 'link_affiliate' || $val['type'] == 'link_affiliate') :
                $link_affiliates[] = isset($wpapg['shortcode'][$key]['value']) && $wpapg['shortcode'][$key]['value'] ? $wpapg['shortcode'][$key]['value'] : $val['value'];
            endif;

            if ($val['type'] == 'fb_pixel_id') :
                $pixel_ids[] = isset($wpapg['shortcode'][$key]['value']) && $wpapg['shortcode'][$key]['value'] ? $wpapg['shortcode'][$key]['value'] : $val['value'];
            endif;
        endforeach;

        foreach ($link_affiliates as $link_affiliate) :
            $link_affiliate = esc_url($link_affiliate);

            if (is_ssl()) :
                $link_affiliate = str_replace('http://', 'https://', $link_affiliate);
            endif;

            ob_start();
?>
            <img width="1" height="1" style="opacity: 0;position:fixed;top:-10px;" src="<?php echo $link_affiliate; ?>" />
        <?php
            $content = ob_get_contents();
            ob_end_clean();

            echo $content;

        endforeach;

        if ($pixel_ids) :
            ob_start();
        ?>
            <!-- Facebook Pixel Code -->
            <script>
                ! function(f, b, e, v, n, t, s) {
                    if (f.fbq) return;
                    n = f.fbq = function() {
                        n.callMethod ?
                            n.callMethod.apply(n, arguments) : n.queue.push(arguments)
                    };
                    if (!f._fbq) f._fbq = n;
                    n.push = n;
                    n.loaded = !0;
                    n.version = '2.0';
                    n.queue = [];
                    t = b.createElement(e);
                    t.async = !0;
                    t.src = v;
                    s = b.getElementsByTagName(e)[0];
                    s.parentNode.insertBefore(t, s)
                }(window, document, 'script',
                    'https://connect.facebook.net/id_ID/fbevents.js');
                <?php foreach ($pixel_ids as $pixel_id) : ?>
                    <?php $pixel_id = sanitize_text_field($pixel_id); ?>
                    fbq('init', '<?php echo $pixel_id; ?>');
                <?php endforeach; ?>
                fbq('track', 'PageView');
            </script>
            <noscript>
                <?php foreach ($pixel_ids as $pixel_id) : ?>
                    <?php $pixel_id = sanitize_text_field($pixel_id); ?>
                    <img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=<?php echo $pixel_id; ?>&ev=PageView&noscript=1" />
                <?php endforeach; ?>
            </noscript>
            <!-- End Facebook Pixel Code -->
<?php
            $js_pixel = ob_get_contents();
            ob_end_clean();
            echo $js_pixel;
        endif;
    }

    /**
     * get active subdomain pages
     * @return [type] [description]
     */
    public static function active_pages()
    {


        $active_pages = get_transient('wpapg_active_pages');

        if (!$active_pages) :

            $args = array(
                'post_status' => 'publish',
                'post_type' => 'page',
                'field' => 'id,title',
                'posts_per_page' => 100,
                'meta_query' => array(
                    'relation' => 'AND',
                    // array(
                    //     'key'     => 'wpapg_shortcode',
                    //     'compare' => 'EXISTS',
                    // ),
                    array(
                        'key'     => 'wpapg_shortcode_enable',
                        'value'   => 1,
                        'compare' => '=',
                    ),
                ),
            );

            $pages = get_posts($args);

            foreach ((array)$pages as $page) :
                $active_pages[] = array(
                    'ID' => $page->ID,
                    'title' => $page->post_title,
                );
            endforeach;

            set_transient('wpapg_active_pages', $active_pages, 24 * HOUR_IN_SECONDS);

        endif;

        return $active_pages;
    }
}

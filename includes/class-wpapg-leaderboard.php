<?php

/**
 * subdomain list classes
 *
 * @link       https://www.fiqhidayat.com
 * @since      1.0.0
 *
 * @package    Wpapg
 * @subpackage Wpapg/includes
 */

class Wpapg_Leaderboard {

	/**
	 * define inserted args
	 * @var [type]
	 */
	private $args = array();

	/**
	 * define error
	 * @var [type]
	 */
	private $error = array();

	/**
	 * data
	 * @var [type]
	 */
	public $data = array();

	/**
	 * Construction
	 */
	public function __construct($leaderboard = false){

		global $wpdb;

		if( is_object($leaderboard) && isset($leaderboard->ID) || is_array($leaderboard) && isset($leaderboard['ID']) ):

			$this->data = is_object($leaderboard) ? $leaderboard : (object) $leaderboard;

        elseif( is_int($leaderboard) && $leaderboard >= 1 || is_string($leaderboard) && intval($leaderboard) >= 1 ):

			$leaderboard_id = intval($leaderboard);

			$d = wp_cache_get($leaderboard_id, 'wpapg_leaderboard');

			if( $d ){
				$this->data = $d;
			}else{

				$table = $wpdb->prefix.WPAPG_LEADERBOARD_TABLE;

				$d = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM $table WHERE ID = %d LIMIT 1", intval($leaderboard_id) ) );

				if( $d ){
					wp_cache_add($leaderboard_id, $d, 'wpapg_leaderboard');
					$this->data = $d;
				}
			}

		endif;

	}

	/**
	 * getter
	 * @param  string $key [description]
	 * @return [type]      [description]
	 */
	public function __get($key){

		$data = get_object_vars($this->data);

		if (array_key_exists($key, $data))
		    return $data[$key];

		return NULL;
	}

	/**
	 * set page id
	 * @param int $page_id [description]
	 */
	public function set_page($page_id){

		$page = get_page(intval($page_id));

		if( $page ):
		    $this->args['page_id'] = $page->ID;
		else:
			$this->error['page_id'] = 'Invalid Page';
		endif;

		return $this;
	}


	/**
	 * set user
	 * @param int $user_id [description]
	 */
	public function set_full_name($string){

		if( $string ):
		    $this->args['full_name'] = sanitize_text_field($string);
		else:
			$this->error['full_name'] = 'Full Name is required';
		endif;

		return $this;
	}

	/**
	 * set subdomain
	 * @param string $string [description]
	 */
	public function set_teamup_name($string){

		if( isset($this->args['full_name']) ):
			$this->args['teamup_name'] = $string ? sanitize_text_field($string) : $this->args['full_name'];
		endif;

		return $this;
	}

	/**
	 * set status
	 * @param string $status [description]
	 */
	public function set_photo($photo){

		$this->args['photo'] = esc_url($photo);

		return $this;
	}

	/**
	 * set status
	 * @param string $status [description]
	 */
	public function set_value($value = false){

		$this->args['value'] = intval($value);

		return $this;
	}

	/**
	 * insert to database
	 * @return [type] [description]
	 */
	public function insert(){

		global $wpdb;

		if( isset($this->error['page_id']) )
			return new WP_Error( 'error', $this->error['page_id'] );

		if( isset($this->error['full_name']) )
			return new WP_Error( 'error', $this->error['full_name'] );

		$table = $wpdb->prefix.WPAPG_LEADERBOARD_TABLE;
		$wpdb->insert($table,$this->args);

		return $wpdb->insert_id;
	}

	/**
	 * update;
	 * @return [type] [description]
	 */
	public function update(){

		global $wpdb;

		$table = $wpdb->prefix.WPAPG_LEADERBOARD_TABLE;
		$updated = $wpdb->update($table,$this->args, ['ID' => $this->ID]);

		if( false === $updated )
			return new WP_Error( 'error', __( 'Update error', 'wpapg'  ) );

		wp_cache_delete($this->ID, 'wpapg_leaderboard');

		return $this->ID;
	}

	/**
	 * delete
	 * @return [type] [description]
	 */
	public function delete(){

		global $wpdb;

		$table = $wpdb->prefix.WPAPG_LEADERBOARD_TABLE;
		$deleted = $wpdb->delete($table,['ID' => $this->ID]);

		if( false === $deleted )
			return new WP_Error( 'error', __( 'Delete error', 'wpapg'  ) );

		wp_cache_delete($this->ID, 'wpapg_leaderboard');

		return true;
	}

	/**
	 * set shortcode;
	 */
	public static function set_shortcode(){
		global $wpapg, $post;

		if( !$post )return;

		$leaderboard_enable = get_post_meta($post->ID, 'wpapg_leaderboard_enable', true);

		if( $leaderboard_enable != 1 ) return;

		$leaderboard = get_post_meta($post->ID, 'wpapg_leaderboard_cache', true);

		$wpapg['leaderboard'] = $leaderboard ? $leaderboard : array();

		return;
	}

}

<?php

/**
 * query classes
 *
 * @link       https://www.fiqhidayat.com
 * @since      1.0.0
 *
 * @package    Wpapg
 * @subpackage Wpapg/includes
 */

class Wpapg_Query {

    /**
	 * found result
	 * @var [type]
	 */
	protected $found = 0;

	/**
	 * count result
	 * @var [type]
	 */
	protected $count = 0;

	/**
	 * database table
	 * @var [type]
	 */
	protected $table;

	/**
	 * wpdb
	 * @var [type]
	 */
	protected $wpdb;

	/**
	 * sql query
	 * @var [type]
	 */
	protected $query;

	/**
	 * prepare statment
	 * @var [type]
	 */
	protected $prepare;

    /**
     * default argument
     * @var [type]
     */
    protected $args = array(
        'page'   => 1,
        'offset' => 0,
        'limit'  => 20,
    );

    /**
     * construction
     */
    public function __construct($args=array()){

        if( isset($args['page']) && $args['page'] > 1 ):
            $limit = isset($args['limit']) && $args['limit'] ? $args['limit'] : $this->args['limit'];
            $args['offset'] = ($args['page'] - 1 ) * $limit;
        endif;

        $this->args = wp_parse_args($args, $this->args);
        $this->init();
    }

    /**
     * initialisation
     * @return [type] [description]
     */
    private function init(){

        global $wpdb;

        $this->wpdb = $wpdb;
		$this->table = $this->wpdb->prefix;
    }

    /**
     * count row by current query
     * @return [type] [description]
     */
    public function count(){
        return $this->count;
    }

    /**
	 * count total row result bu current query
	 * @return [type] [description]
	 */
	public function found(){
		return $this->found;
	}

}

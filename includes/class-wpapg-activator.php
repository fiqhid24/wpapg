<?php

/**
 * Fired during plugin activation
 *
 * @link       https://www.fiqhidayat.com
 * @since      1.0.0
 *
 * @package    Wpapg
 * @subpackage Wpapg/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Wpapg
 * @subpackage Wpapg/includes
 * @author     Fiq Hidayat <taufik@fiqhidayat.com>
 */
class Wpapg_Activator {

	/**
	 * create adodn domain tabel on plugin activation
	 * @return [type] [description]
	 */
	public static function create_addon_domain_table() {

		global $wpdb;

		$table = $wpdb->prefix . WPAPG_ADDON_DOMAIN_TABLE;
        $sql = "CREATE TABLE IF NOT EXISTS {$table} (
          ID int(11) NOT NULL AUTO_INCREMENT,
		  domain varchar(255) NOT NULL,
		  page_id int(11) NULL,
		  status enum('connected', 'unconnected') DEFAULT 'unconnected' NOT NULL,
          PRIMARY KEY (ID)
        )";
        $wpdb->query($sql);

	}

	/**
	 * create  dubomain data tabel on plugin activation
	 * @return [type] [description]
	 */
	public static function create_subdomain_table() {

		global $wpdb;

		$table = $wpdb->prefix . WPAPG_SUBDOMAIN_TABLE;
        $sql = "CREATE TABLE IF NOT EXISTS {$table} (
          ID int(11) NOT NULL AUTO_INCREMENT,
		  subdomain varchar(255) NOT NULL,
		  user_id int(11) NULL,
		  created_at datetime DEFAULT CURRENT_TIMESTAMP NOT NULL,
		  updated_at datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
		  page_id int(11) NOT NULL,
		  data longtext NULL,
		  status enum('active', 'blocked') DEFAULT 'active' NOT NULL,
          PRIMARY KEY (ID)
        )";
        $wpdb->query($sql);

	}

	public static function create_leaderboard_table() {

		global $wpdb;

		$table = $wpdb->prefix . WPAPG_LEADERBOARD_TABLE;
        $sql = "CREATE TABLE IF NOT EXISTS {$table} (
          ID int(11) NOT NULL AUTO_INCREMENT,
		  page_id int(11) NOT NULL,
		  full_name varchar(255) NOT NULL,
		  teamup_name varchar(255) NOT NULL,
		  photo text NULL,
		  value int(11) DEFAULT 0 NOT NULL,
          PRIMARY KEY (ID)
        )";
        $wpdb->query($sql);

	}

}

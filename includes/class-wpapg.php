<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://www.fiqhidayat.com
 * @since      1.0.0
 *
 * @package    Wpapg
 * @subpackage Wpapg/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Wpapg
 * @subpackage Wpapg/includes
 * @author     Fiq Hidayat <taufik@fiqhidayat.com>
 */
class Wpapg
{

	private static $url = 'https://www.affiliatepagegenerator.com/check/license';

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Wpapg_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct()
	{
		if (defined('WPAPG_VERSION')) {
			$this->version = WPAPG_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'wpapg';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();
		add_action('after_setup_theme', [$this, 'elementor_dependencies']);
	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Wpapg_Loader. Orchestrates the hooks of the plugin.
	 * - Wpapg_i18n. Defines internationalization functionality.
	 * - Wpapg_Admin. Defines all hooks for the admin area.
	 * - Wpapg_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies()
	{

		require_once WPAPG_DIR . 'includes/class-wpapg-loader.php';
		require_once WPAPG_DIR . 'includes/class-wpapg-i18n.php';
		require_once WPAPG_DIR . 'includes/class-wpapg-query.php';
		require_once WPAPG_DIR . 'includes/class-wpapg-addon-domain.php';
		require_once WPAPG_DIR . 'includes/class-wpapg-addon-domain-query.php';
		require_once WPAPG_DIR . 'includes/class-wpapg-subdomain.php';
		require_once WPAPG_DIR . 'includes/class-wpapg-subdomain-query.php';
		require_once WPAPG_DIR . 'includes/class-wpapg-leaderboard.php';
		require_once WPAPG_DIR . 'includes/class-wpapg-leaderboard-query.php';


		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once WPAPG_DIR . 'admin/class-wpapg-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once WPAPG_DIR . 'public/class-wpapg-public.php';

		$this->loader = new Wpapg_Loader();
	}

	public function elementor_dependencies()
	{

		if (did_action('elementor/loaded')) :
			require_once WPAPG_DIR . 'includes/class-wpapg-elementor-template.php';
		endif;
	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Wpapg_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale()
	{

		$plugin_i18n = new Wpapg_i18n();

		$this->loader->add_action('plugins_loaded', $plugin_i18n, 'load_plugin_textdomain');
	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks()
	{

		$plugin_admin = new Wpapg_Admin($this->get_plugin_name(), $this->get_version());

		$this->loader->add_action('admin_enqueue_scripts', $plugin_admin, 'enqueue_styles');
		$this->loader->add_action('admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts');
		$this->loader->add_action('admin_menu',            $plugin_admin, 'admin_menu');
		$this->loader->add_action('wp_ajax_addon_domain', $plugin_admin, 'ajax_addon_domain');
		$this->loader->add_action('wp_ajax_check_addon_domain', $plugin_admin, 'ajax_check_addon_domain');
		$this->loader->add_action('wp_ajax_get_addon_domain', $plugin_admin, 'ajax_get_addon_domain');
		$this->loader->add_action('add_meta_boxes', $plugin_admin, 'add_post_meta');
		$this->loader->add_action('save_post', $plugin_admin, 'post_meta_save', 1, 2);
		$this->loader->add_filter('manage_edit-page_columns',  $plugin_admin, 'page_columns');
		$this->loader->add_action('manage_page_posts_custom_column', $plugin_admin, 'manage_page_columns', 10, 2);
		$this->loader->add_action('wp_ajax_search_user', $plugin_admin, 'ajax_search_user');
		$this->loader->add_action('after_setup_theme', $plugin_admin, 'after_setup_theme');
		$this->loader->add_action('admin_notices', $plugin_admin, 'notice');
		$this->loader->add_action('admin_init', $plugin_admin, 'init');
		$this->loader->add_action('wp_ajax_delete_leaderboard', $plugin_admin, 'ajax_delete_leaderboard');
		$this->loader->add_action('wp_ajax_wpapg_import_template', $plugin_admin, 'ajax_import_template');
		$this->loader->add_action('admin_footer', $plugin_admin, 'footer');
	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks()
	{

		$plugin_public = new Wpapg_Public($this->get_plugin_name(), $this->get_version());

		$this->loader->add_action('wp_head', $plugin_public, 'enqueue_styles');
		$this->loader->add_action('wp_footer', $plugin_public, 'enqueue_scripts');
		$this->loader->add_action('init', $plugin_public, 'init', 10, 0);
		$this->loader->add_action('wp', $plugin_public, 'wp', 10);
		$this->loader->add_action('template_redirect', $plugin_public, 'template_redirect');
		$this->loader->add_action('wp_head', $plugin_public, 'wp_head');
		$this->loader->add_action('wp_ajax_create-subdomain', $plugin_public, 'ajax_create_subdomain');
		$this->loader->add_action('wp_ajax_nopriv_create-subdomain', $plugin_public, 'ajax_create_subdomain');
		$this->loader->add_action('wp_ajax_check-subdomain', $plugin_public, 'ajax_check_subdomain');
		$this->loader->add_action('wp_ajax_nopriv_check-subdomain', $plugin_public, 'ajax_check_subdomain');
		$this->loader->add_action('wp_ajax_search-subdomain', $plugin_public, 'ajax_search_subdomain');
		$this->loader->add_action('wp_ajax_nopriv_search-subdomain', $plugin_public, 'ajax_search_subdomain');
		$this->loader->add_action('wp_ajax_check-link-affiliate', $plugin_public, 'ajax_check_link_affiliate');
		$this->loader->add_action('wp_ajax_nopriv_check-link-affiliate', $plugin_public, 'ajax_check_link_affiliate');
		$this->loader->add_filter('the_content', $plugin_public, 'the_content', 99);
	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run()
	{
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name()
	{
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Wpapg_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader()
	{
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version()
	{
		return $this->version;
	}

	public static function lislis()
	{

		$transient = get_transient('wpapglislis');

		if ($transient != 'active') :
			$data = get_option('wpapglislisdata');

			if (!$data) return false;

			$result = self::curl(sanitize_text_field($data['code']), sanitize_email($data['email']));

			if ($result == 'error') :
				set_transient('wpapglislis', 'active', 3 * DAY_IN_SECONDS);
			elseif (isset($result['result']) && $result['result'] == 1) :
				set_transient('wpapglislis', 'active', 30 * DAY_IN_SECONDS);
			else :
				$error = sanitize_text_field($result['content']);
				set_transient('wpapglislis', $error, 30 * DAY_IN_SECONDS);
			endif;
			$transient = get_transient('wpapglislis');
		endif;

		return $transient;
	}

	public static function check_curl()
	{

		if (!function_exists("curl_init")) return true;

		return false;
	}

	public static function curl($license, $email)
	{

		$domain = preg_replace('(^https?://)', '', site_url());

		$fields = array(
			'key'     => $license,
			'email'   => $email,
			'string'  => $domain,
		);

		$response = wp_remote_post(self::$url, array(
			'method'   => 'POST',
			'timeout'  => 10,
			'redirection' => 5,
			'httpversion' => '1.0',
			'blocking'    => true,
			'headers'     => array(),
			'body'        => $fields,
			'cookies'     => array()
		));

		if (is_wp_error($response)) {
			return 'error';
		}

		return json_decode($response['body'], true);
	}
}
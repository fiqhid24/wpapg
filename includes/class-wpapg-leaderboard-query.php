<?php

/**
 * domain query classes
 *
 * @link       https://www.fiqhidayat.com
 * @since      1.0.0
 *
 * @package    Wpapg
 * @subpackage Wpapg/includes
 */

class Wpapg_Leaderboard_Query extends Wpapg_Query {


	/**
	 * construction
	 * @param array $this->args [description]
	 */
	public function __construct($args=array()){

		parent::__construct($args);
		$this->query();
		$this->table = $this->table.WPAPG_LEADERBOARD_TABLE;

	}

	/**
	 * parse query
	 * @return [type] [description]
	 */
	private function query(){

		$where = array();

		if( isset($this->args['id']) && $this->args['id'] ):
			if( is_array($this->args['ids']) ):
				$ids = implode('","', $this->args['id']);
				$where[] = 'ID IN("'.$ids.'")';
			else:
				$where[] = 'ID = "'.intval($this->args['id']).'"';
			endif;
		endif;

		if( isset($this->args['page_id']) && $this->args['page_id'] ):
			if( is_array($this->args['page_id']) ):
				$page_ids = implode('","', $this->args['page_id']);
				$where[] = 'page_id IN("'.$page_ids.'")';
			else:
				$where[] = 'page_id = "'.intval($this->args['page_id']).'"';
			endif;
		endif;

		if( isset($this->args['s']) && $this->args['s'] ):
			$where[] = 'full_name LIKE "%'.sanitize_text_field($this->args['s']).'%" OR teamup_name LIKE "%'.sanitize_text_field($this->args['s']).'%"';
		endif;

		if( count($where) >= 1 ){
			$this->query .= ' WHERE '.implode(' AND ', $where );
		}

		return $this;
	}

	/**
	 * results
	 * @return [type] [description]
	 */
	public function results(){

		$query = 'SELECT * FROM '.$this->table.$this->query;

		$query .= ' ORDER BY ID DESC LIMIT '.$this->args['limit'].' OFFSET '.$this->args['offset'];

		$result = $this->wpdb->get_results($query);

		$this->count = $this->wpdb->num_rows;
		$this->found = $this->wpdb->get_var('SELECT count(*) FROM '.$this->table.$this->query);

		return array_map('wpapg_get_leaderboard', $result);
	}

	/**
	 * get list
	 * @return [type] [description]
	 */
	public function get_list(){

		$query = 'SELECT * FROM '.$this->table.$this->query;

		$query .= ' ORDER BY value DESC LIMIT '.$this->args['limit'].' OFFSET '.$this->args['offset'];

		$result = $this->wpdb->get_results($query);

		$this->count = $this->wpdb->num_rows;
		$this->found = $this->wpdb->get_var('SELECT count(*) FROM '.$this->table.$this->query);

		return array_map('wpapg_get_leaderboard', $result);
	}

	public function get_leaderboard(){

		$query = 'SELECT ID, teamup_name as name, photo, SUM(value) as value FROM '.$this->table.$this->query.' AND value != 0';

		$query .= ' GROUP BY teamup_name ORDER BY value DESC LIMIT '.$this->args['limit'].' OFFSET '.$this->args['offset'];

		$result = $this->wpdb->get_results($query);

		$this->count = $this->wpdb->num_rows;
		$this->found = $this->wpdb->get_var('SELECT count(*) FROM '.$this->table.$this->query);

		return array_map('wpapg_get_leaderboard', $result);
	}

}

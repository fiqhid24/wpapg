<?php

use Elementor\Core\Base\Document;
use Elementor\DB;
use Elementor\Core\Settings\Manager as SettingsManager;
use Elementor\Core\Settings\Page\Model;
use Elementor\Editor;
use Elementor\Modules\Library\Documents\Library_Document;
use Elementor\Plugin;
use Elementor\Settings;
use Elementor\Utils;

if (!defined('ABSPATH')) {
	exit; // Exit if accessed directly.
}

/**
 * Elementor template library local source.
 *
 * Elementor template library local source handler class is responsible for
 * handling local Elementor templates saved by the user locally on his site.
 *
 * @since 1.0.0
 */
class Wpapg_Elementor_Template extends \Elementor\TemplateLibrary\Source_Base
{

	/**
	 * Get remote template ID.
	 *
	 * Retrieve the remote template ID.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string The remote template ID.
	 */
	public function get_id()
	{
		return 'wpapg';
	}

	/**
	 * Get remote template title.
	 *
	 * Retrieve the remote template title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string The remote template title.
	 */
	public function get_title()
	{
		return __('Wpapg Template', 'wpapg');
	}

	/**
	 * Register remote template data.
	 *
	 * Used to register custom template data like a post type, a taxonomy or any
	 * other data.
	 *
	 * @since 1.0.0
	 * @access public
	 */
	public function register_data()
	{
	}
	public function get_items($args = [])
	{
	}
	public function get_item($template_id)
	{
	}

	/**
	 * source template
	 * @param  array  $args [description]
	 * @return [type]       [description]
	 */
	public function get_data(array $args)
	{
		$templates = array();

		$templates[] = array(
			'id' => 'bonus-page-1',
			'title' => 'Bonus Page 1',
			'thumbnail' => 'https://i0.wp.com/www.affiliatepagegenerator.com/wp-content/uploads/2019/07/demo-bonus-page.png',
			'source' => WPAPG_DIR . '/admin/elementor-template/elementor-bonus-page.json',
			'preview' => 'https://www.affiliatepagegenerator.com/demo-bonus-page/',
			'meta' => array(
				'wpapg_shortcode_enable' => 1,
				'wpapg_shortcode' => array(
					'link_affiliate' => array(
						'label' => 'Link Affiliate',
						'type' => 'link_affiliate',
						'value' => 'https://www.affiliatepagegenerator.com/',
					),
					'kupon' => array(
						'label' => 'Kode Kupon',
						'type' => 'text',
						'value' => 'PRELAUNCH',
					)
				)
			),
			'global' => array(
				'name' => array(
					'label' => 'Nama Lengkap',
					'type' => 'text',
					'value' => 'Jhon Doe',
				),
				'photo' => array(
					'label' => 'Photo',
					'type' => 'image',
					'value' => 'https://i.stack.imgur.com/l60Hf.png',
				),
				'messenger' => array(
					'label' => 'FB Messenger Link',
					'type' => 'text',
					'value' => 'https://www.messenger.com/',
				)
			)
		);

		$templates[] = array(
			'id' => 'jv-page-1',
			'title' => 'JV Page & Bonus Page Generator 1',
			'thumbnail' => 'https://i0.wp.com/www.affiliatepagegenerator.com/wp-content/uploads/2019/07/demo-jv-page.png',
			'source' => WPAPG_DIR . '/admin/elementor-template/elementor-jv-page.json',
			'preview' => 'https://www.affiliatepagegenerator.com/demo-jv-page-atau-bonus-page-generator/',
			'meta' => array(),
			'global' => array()
		);

		$templates[] = array(
			'id' => 'leaderboard-page-1',
			'title' => 'Leaderboard Page 1',
			'thumbnail' => 'https://i0.wp.com/www.affiliatepagegenerator.com/wp-content/uploads/2019/07/demo-leaderboard-page.png',
			'source' => WPAPG_DIR . '/admin/elementor-template/elementor-leaderboard-page.json',
			'preview' => 'https://www.affiliatepagegenerator.com/demo-leaderboard/',
			'meta' => array(
				'wpapg_leaderboard_enable' => 1,
			),
			'global' => array(),
		);



		return $templates;
	}
	public function delete_template($template_id)
	{
	}
	public function update_item($new_data)
	{
	}
	public function export_template($template_id)
	{
	}



	/**
	 * Import single template.
	 *
	 * Import template from a file to the database.
	 *
	 * @since 1.6.0
	 * @access private
	 *
	 * @param string $file_name File name.
	 *
	 * @return \WP_Error|int|array Local template array, or template ID, or
	 *                             `WP_Error`.
	 */
	public function import_single_template($post_id, $file_name, $global, $meta)
	{
		$data = json_decode(file_get_contents($file_name), true);

		if (empty($data)) {
			return new \WP_Error('file_error', 'Invalid File');
		}

		$content = $data['content'];

		if (!is_array($content)) {
			return new \WP_Error('file_error', 'Invalid File');
		}

		$content = $this->process_export_import_content($content, 'on_import');

		$page_settings = [];

		if (!empty($data['page_settings'])) {
			$page = new Model([
				'id' => 0,
				'settings' => $data['page_settings'],
			]);

			$page_settings_data = $this->process_element_export_import_content($page, 'on_import');

			if (!empty($page_settings_data['settings'])) {
				$page_settings = $page_settings_data['settings'];
			}
		}

		$new_post_id = $this->save_item([
			'content'          => $content,
			'title'            => $data['title'],
			'type'             => $data['type'],
			'page_settings'    => $page_settings,
			'post_id' => $post_id,
			'global'  => json_decode(stripslashes_deep($global), true),
			'meta'    => json_decode(stripslashes_deep($meta), true),
		]);

		return $new_post_id;
	}

	/**
	 * Save local template.
	 *
	 * Save new or update existing template on the database.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @param array $template_data Local template data.
	 *
	 * @return \WP_Error|int The ID of the saved/updated template, `WP_Error` otherwise.
	 */
	public function save_item($template_data)
	{
		$type = Plugin::$instance->documents->get_document_type($template_data['type'], false);

		if (!$type) {
			return new \WP_Error('save_error', sprintf('Invalid template type "%s".', $template_data['type']));
		}

		$post_data = array(
			'post_title' => !empty($template_data['title']) ? $template_data['title'] : __('(no title)', 'wpapg'),
			'post_status' => current_user_can('publish_posts') ? 'publish' : 'pending',
			'post_type' => 'page',
			'page_template' => 'elementor_canvas',
		);

		if ($template_data['post_id'] && $template_data['post_id'] > 0) :
			$post_data['ID'] = intval($template_data['post_id']);
			$post_data['post_content'] = '';
			$post_data['post_name'] = sanitize_title($post_data['post_title']);
		endif;

		$post_id = wp_insert_post($post_data);

		if (is_wp_error($post_id)) {
			return $post_id;
		}

		Plugin::$instance->db->set_is_elementor_page($post_id);

		Plugin::$instance->db->save_editor($post_id, $template_data['content']);

		if (!empty($template_data['page_settings'])) {
			SettingsManager::get_settings_managers('page')->save_settings($template_data['page_settings'], $post_id);
		}

		foreach ((array) $template_data['meta'] as $key => $val) :
			update_post_meta($post_id, $key, $val);
		endforeach;

		if ($template_data['global']) :
			$old_global = get_option('wpapg_global_shortcode') ? get_option('wpapg_global_shortcode') : array();

			$new_global = wp_parse_args($old_global, $template_data['global']);

			update_option('wpapg_global_shortcode', $new_global);
		endif;

		return $post_id;
	}

	public function get_source()
	{

		$data = get_transient('wpapg_elementor_templates_list');

		if (empty($data)) :
			$url = 'https://www.affiliatepagegenerator.com/wp-json/wpapgserver/templates/elementor';

			$respons = wp_remote_get($url);

			if (is_wp_error($respons)) return false;

			$data = json_decode($respons['body'], true);

			set_transient('wpapg_elementor_templates_list', $data, 24 * HOUR_IN_SECONDS);
		endif;

		return $data;
	}

	public static function content()
	{

		$e = new Wpapg_Elementor_Template();
		$templates = $e->get_source();

		if (!$templates) :
			$e->get_data([]);
		endif;

		ob_start();
?>
<div class="wpapg-template">
    <div class="wpapg-template-box">
        <div class="wpapg-template-box-head">
            <h3><?php _e('Affiliate Page Generator Predesigned Template'); ?></h3>
            <div class="wpapgCloser">
                <span class="dashicons dashicons-no-alt"></span>
            </div>
        </div>
        <div class="wpapg-template-box-list">
            <?php foreach ((array)$templates as $template) : ?>
            <div class="wpapgTemplate">
                <img src="<?php echo $template['thumbnail']; ?>" />
                <div class="wpapgTemplate-title"><?php echo $template['title']; ?></div>
                <div class="wpapgTemplate-action">
                    <div class="wpapgImporter" data-source="<?php echo $template['source']; ?>"
                        data-meta='<?php echo json_encode($template['meta']); ?>'
                        data-global='<?php echo json_encode($template['global']); ?>'>
                        <span class="dashicons dashicons-arrow-down-alt"></span>IMPORT
                    </div>
                    <div class="wpapgPreview" data-preview="<?php echo $template['preview']; ?>">PREVIEW<span
                            class="dashicons dashicons-visibility"></span></div>
                </div>
            </div>
            <?php endforeach; ?>
            <div class="wpapg-template-box-list-loader">
                <svg width="77px" height="77px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100"
                    preserveAspectRatio="xMidYMid" style="background: none;">
                    <circle cx="50" cy="50" fill="none" stroke="#ffffff" stroke-width="10" r="35"
                        stroke-dasharray="164.93361431346415 56.97787143782138" transform="rotate(118.397 50 50)">
                        <animateTransform attributeName="transform" type="rotate" calcMode="linear"
                            values="0 50 50;360 50 50" keyTimes="0;1" dur="0.4s" begin="0s" repeatCount="indefinite">
                        </animateTransform>
                    </circle>
                </svg>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
function wpaphShowTemplateBox() {
    jQuery('.wpapg-template').show();
}

jQuery('.wpapgCloser').on('click', function() {
    jQuery('.wpapg-template').hide();
});

jQuery('.wpapgPreview').on('click', function() {
    let preview_link = jQuery(this).attr('data-preview');
    window.open(preview_link);
});

jQuery('.wpapgImporter').on('click', function() {

    jQuery('.wpapg-template-box-list-loader').show();

    let source = jQuery(this).attr('data-source');
    let globals = jQuery(this).attr('data-global');
    let meta = jQuery(this).attr('data-meta');
    let data = {
        source: source,
        globals: globals,
        meta: meta,
        post_id: < ? php echo get_the_ID(); ? > ,
        action : 'wpapg_import_template',
        nonce: '<?php echo wp_create_nonce('
        wpapg_nonce '); ?>',
    }
    jQuery.post(ajaxurl, data, function(result) {
        if (result == 'error') {
            alert('Error imported, please try again !');
        } else {
            window.location.href = result;
        }
    });
})
</script>

<script id="wpapg-classic-button" type="text/html">
<button class="page-title-action"
    onclick="wpaphShowTemplateBox();"><?php echo __('APG Predesigned Template', 'wpapg'); ?></button>
</script>
<script id="wpapg-guttenberg-button" type="text/html">
<button class="button button-primary button-large"
    onclick="wpaphShowTemplateBox();"><?php echo __('APG Predesigned Template', 'wpapg'); ?></button>
</script>
<?php
		$content = ob_get_contents();
		ob_end_clean();

		//echo $content;
	}
}
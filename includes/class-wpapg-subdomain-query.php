<?php

/**
 * domain query classes
 *
 * @link       https://www.fiqhidayat.com
 * @since      1.0.0
 *
 * @package    Wpapg
 * @subpackage Wpapg/includes
 */

class Wpapg_Subdomain_Query extends Wpapg_Query {


	/**
	 * construction
	 * @param array $this->args [description]
	 */
	public function __construct($args=array()){

		parent::__construct($args);
		$this->query();
		$this->table = $this->table.WPAPG_SUBDOMAIN_TABLE;

	}

	/**
	 * parse query
	 * @return [type] [description]
	 */
	private function query(){

		$where = array();

		if( isset($this->args['id']) && $this->args['id'] ):
			if( is_array($this->args['ids']) ):
				$ids = implode('","', $this->args['id']);
				$where[] = 'ID IN("'.$ids.'")';
			else:
				$where[] = 'ID = "'.intval($this->args['id']).'"';
			endif;
		endif;

		if( isset($this->args['user_id']) && $this->args['user_id'] ):
			if( is_array($this->args['user_id']) ):
				$page_ids = implode('","', $this->args['user_id']);
				$where[] = 'user_id IN("'.$page_ids.'")';
			else:
				$where[] = 'user_id = "'.intval($this->args['user_id']).'"';
			endif;
		endif;

		if( isset($this->args['subdomain']) && $this->args['subdomain'] ):
			if( is_array($this->args['subdomain']) ):
				$domains = implode('","', $this->args['subdomain']);
				$where[] = 'subdomain IN("'.$domains.'")';
			else:
				$where[] = 'subdomain = "'.sanitize_text_field($this->args['subdomain']).'"';
			endif;
		endif;

		if( isset($this->args['page_id']) && $this->args['page_id'] ):
			if( is_array($this->args['page_id']) ):
				$page_ids = implode('","', $this->args['page_id']);
				$where[] = 'page_id IN("'.$page_ids.'")';
			else:
				$where[] = 'page_id = "'.intval($this->args['page_id']).'"';
			endif;
		endif;

		if( isset($this->args['s']) && $this->args['s'] ):
			$where[] = 'subdomain LIKE "%'.sanitize_text_field($this->args['s']).'%" OR data LIKE "%'.sanitize_text_field($this->args['s']).'%"';
		endif;

		if( count($where) >= 1 ){
			$this->query .= ' WHERE '.implode(' AND ', $where );
		}

		return $this;
	}

	/**
	 * query
	 * @return [type] [description]
	 */
	public function results(){

		$query = 'SELECT * FROM '.$this->table.$this->query;

		$query .= ' ORDER BY ID DESC LIMIT '.$this->args['limit'].' OFFSET '.$this->args['offset'];

		$result = $this->wpdb->get_results($query);

		$this->count = $this->wpdb->num_rows;
		$this->found = $this->wpdb->get_var('SELECT count(*) FROM '.$this->table.$this->query);

		return array_map('wpapg_get_subdomain', $result);
	}

}

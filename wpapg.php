<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://www.fiqhidayat.com
 * @since             1.0.0
 * @package           Wpapg
 *
 * @wordpress-plugin
 * Plugin Name:       Affiliate Page Generator
 * Plugin URI:        https://www.affiliatepagegenerator.com/
 * Description:       Affiliate page generator
 * Version:           1.9.0
 * Author:            Fiq Hidayat
 * Author URI:        https://www.fiqhidayat.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       wpapg
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
}

global $wpapg;

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define('WPAPG_VERSION', '1.8.6');
define('WPAPG_URL', plugin_dir_url(__FILE__));
define('WPAPG_DIR', plugin_dir_path(__FILE__));
define('WPAPG_ROOT', __FILE__);
define('WPAPG_ADDON_DOMAIN_TABLE', 'wpapg_addon_domain');
define('WPAPG_SUBDOMAIN_TABLE', 'wpapg_subdomain');
define('WPAPG_LEADERBOARD_TABLE', 'wpapg_leaderboard');
define('WPAPG_SHORTCODE_PATTERN', '/{{(.*?)}}/');

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-wpapg-activator.php
 */
function activate_wpapg()
{
    require_once WPAPG_DIR . 'includes/class-wpapg-activator.php';
    Wpapg_Activator::create_addon_domain_table();
    Wpapg_Activator::create_subdomain_table();
    Wpapg_Activator::create_leaderboard_table();
    update_option('wpapg_upgrade_database', '1.0.0');
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-wpapg-deactivator.php
 */
function deactivate_wpapg()
{
    require_once WPAPG_DIR . 'includes/class-wpapg-deactivator.php';
    Wpapg_Deactivator::deactivate();
}

register_activation_hook(__FILE__, 'activate_wpapg');
register_deactivation_hook(__FILE__, 'deactivate_wpapg');

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require WPAPG_DIR . 'includes/class-wpapg.php';
require WPAPG_DIR . 'plugin-update/plugin-update-checker.php';


/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_wpapg()
{

    //date_default_timezone_set("''");
    $plugin = new Wpapg();
    $plugin->run();
}
run_wpapg();

if (!function_exists('wpapg_filter_username')) {
    add_filter('username_exists', 'wpapg_filter_username', 10, 2);
    function wpapg_filter_username($user_id, $username)
    {
        global $wpdb;

        //bentrok dengan wuoymember
        if (did_action('wuoyMember-prepare'))
            return $user_id;

        if ($user_id)
            return $user_id;

        //check is username or subdomain on blacklist list
        $blacklist_subdomain = wpapg_get_option('blacklist_subdomain');
        $blacklist_subdomain = explode(',', $blacklist_subdomain);
        $blacklist_subdomain = array_merge($blacklist_subdomain, ['www', 'admin', 'administrator']);

        if (in_array($username, $blacklist_subdomain))
            return 99999999;

        foreach ($blacklist_subdomain as $key => $val) :
            if (strpos($username, $val)) {
                return 99999999;
            }
        endforeach;

        //check limit username
        $limit_min = wpapg_get_option('limit_min_subdomain', 5);
        $limit_max = wpapg_get_option('limit_max_subdomain', 20);

        if (strlen($username) <= $limit_min || strlen($username) >= $limit_max)
            return 99999999;

        return $user_id;
    }
}

if (!function_exists('__debug')) {
    function __debug()
    {
        echo '<pre>';
        print_r(func_get_args());
        echo '</pre>';
    }
}

if (!function_exists('wpapg_shortcode_in')) {
    function wpapg_shortcode_in($shortcode)
    {

        $shortcode = sanitize_text_field($shortcode);

        preg_match(WPAPG_SHORTCODE_PATTERN, $shortcode, $match);

        return is_array($match) && isset($match[1]) ? $match[1] : $shortcode;
    }
}

if (!function_exists('wpapg_shortcode_out')) {
    function wpapg_shortcode_out($key)
    {

        $key = sanitize_text_field($key);

        preg_match(WPAPG_SHORTCODE_PATTERN, $key, $match);

        return is_array($match) && isset($match[1]) ? $key : '{{' . $key . '}}';
    }
}

if (!function_exists('wpapg_global_shortcode_save')) {
    function wpapg_global_shortcode_save($subdomain, $data)
    {

        $global_shortcode = get_option('wpapg_global_shortcode');

        $inserted = array();
        foreach ((array) $global_shortcode as $key => $val) :

            if (!isset($data[$key])) continue;

            if ($val['type'] == 'image' && is_array($data[$key]) && isset($data[$key]['tmp_name'])) :

                $upload = wp_upload_bits($data[$key]['name'], null, file_get_contents($data[$key]['tmp_name']));

                $inserted[$key] = array(
                    'label' => $val['label'],
                    'value' => isset($upload['url']) ? esc_url_raw($upload['url']) : '',
                );
            else :
                $v = is_array($data[$key]) ? implode(',', $data[$key]) : $data[$key];

                /**
                 * make sure all input not contain harmfull code
                 * @var string
                 */
                $regex = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,6}(\/\S*)?/";
                if (preg_match($regex, $v, $url)) :
                    $clean_url = esc_url_raw($url[0]);
                    $v = preg_replace($regex, $clean_url, $v);
                endif;

                $inserted[$key] = array(
                    'label' => $val['label'],
                    'value' => sanitize_text_field($v),
                );
            endif;
        endforeach;

        update_option('wpapg_global_shortcode_' . $subdomain, $inserted, false);
    }
}

if (!function_exists('wpapg_get_option')) {
    function wpapg_get_option($key, $default = '')
    {
        global $wpapg;

        if (isset($wpapg['options']) && isset($wpapg['options'][$key]) && $wpapg['options'][$key])
            return $wpapg['options'][$key];

        return $default;
    }
}

if (!function_exists('wpapg_get_domain')) {
    function wpapg_get_domain()
    {

        $url = wpapg_get_option('siteurl', site_url());
        $domain = parse_url($url);

        return str_replace('www.', '', $domain['host']);
    }
}

if (!function_exists('wpapg_get_current_subdomain')) {
    function wpapg_get_current_subdomain()
    {

        $regex = '/([a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,7})$/';
        $host = parse_url(site_url(), PHP_URL_HOST);

        preg_match($regex, $host, $hasil);

        $root_domain = isset($hasil[0]) ? $hasil[0] : $host;

        if ($host == $root_domain) return false;

        $subdomain = str_replace('.' . $root_domain, '', $host);

        return $subdomain;
    }
}

if (!function_exists('wpapg_get_page_permalink')) {
    function wpapg_get_page_permalink($page_id = false, $subdomain = false)
    {

        $permalink = wpapg_get_option('siteurl', site_url());
        $homepage_addon_domain = wpapg_get_addon_domain_by('page_id', NULL);

        if ($homepage_addon_domain) :
            $permalink = 'http://' . $homepage_addon_domain->domain;
        endif;

        if (!$page_id && !$subdomain)
            return $permalink;

        if ($page_id) :
            $addon_domain = wpapg_get_addon_domain_by('page_id', intval($page_id));
            if ($addon_domain) :
                $permalink = 'http://' . $addon_domain->domain;
            endif;

            if ($post = get_post(intval($page_id))) :
                $link = parse_url(get_the_permalink($page_id));
                $path = isset($link['path']) ? $link['path'] : '';
                $permalink = $permalink . $path;
            endif;
        endif;

        if ($subdomain) :
            $link = parse_url($permalink);
            $domain = str_replace('www.', '', $link['host']);
            $path = isset($link['path']) ? $link['path'] : '';
            $permalink = $link['scheme'] . '://' . sanitize_text_field($subdomain) . '.' . $domain . $path;
        endif;

        return $permalink;
    }
}

if (!function_exists('wpapg_get_addon_domain')) {
    function wpapg_get_addon_domain($addon_domain)
    {

        $d = new Wpapg_Addon_Domain($addon_domain);

        if ($d->data) return $d->data;

        return false;
    }
}

if (!function_exists('wpapg_get_addon_domain_by')) {
    function wpapg_get_addon_domain_by($field, $value, $first = true)
    {

        $args = array(
            sanitize_text_field($field) => is_array($value) ? array_map('sanitize_text_field', $value) : sanitize_text_field($value),
            'status' => 'connected'
        );

        $d = new Wpapg_Addon_Domain_Query($args);

        $result = $d->results();

        if ($first && isset($result[0]))
            return $result[0];

        return $result;
    }
}

if (!function_exists('wpapg_get_leaderboard')) {
    function wpapg_get_leaderboard($leaderboard)
    {

        $s = new Wpapg_Subdomain($leaderboard);

        if ($s->data) return $s->data;

        return false;
    }
}

if (!function_exists('wpapg_get_subdomain')) {
    function wpapg_get_subdomain($subdomain)
    {

        $s = new Wpapg_Subdomain($subdomain);

        if ($s->data) return $s->data;

        return false;
    }
}

if (!function_exists('wpapg_get_subdomain_by')) {
    function wpapg_get_subdomain_by($field, $value, $first = true)
    {

        $args = array(
            sanitize_text_field($field) => is_array($value) ? array_map('sanitize_text_field', $value) : sanitize_text_field($value),
        );

        $s = new Wpapg_Subdomain_Query($args);

        $result = $s->results();

        if ($first && isset($result[0]))
            return $result[0];

        return $result;
    }
}

if (!function_exists('wpapg_is_domain')) {
    function wpapg_is_domain($string)
    {

        $regex = '/(^(?:[a-zA-Z0-9]+(?:\-*[a-zA-Z0-9])*\.)+[a-zA-Z]{2,7}$)/';

        if (preg_match($regex, $string, $url)) return true;

        return false;
    }
}

if (!function_exists('wpapg_is_phone_number')) {
    function wpapg_is_phone_number($phone)
    {

        $regex = '/[a-zA-Z]/';

        if (preg_match($regex, $phone, $match)) return false;

        $filtered_phone_number = filter_var($phone, FILTER_SANITIZE_NUMBER_INT);
        $phone_to_check = str_replace("-", "", $filtered_phone_number);

        if (strlen($phone_to_check) < 9 || strlen($phone_to_check) > 14) return false;

        $phone_to_check = preg_replace('/[^0-9]/', '', $phone_to_check);
        $phone_to_check = preg_replace('/^620/', '62', $phone_to_check);
        $phone_to_check = preg_replace('/^0/', '62', $phone_to_check);

        return $phone_to_check;
    }
}

if (!function_exists('wpapg_shortcode_form')) {
    function wpapg_shortcode_form($page_id, $styles = array())
    {
        global $post, $wpapg;

        $available_style = array('table', 'inline', 'block');
        $style = $styles['style'] && in_array($styles['style'], $available_style) ? $styles['style'] : 'table';

        $global_shortcode = get_option('wpapg_global_shortcode');
        if (is_user_logged_in() && !current_user_can('manage_options')) {
            $user = wp_get_current_user();
            $current_global_shortcode = get_option('wpapg_global_shortcode_' . $user->user_login);
        }

        ob_start();

        if ($page_id) :

            $enable = get_post_meta($page_id, 'wpapg_shortcode_enable', true);
            $shortcode = get_post_meta($page_id, 'wpapg_shortcode', true);
            $current_shortcode = array();
            $current_shortcode_id = 0;

            if ($enable) :

                if (is_user_logged_in() && !current_user_can('manage_options')) {

                    $args = array(
                        'user_id' => get_current_user_id(),
                        'page_id' => $page_id,
                    );

                    $query = new Wpapg_Subdomain_Query($args);
                    $db = $query->results();

                    $current_shortcode = isset($db[0]) ? $db[0]->data : $current_shortcode;
                    $current_shortcode_id = isset($db[0]) ? $db[0]->ID : $current_shortcode_id;
                }

                $page_url = parse_url(wpapg_get_page_permalink($page_id));
                $page_path = isset($page_url['path']) ? $page_url['path'] : '';
                $page_domain = str_replace('www.', '', $page_url['host']);
                $page_domain = $page_domain . $page_path;

                include(WPAPG_DIR . 'public/partials/form-generator-single.php');

            endif;

        else :
            $active_pages = Wpapg_Subdomain::active_pages();
            $subdomain = array();
            $subdomain = isset($_GET['subdomain']) && $_GET['subdomain'] ? wpapg_get_subdomain_by('subdomain', sanitize_text_field($_GET['subdomain']), false) : $subdomain;

            $subdomains = is_user_logged_in() && !current_user_can('manage_options') ? wpapg_get_subdomain_by('user_id', get_current_user_id(), false) : $subdomain;

            $page_url = parse_url(wpapg_get_page_permalink());
            $page_path = isset($page_url['path']) ? $page_url['path'] : '';
            $page_domain = str_replace('www.', '', $page_url['host']);
            $page_domain = $page_domain . $page_path;

            $current_subdomains = array();

            foreach ((array)$subdomains as $sh) {
                $current_subdomains[$sh->page_id] = $sh;
            }

            include(WPAPG_DIR . 'public/partials/form-generator-all.php');
        endif;
        $form = ob_get_contents();
        ob_end_clean();

        return $form;
    }
}

if (!function_exists('wpapg_minify_css')) {
    function wpapg_minify_css($css)
    {
        // some of the following functions to minimize the css-output are directly taken
        // from the awesome CSS JS Booster: https://github.com/Schepp/CSS-JS-Booster
        // all credits to Christian Schaefer: http://twitter.com/derSchepp
        // remove comments
        $css = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $css);
        // backup values within single or double quotes
        preg_match_all('/(\'[^\']*?\'|"[^"]*?")/ims', $css, $hit, PREG_PATTERN_ORDER);
        for ($i = 0; $i < count($hit[1]); $i++) {
            $css = str_replace($hit[1][$i], '##########' . $i . '##########', $css);
        }
        // remove traling semicolon of selector's last property
        $css = preg_replace('/;[\s\r\n\t]*?}[\s\r\n\t]*/ims', "}\r\n", $css);
        // remove any whitespace between semicolon and property-name
        $css = preg_replace('/;[\s\r\n\t]*?([\r\n]?[^\s\r\n\t])/ims', ';$1', $css);
        // remove any whitespace surrounding property-colon
        $css = preg_replace('/[\s\r\n\t]*:[\s\r\n\t]*?([^\s\r\n\t])/ims', ':$1', $css);
        // remove any whitespace surrounding selector-comma
        $css = preg_replace('/[\s\r\n\t]*,[\s\r\n\t]*?([^\s\r\n\t])/ims', ',$1', $css);
        // remove any whitespace surrounding opening parenthesis
        $css = preg_replace('/[\s\r\n\t]*{[\s\r\n\t]*?([^\s\r\n\t])/ims', '{$1', $css);
        // remove any whitespace between numbers and units
        $css = preg_replace('/([\d\.]+)[\s\r\n\t]+(px|em|pt|%)/ims', '$1$2', $css);
        // shorten zero-values
        $css = preg_replace('/([^\d\.]0)(px|em|pt|%)/ims', '$1', $css);
        // constrain multiple whitespaces
        $css = preg_replace('/\p{Zs}+/ims', ' ', $css);
        // remove newlines
        $css = str_replace(array("\r\n", "\r", "\n"), '', $css);
        // Restore backupped values within single or double quotes
        for ($i = 0; $i < count($hit[1]); $i++) {
            $css = str_replace('##########' . $i . '##########', $hit[1][$i], $css);
        }
        return $css;
    }
}

add_filter('clean_url', 'wpapg_bypass_url_shortcode', 99, 3);
function wpapg_bypass_url_shortcode($good_protocol_url, $original_url, $_context)
{

    $check = strpos($original_url, '{{');

    if ($check === false) return $good_protocol_url;

    return $original_url;
}

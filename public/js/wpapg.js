//button to input file
function wpapgUploaderButton(ini) {
    let parent = ini.parentElement;
    let inputFile = parent.querySelector('input[type="file"]');
    let inputText = parent.querySelector('input[type="text"]');
    inputFile.click();
};

function wpapgUploader(ini) {
    let parent = ini.parentElement;
    let inputText = parent.querySelector('input[type="text"]');
    let file = ini.files[0];
    let mime_types = ['image/jpeg', 'image/png'];
    // Validate MIME type
    if (mime_types.indexOf(file.type) == -1) {
        alert('Error : Incorrect file type');
        return;
    }

    // Max 2 Mb allowed
    if (file.size > 2 * 1024 * 1024) {
        alert('Error : Exceeded size 2MB');
        return;
    }
    inputText.value = file.name;
};

//Copy to clipboard function
function wpapgCopyToClipboard(ini) {
    let text = ini.getAttribute('data-copy');
    let input = document.createElement('input');
    input.style = "position: absolute; left: -1000px; top: -1000px";
    input.setAttribute('value', text);
    document.body.appendChild(input);
    input.select();
    let result = document.execCommand('copy');
    document.body.removeChild(input);
    wpapgAddTooltip(ini, 'Copied');
    setTimeout(wpapgRemoveTooltip, 1000, ini)
    return result;
}

//add tooltip
function wpapgAddTooltip(obj, message) {
    obj.setAttribute('wpapgtooltip', message);
}
//remove tooltip
function wpapgRemoveTooltip(obj) {
    obj.removeAttribute("wpapgtooltip");
}

//check $subdomains
function wpapgCheckSubdomain(ini) {

    let subdomain = ini.value,
        inputBox = ini.parentElement.parentElement,
        desc = inputBox.querySelector('.desc').querySelector('span'),
        noticee = inputBox.querySelector('.inpute').querySelector('span'),
        regex = new RegExp('^[a-z0-9]+$'),
        ngajax = ini.getAttribute('ajax'),
        form = document.querySelector('#wpapgform'),
        submit = form.querySelector('button[name="submito"]');

    if (subdomain == '') {
        desc.innerHTML = 'example';
        return;
    }

    noticee.innerHTML = '';
    noticee.style.display = 'none';

    if (!regex.test(subdomain)) {
        noticee.innerHTML = 'Please! insert lowercase alfanumeric only';
        noticee.style.display = 'block';
        ini.value = '';
        desc.innerHTML = 'example';
        submit.disabled = true;
    } else {
        desc.innerHTML = subdomain;

        let ajax = new XMLHttpRequest();
        ngajax += '&subdomain=' + subdomain;

        ajax.open('GET', ngajax, true);
        ajax.onload = function() {
            let str = ajax.responseText;
            if (ajax.status === 200 && str.replace(/\s/g, '') != 'OK') {
                noticee.innerHTML = ajax.responseText;
                noticee.style.display = 'block';
                submit.disabled = true;
            } else {
                noticee.innerHTML = '';
                noticee.style.display = 'none';
                submit.disabled = false;
            }
        }
        ajax.send(null);
    }
}

function wpapgCheckLinkAffiliate(ini) {

    let linkAffiliate = ini.value,
        inputBox = ini.parentElement.parentElement,
        noticee = inputBox.querySelector('.inpute').querySelector('span'),
        regex = new RegExp('^(http:\/\/www\.|https:\/\/www\.|www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?'),
        ngajax = ini.getAttribute('ajax'),
        form = document.querySelector('#wpapgform'),
        submit = form.querySelector('button[name="submito"]');

    noticee.innerHTML = '';
    noticee.style.display = 'none';

    if (!regex.test(linkAffiliate)) {
        noticee.innerHTML = 'Invalid url';
        noticee.style.display = 'block';
        ini.value = '';
        submit.disabled = true;
    } else {

        let ajax = new XMLHttpRequest();
        let ajax_link = ngajax + '&link=' + linkAffiliate;

        ajax.open('GET', ajax_link, true);
        ajax.onload = function() {
            let str = ajax.responseText;
            if (ajax.status === 200 && str.replace(/\s/g, '') != 'ko') {
                noticee.innerHTML = ajax.responseText;
                noticee.style.display = 'block';
                submit.disabled = true;
            } else {
                noticee.innerHTML = '';
                noticee.style.display = 'none';
                submit.disabled = false;
            }
        }
        ajax.send(null);
    }
}

// form submit
function wpapgFormSubmit(ini) {
    let ngajax = ini.getAttribute('ajax'),
        data = ini.elements,
        submit = ini.querySelector('button[name="submito"]'),
        wpapg = ini.parentElement,
        result = wpapg.querySelector('.wpapg-result');

    submit.disabled = true;
    submit.innerHTML = 'Generating ... please wait !';
    result.removeAttribute('style');
    result.style.display = 'none';

    let inputs = new FormData(ini);

    let ajax = new XMLHttpRequest();
    ajax.open('POST', ngajax + '?action=create-subdomain');
    //ajax.setRequestHeader('Content-Type', 'multipart/form-data');
    ajax.onload = function() {
        console.log(ajax.responseText);

        let data = JSON.parse(ajax.responseText),
            newNode = document.createElement('span'),
            buttonCopy = result.querySelector('.wpapgcopylink'),
            aFollowLink = result.querySelector('.wpapgfollowlink');

        if (ajax.status === 200 && data.status == 'success') {

            buttonCopy.setAttribute('data-copy', data.message);
            aFollowLink.setAttribute('href', data.message);
            newNode.innerHTML = data.message;
            result.insertBefore(newNode, result.firstChild);
            result.style.display = 'block';

            submit.disabled = false;
            submit.innerHTML = 'Generate Page';

        } else if (ajax.status === 200 && data.status == 'error') {

            result.style.color = 'red';
            result.innerHTML = data.message;
            result.style.border = '1px solid red';
            result.style.display = 'block';

            submit.disabled = false;
            submit.innerHTML = 'Generate Page';
        } else {
            alert('Error ocurred, please refresh and try again or Contact administrator !')
        }
    }
    ajax.send(inputs);

    return false;

}
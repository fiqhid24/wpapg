<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://www.fiqhidayat.com
 * @since      1.0.0
 *
 * @package    Wpapg
 * @subpackage Wpapg/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Wpapg
 * @subpackage Wpapg/public
 * @author     Fiq Hidayat <taufik@fiqhidayat.com>
 */
class Wpapg_Public
{

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $plugin_name    The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string    $plugin_name       The name of the plugin.
     * @param      string    $version    The version of this plugin.
     */
    public function __construct($plugin_name, $version)
    {

        $this->plugin_name = $plugin_name;
        $this->version = $version;
    }

    /**
     * Register the stylesheets for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function enqueue_styles()
    {
        global $post;

        //if( isset($post->post_content) && !has_shortcode( $post->post_content, 'wpapg_form_generator' ) ) return;

        if (file_exists(WPAPG_DIR . 'public/css/wpapg.css')) :
            ob_start();
            include_once(WPAPG_DIR . 'public/css/wpapg.css');
            $css = ob_get_contents();
            ob_end_clean();
            echo '<style type="text/css">' . wpapg_minify_css($css) . '</style>';
        endif;
    }

    /**
     * Register the JavaScript for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts()
    {
        global $post;

        //if( isset($post->post_content) && !has_shortcode( $post->post_content, 'wpapg_form_generator' ) ) return;

        if (file_exists(WPAPG_DIR . 'public/js/wpapg.js')) :
            ob_start();
            include_once(WPAPG_DIR . 'public/js/wpapg.js');
            $js = ob_get_contents();
            ob_end_clean();
            echo '<script type="text/javascript">' . $js . '</script>';
        endif;
    }

    /**
     * call on init hook
     * @return [type] [description]
     */
    public function init()
    {
        global $wpdb, $wpapg;

        $wpapg['options'] = get_option('wpapg_options');
        //Checking domain
        Wpapg_Addon_Domain::validation();
        Wpapg_Addon_Domain::check();

        add_shortcode('wpapg_form_generator', [$this, 'form_generator_shortcode']);
        add_shortcode('wpapg_subdomain_search', [$this, 'subdomain_search_shortcode']);
    }

    /**
     * called on wp hook
     * @return [type] [description]
     */
    public function wp()
    {

        if (is_admin()) return;
    }

    /**
     * called on template_redirect hook
     * @return [type] [description]
     */
    public function template_redirect()
    {

        global $wpapg;

        Wpapg_Addon_Domain::access();
        Wpapg_Subdomain::set_shortcode();
        Wpapg_Leaderboard::set_shortcode();
    }

    /**
     * called on wp hook
     * @return [type] [description]
     */
    public function wp_head()
    {
        global $wpapg;

        Wpapg_Subdomain::set_locker();

        // __debug($wpapg);
        // exit;
    }

    /**
     * form generator shortcode display
     * @param  [type] $attr [description]
     * @return [type]       [description]
     */
    public function form_generator_shortcode($atts)
    {
        $attr = shortcode_atts(
            array(
                'page_id' => false,
                'style' => 'table',
            ),
            $atts
        );

        $page_id = isset($_GET['lp']) && $_GET['lp'] ? intval($_GET['lp']) : false;
        $page_id = $attr['page_id'] ? intval($attr['page_id']) : $page_id;

        $form = wpapg_shortcode_form($page_id, $attr);

        return $form;
    }

    /**
     * ajax create subdomain
     * @return [type] [description]
     */
    public function ajax_create_subdomain()
    {

        //Check submit
        if (!isset($_POST['__nonce'])) {
            exit;
        }

        //verify nonce
        if (!wp_verify_nonce($_POST['__nonce'], 'noncenonce')) {
            exit;
        }

        $page_id = intval($_POST['page_id']);

        if (isset($_POST['value']['link_affiliate'])) :

            $link_affiliate = $_POST['value']['link_affiliate'];

            if (!filter_var($link_affiliate, FILTER_VALIDATE_URL)) :
                $respons = array(
                    'status' => 'error',
                    'message' => 'Invalid link affiliate',
                );

                echo json_encode($respons);
                exit;
            endif;

            //$link_affiliate = strtok($link_affiliate,'?');

            $args = array(
                's' => esc_url($link_affiliate),
                'page_id' => $page_id,
            );

            $query = new Wpapg_Subdomain_Query($args);

            $result = $query->results();

            if ($result && isset($result[0])) :

                $cshortcode_exists = wpapg_get_subdomain($result[0]);
                $respons = array(
                    'status' => 'success',
                    'message' => wpapg_get_page_permalink($page_id, $cshortcode_exists->subdomain),
                );

                echo json_encode($respons);
                exit;

            endif;

        endif;

        $post_global_value = isset($_POST['global_value']) ? $_POST['global_value'] : array();
        $post_global_defvalue = isset($_POST['global_defvalue']) ? $_POST['global_defvalue'] : array();
        $global_value = array_merge($post_global_value, $post_global_defvalue);

        $global_files = array();
        if (isset($_FILES) && isset($_FILES['global_value'])) :
            foreach ((array)$_FILES['global_value']['name'] as $key => $val) :
                if (empty($val)) continue;

                $global_files[$key] = array(
                    'name'     => $val,
                    'type'     => $_FILES['global_value']['type'][$key],
                    'tmp_name' => $_FILES['global_value']['tmp_name'][$key],
                    'error'    => $_FILES['global_value']['error'][$key],
                    'size'     => $_FILES['global_value']['size'][$key],
                );
            endforeach;
        endif;

        $global_value = array_merge($global_value, $global_files);

        $post_value = isset($_POST['value']) ? $_POST['value'] : array();
        $post_defvalue = isset($_POST['defvalue']) ? $_POST['defvalue'] : array();
        $value = array_merge($post_value, $post_defvalue);

        $files = array();
        if (isset($_FILES) && isset($_FILES['value'])) :
            foreach ((array)$_FILES['value']['name'] as $key => $val) :
                if (empty($val)) continue;

                $files[$key] = array(
                    'name'     => $val,
                    'type'     => $_FILES['value']['type'][$key],
                    'tmp_name' => $_FILES['value']['tmp_name'][$key],
                    'error'    => $_FILES['value']['error'][$key],
                    'size'     => $_FILES['value']['size'][$key],
                );
            endforeach;
        endif;

        $value = array_merge($value, $files);

        //$shortcode = get_post_meta($page_id, 'wpapg_shortcode', true);

        //exit if page id invalid
        if (!$page_id) exit;

        $subdomain = new Wpapg_Subdomain(intval($_POST['ID']));

        $subdomain->set_page($page_id);
        if (is_user_logged_in() && !current_user_can('manage_options')) :
            $subdomain->set_user_id(get_current_user_id());
        else :
            $subdomain->set_subdomain($_POST['subdomain']);
        endif;
        $subdomain->set_data($value);

        if ($subdomain->data) :
            $save = $subdomain->update();
        else :
            $save = $subdomain->insert();
        endif;

        if (is_wp_error($save)) {
            $respons = array(
                'status' => 'error',
                'message' => $save->get_error_message(),
            );
        } else {
            $current_shortcode = wpapg_get_subdomain($save);
            wpapg_global_shortcode_save($current_shortcode->subdomain, $global_value);
            $respons = array(
                'status' => 'success',
                'message' => wpapg_get_page_permalink($page_id, $current_shortcode->subdomain),
            );

            do_action('wpapg_create_subdomain', $respons['message'], $_POST);
        }

        echo json_encode($respons);
        exit;
    }

    public function ajax_check_link_affiliate()
    {

        $nonce = $_REQUEST['nonce'];
        if (!wp_verify_nonce($nonce, 'noncenonce')) exit;

        $args = array(
            's' => esc_url($_GET['link']),
        );

        $query = new Wpapg_Subdomain_Query($args);

        $result = $query->results();

        if ($result && isset($result[0])) {
            echo 'Link affiliate already used';
        } else {
            echo 'ko';
        }

        exit;
    }

    /**
     * ajax check domain
     * @return [type] [description]
     */
    public function ajax_check_subdomain()
    {
        $nonce = $_REQUEST['nonce'];
        if (!wp_verify_nonce($nonce, 'noncenonce')) exit;
        $subdomain = sanitize_text_field($_GET['subdomain']);
        $check = Wpapg_Subdomain::check_subdomain($subdomain);
        if (is_wp_error($check)) :
            echo $check->get_error_message();
            exit;
        else :
            echo esc_attr('OK');
            exit;
        endif;
        exit;
    }

    /**
     * replace subdomain on content
     * @param  [type] $content [description]
     * @return [type]          [description]
     */
    public function the_content($content)
    {
        global $post;

        if (is_admin() || empty($content)) return $content;

        if (isset($post->post_content) && has_shortcode($post->post_content, 'wpapg_form_generator')) return $content;

        $content = preg_replace_callback(WPAPG_SHORTCODE_PATTERN, [$this, 'replace_shortcode'], $content);

        return $content;
    }

    /**
     * replace shortcode
     * @param  [type] $shortcode_array [description]
     * @return [type]                  [description]
     */
    public function replace_shortcode($shortcode_array)
    {
        global $wpapg;

        $default     = isset($wpapg['shortcode_default']) ? $wpapg['shortcode_default'] : array();
        $data        = isset($wpapg['shortcode']) ? $wpapg['shortcode'] : array();
        $leaderboard = isset($wpapg['leaderboard']) ? $wpapg['leaderboard'] : array();
        $shortcode   = $shortcode_array[1];

        $val = isset($data[$shortcode]['value']) && $data[$shortcode]['value'] ? esc_attr($data[$shortcode]['value']) : false;

        if (!$val)
            $val = isset($default[$shortcode]['value']) && $default[$shortcode]['value'] ? esc_attr($default[$shortcode]['value']) : false;

        if (isset($wpapg['leaderboard'])) {
            if (!$val)
                $val = isset($leaderboard[$shortcode]) && $leaderboard[$shortcode] ? esc_attr($leaderboard[$shortcode]) : false;

            if (false === $val && strpos($shortcode, 'rank') !== false) :
                preg_match('/\d+/', $shortcode, $match);
                $rank = isset($match[0]) ? $match[0] : '';
                if (strpos($shortcode, 'name') !== false) :
                    $val = '#' . $rank;
                endif;
                if (strpos($shortcode, 'photo') !== false) :
                    $val = 'https://i.stack.imgur.com/l60Hf.png';
                endif;
                if (strpos($shortcode, 'value') !== false) :
                    $val = '0';
                endif;
            endif;
        }

        if ($val === false) return $shortcode_array[0];

        if (wpapg_is_domain($val))
            $val = esc_url_raw($val);

        if (wpapg_is_phone_number($val))
            $val = wpapg_is_phone_number($val);

        return $val;
    }

    /**
     * ajax check domain
     * @return [type] [description]
     */
    public function ajax_search_subdomain()
    {
        $nonce = $_REQUEST['nonce'];
        if (!wp_verify_nonce($nonce, 'noncenonce')) exit;
        $subdomain = sanitize_text_field($_GET['subdomain']);
        $check = wpapg_get_subdomain_by('subdomain', $subdomain);
        if (!$check) :
            echo 'Nama toko Yang Anda cari tidak tersedia';
            exit;
        else :
            echo esc_attr('OK');
            exit;
        endif;
        exit;
    }


    public function subdomain_search_shortcode($att)
    {

        $domain = parse_url(site_url(), PHP_URL_HOST);
        $domain = str_replace('www.', '', $domain);

        $attr = shortcode_atts(array(
            'button_text' => 'submit',
            'domain' => $domain,
            'error_message' => 'Nama toko Yang Anda cari tidak tersedia',
            'placeholder' => 'example',
        ), $att);

        ob_start();
?>
        <style>
            .wpapg-subdomain-search-box {
                position: relative;
                height: 40px;
            }

            .wpapg-subdomain-search-box input {
                width: calc(100% - 150px);
                float: left;
                height: 40px;
                line-height: 40px;
                padding: 0 10px;
            }

            .wpapg-subdomain-search-box button {
                width: 150px;
                float: right;
                height: 40px;
                line-height: 40px;
                padding: 0;
                text-align: center;
            }
        </style>
        <div class="wpapg-subdomain-search-box">
            <input onkeyup="wpapgSubdomainSearch(this);" type="text" id="sbd-search" placeholder="<?php echo $attr['placeholder']; ?>" ajax="<?php echo admin_url('admin-ajax.php'); ?>?action=search-subdomain&nonce=<?php echo wp_create_nonce('noncenonce'); ?>">
            <button type="button" disabled="disabled"><?php echo $attr['button_text']; ?></button>
            <div class="desc"></div>
        </div>
        <script>
            function wpapgSubdomainSearch(ini) {

                let subdomain = ini.value,
                    inputBox = ini.parentElement,
                    desc = inputBox.querySelector('.desc'),
                    regex = new RegExp('^[a-z0-9]+$'),
                    ngajax = ini.getAttribute('ajax'),
                    submit = inputBox.querySelector('button'),
                    scheme = '<?php echo is_ssl() ? '
    https: //' : 'http://'; ?>',
                    site = '<?php echo $attr['
    domain ']; ?>';

                if (!regex.test(subdomain)) {
                    desc.innerHTML = '<span style="color:red">Please! insert lowercase alfanumeric only</span>';
                    submit.disabled = true;
                } else {

                    let ajax = new XMLHttpRequest();
                    ngajax += '&subdomain=' + subdomain;

                    ajax.open('GET', ngajax, true);
                    ajax.onload = function() {
                        let str = ajax.responseText;
                        if (ajax.status === 200 && str.replace(/\s/g, '') == 'OK') {
                            desc.innerHTML = scheme + subdomain + '.' + site;
                            submit.disabled = false;
                            submit.setAttribute('onclick', 'window.location.href = \'' + scheme + subdomain + '.' + site +
                                '\';');
                        } else {
                            desc.innerHTML = '<span style="color:red"><?php echo $attr['
                error_message ']; ?></span>';
                            submit.disabled = true;
                            submit.removeAttribute('onclick');
                        }
                    }
                    ajax.send(null);
                }
            }
        </script>
<?php
        $content = ob_get_contents();
        ob_end_clean();

        return $content;
    }
}

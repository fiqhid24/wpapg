<?php

if( isset($_POST['submito']) && $_POST['submito'] == 1 && wp_verify_nonce( $_POST['__nonce'], 'noncenonce' ) ){
    $post_global_value = isset($_POST['global_value']) ? $_POST['global_value'] : array();
    $post_global_defvalue = isset($_POST['global_defvalue']) ? $_POST['global_defvalue'] : array();
    $global_value = array_merge($post_global_value, $post_global_defvalue);

    $global_files = array();
    if( isset($_FILES) && isset($_FILES['global_value']) ):
        foreach( (array)$_FILES['global_value']['name'] as $key=>$val):
            if( empty($val) ) continue;

            $global_files[$key] = array(
                'name'     => $val,
                'type'     => $_FILES['global_value']['type'][$key],
                'tmp_name' => $_FILES['global_value']['tmp_name'][$key],
                'error'    => $_FILES['global_value']['error'][$key],
                'size'     => $_FILES['global_value']['size'][$key],
            );
        endforeach;
    endif;

    $global_value = array_merge($global_value, $global_files);

    $active_pages = Wpapg_Subdomain::active_pages();

    foreach( (array)$active_pages as $p ):

        $page_id = intval($p['ID']);

        //if( !isset($_POST['ID'][$page_id]) )continue;

        $post_value = isset($_POST['value'][$page_id]) ? $_POST['value'][$page_id] : array();
        $post_defvalue = isset($_POST['defvalue'][$page_id]) ? $_POST['defvalue'][$page_id] : array();
        $value = array_merge($post_value, $post_defvalue);

        $files = array();
        if( isset($_FILES) && isset($_FILES['value']['name'][$page_id]) ):
            foreach( (array)$_FILES['value']['name'][$page_id] as $key=>$val):
                if( empty($val) ) continue;

                $files[$key] = array(
                    'name'     => $val,
                    'type'     => $_FILES['value']['type'][$page_id][$key],
                    'tmp_name' => $_FILES['value']['tmp_name'][$page_id][$key],
                    'error'    => $_FILES['value']['error'][$page_id][$key],
                    'size'     => $_FILES['value']['size'][$page_id][$key],
                );
            endforeach;
        endif;

        $value = array_merge($value, $files);

        $subdomain = new Wpapg_Subdomain(intval($_POST['ID'][$page_id]));

        $subdomain->set_page($page_id);
        if( is_user_logged_in() && !current_user_can( 'manage_options' ) ):
            $subdomain->set_user_id( get_current_user_id() );
        else:
            $subdomain->set_subdomain($_POST['subdomain']);
        endif;
        $subdomain->set_data($value);

        if( $subdomain->data ):
            $save = $subdomain->update();
        else:
            $save = $subdomain->insert();
        endif;

        $current_shortcode = wpapg_get_subdomain($save);

    endforeach;

    $sbd = isset($current_shortcode->subdomain) ? $current_shortcode->subdomain : sanitize_text_field($_POST['subdomain']);

    wpapg_global_shortcode_save($sbd, $global_value);

    if( is_user_logged_in() && !current_user_can( 'manage_options' ) ):
        $slug = '';
    else:
        $slug = '?subdomain='.$sbd.'#wpapgsList';
    endif;
    ?>
    <script>
    window.location.href = '<?php echo get_the_permalink(); ?><?php echo $slug; ?>'
    </script>
    <?php
}
?>
<form action="" id="wpapgform" method="post" enctype="multipart/form-data" onsubmit="return wpapgFormSubmitt(this);" ajax="<?php echo admin_url('admin-ajax.php'); ?>">
    <div class="wpapg">
        <div class="wpapg-box wpapg-clearfix wpapg-box-<?php echo $style; ?>">
            <div class="labele">
                <label>Sub Domain</label>
            </div>
            <div class="wpapg-input-box">
                <div class="inpute">
                    <?php
                    if( is_user_logged_in() && !current_user_can( 'manage_options' ) ){
                        $user = get_userdata(get_current_user_id());
                        $username = $user->user_login;
                        $preview = $username;
                        $onkeyup = '';
                    }else{
                        $username = '';
                        $preview = 'example';
                        $onkeyup = 'onkeyup="wpapgCheckSubdomain(this);"';
                    }
                    ?>
                    <input <?php echo $onkeyup; ?> type="text" name="subdomain" value="<?php echo $username; ?>" placeholder="example" required <?php if( is_user_logged_in() && !current_user_can( 'manage_options' ) ){echo 'readonly';}?> ajax="<?php echo admin_url('admin-ajax.php'); ?>?action=check-subdomain&nonce=<?php echo wp_create_nonce('noncenonce'); ?>"/>
                    <span></span>
                </div>
                <div class="desc"><?php echo is_ssl() ? 'https://' : 'http://'; ?><span><?php echo $preview; ?></span>.<?php echo $page_domain; ?></div>
            </div>
        </div>
        <?php foreach((array) $global_shortcode as $key=>$val ):

            if( empty($key) )continue;

            $type = isset($val['type']) ? $val['type'] : 'text';
            $value = isset($current_global_shortcode[$key]['value']) ? $current_global_shortcode[$key]['value'] : '';

            $req = $type != 'fb_pixel_id' ? 'required' : '';
            ?>
            <div class="wpapg-box wpapg-clearfix wpapg-box-<?php echo $style; ?>">
                <div class="labele">
                    <label><?php echo $val['label']; ?></label>
                </div>
                <div class="wpapg-input-box">
                    <div class="inpute">
                        <?php if( $type == 'image' ): ?>
                            <input type="file" name="global_value[<?php echo $key; ?>]" accept="image/jpeg, image/png" onchange="wpapgUploader(this);" style="display:none"/>
                            <input type="text" name="global_defvalue[<?php echo $key; ?>]" value="<?php echo $value; ?>"/>
                            <div class="wpapg-upload-button" onclick="wpapgUploaderButton(this);">
                                <svg enable-background="new 0 0 612 612" version="1.1" viewBox="0 0 612 612" xml:space="preserve" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M494.7,255C476.85,168.3,400.35,102,306,102c-73.95,0-137.7,40.8-168.3,102C58.65,214.2,0,277.95,0,357    c0,84.15,68.85,153,153,153h331.5c71.4,0,127.5-56.1,127.5-127.5C612,316.2,558.45,260.1,494.7,255z M357,331.5v102H255v-102    h-76.5L306,204l127.5,127.5H357z"/>
                                </svg>
                            </div>
                        <?php else: ?>
                            <?php
                            $onkeyup = '';
                            if( $key == 'link_affiliate' || $val['type'] == 'link_affiliate' ){
                                $onkeyup = 'onkeyup="wpapgCheckLinkAffiliate(this);" ajax="'.admin_url('admin-ajax.php').'?action=check-link-affiliate&nonce='.wp_create_nonce('noncenonce').'"';
                            }
                            ?>
                            <input type="text" name="global_value[<?php echo $key; ?>]" value="<?php echo $value; ?>" <?php echo $onkeyup; ?> <?php echo $req; ?> placeholder="<?php echo $val['value']; ?>"/>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
        <?php if( $active_pages) : ?>
            <?php
            foreach( (array)$active_pages as $p ):
                $shortcode = get_post_meta($p['ID'], 'wpapg_shortcode', true);

                if( !$shortcode ) continue;
    			$current_shortcode = array();
                $current_shortcode_id = 0;

                if( is_user_logged_in() && !current_user_can( 'manage_options' ) ){

                    $args = array(
                        'user_id' => get_current_user_id(),
                        'page_id' => $p['ID'],
                    );

                    $query = new Wpapg_Subdomain_Query($args);
                    $db = $query->results();

                    $current_shortcode = isset($db[0]) ? $db[0]->data : $current_shortcode;
                    $current_shortcode_id = isset($db[0]) ? $db[0]->ID : $current_shortcode_id;
                }
                ?>
                <div class="wpapg-hr">
                    <?php echo $p['title']; ?>
                    <input type="hidden" name="ID[<?php echo $p['ID']; ?>]" value="<?php echo $current_shortcode_id; ?>">
                </div>
                <div class="wpapg">
                <?php foreach((array) $shortcode as $key=>$val ):

                    if( isset($global_shortcode[$key]) )continue;

                    $type = isset($val['type']) ? $val['type'] : 'text';
                    $value = isset($current_shortcode[$key]['value']) ? $current_shortcode[$key]['value'] : '';

                    $req = $type != 'fb_pixel_id' ? 'required' : '';
                    ?>
                    <div class="wpapg-box wpapg-clearfix wpapg-box-<?php echo $style; ?>">
                        <div class="labele">
                            <label><?php echo $val['label']; ?></label>
                        </div>
                        <div class="wpapg-input-box">
                            <div class="inpute">
                                <?php if( $type == 'image' ): ?>
                                    <input type="file" name="value[<?php echo $p['ID']; ?>][<?php echo $key; ?>]" accept="image/jpeg, image/png" onchange="wpapgUploader(this);" style="display:none"/>
                                    <input type="text" name="defvalue[<?php echo $p['ID']; ?>][<?php echo $key; ?>]" value="<?php echo $value; ?>"/>
                                    <div class="wpapg-upload-button" onclick="wpapgUploaderButton(this);">
                                        <svg enable-background="new 0 0 612 612" version="1.1" viewBox="0 0 612 612" xml:space="preserve" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M494.7,255C476.85,168.3,400.35,102,306,102c-73.95,0-137.7,40.8-168.3,102C58.65,214.2,0,277.95,0,357    c0,84.15,68.85,153,153,153h331.5c71.4,0,127.5-56.1,127.5-127.5C612,316.2,558.45,260.1,494.7,255z M357,331.5v102H255v-102    h-76.5L306,204l127.5,127.5H357z"/>
                                        </svg>
                                    </div>
                                <?php else: ?>
                                    <?php
                                    $onkeyup = '';
                                    if( $key == 'link_affiliate' || $val['type'] == 'link_affiliate' ){
                                        $onkeyup = 'onkeyup="wpapgCheckLinkAffiliate(this);" ajax="'.admin_url('admin-ajax.php').'?action=check-link-affiliate&nonce='.wp_create_nonce('noncenonce').'"';
                                    }
                                    ?>
                                    <input type="text" name="value[<?php echo $p['ID']; ?>][<?php echo $key; ?>]" value="<?php echo $value; ?>" <?php echo $onkeyup; ?> <?php echo $req; ?> placeholder="<?php echo $val['value']; ?>"/>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
                </div>
                <?php
            endforeach;
            ?>
        <?php endif; ?>
    <div class="wpapg">
        <div class="wpapg-box wpapg-clearfix wpapg-submit-box-<?php echo $style; ?>">
            <div class="wpapg-submit-box">
                <input type="hidden" name="__nonce" value="<?php echo wp_create_nonce('noncenonce'); ?>">
                <?php
                if( isset($styles['hover_animation']) ):
                    $button_class = 'elementor-animation-' . $styles['hover_animation'];
                else:
                    $button_class = '';
                endif;
                ?>
                <button class="<?php echo $button_class; ?>" type="submit" name="submito" value="1"><?php echo wpapg_get_option('generator_form_submit_text', 'Generate Page'); ?></button>
            </div>
        </div>
    </div>
</form>
<?php if( $active_pages ): if( is_user_logged_in() && !current_user_can( 'manage_options' ) || isset($_GET['subdomain']) ): ?>
    <?php
    ob_start();
    ?>
    .wpapgs{
        width: 100%;
        -moz-box-sizing: border-box;
    	-webkit-box-sizing: border-box;
    	box-sizing: border-box;
        margin: 20px 0;
    }
    .wpapgs ul.pagelist{
        list-style-type: : none;
        margin: 0 !important;
        padding: 0 !important;
        border: 1px solid rgba(0,0,0,0.1);
    }
    .wpapgs ul.pagelist li {
        list-style-type: none;
        background: #ffffff;
        border-bottom: 1px solid rgba(0,0,0,0.1) !important;
        display: flex;
        align-items: center;
        justify-content: center;
        width: 100%;
        height: 80px;
        position: relative;
    }
    .wpapgs ul.pagelist li:last-child {
        border-bottom: none !important;
    }
    .wpapgs ul.pagelist li:before{
        content: "";
        position: absolute;
        top: 0;
        left: 0;
        width: 0%;
        height: 100%;
        background: rgba(0,0,0,0.1);
        z-index: 1;
        -webkit-transition: all .5s;-moz-transition: all .5s;transition: all .5s;
    }
    .wpapgs ul.pagelist li:hover::before {
    }
    .wpapgs ul.pagelist li .itembox{
        display: block;
        position: relative;
        height: auto;
        width: 100%;
    }
    .wpapgs ul.pagelist li .itembox p{
        display: block;
        width: 100%;
        font-size: 20px;
        line-height: 25px;
        padding: 0 10px;
        color: #111;
        z-index: 2;
        margin: 0 !important;
    }
    .wpapgs ul.pagelist li .itembox p.link{
        width: 100%;
        font-size: 13px;
        line-height: 25px;
        padding: 0 10px;
        color: #666;
        z-index: 3;
        margin: 0 !important;
    }
    .wpapgs ul.pagelist li .iconer{
        height: 35px;
        width: auto;
        position: absolute;
        top: 22.5px;
        right: 5px;
        z-index: 99;
        border: 1px solid #5CA814;
        background: #5CA814;
        color: #ffffff;
        border-radius: 25px;
        font-size: 14px;
        font-weight: bold;
        line-height: 35px;
        text-align: center;
        letter-spacing: 1px;
        padding: 0 20px;
    }
    .copyClick{
        color: green;
        cursor: pointer;
        font-weight: bold;
    }
    <?php
    $css = ob_get_contents();
    ob_end_clean();
    ?>
    <style>
    <?php echo wpapg_minify_css($css); ?>
    </style>
    <div class="wpapgs" id="wpapgsList">
        <ul class="pagelist">
            <?php foreach( (array)$active_pages as $page ): ?>
                <?php
                if( isset($current_subdomains[$page['ID']]) ){
                    $link = wpapg_get_page_permalink($page['ID'], $current_subdomains[$page['ID']]->subdomain);
                }else{
                    $link = false;
                }
                ?>
                <li>
                    <div class="itembox">
                        <p><?php echo $page['title']; ?></p>
                        <?php if( $link ): ?>
                            <p class="link">
                                <?php echo $link; ?>&nbsp;&nbsp;
                                <span class="copyClick" onclick="wpapgCopyToClipboard(this);" data-copy="<?php echo $link; ?>">COPY</span>
                            </p>
                        <?php endif; ?>
                    </div>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif; endif; ?>

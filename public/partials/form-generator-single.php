<?php
?>
<div class="wpapg">
    <form action="" id="wpapgform" method="post" enctype="multipart/form-data" onsubmit="return wpapgFormSubmit(this);"
        ajax="<?php echo admin_url('admin-ajax.php'); ?>">
        <div class="wpapg-box wpapg-clearfix wpapg-box-<?php echo $style; ?>">
            <div class="labele">
                <label>Sub Domain</label>
            </div>
            <div class="wpapg-input-box">
                <div class="inpute">
                    <?php
                    if (is_user_logged_in() && !current_user_can('manage_options')) {
                        $user = get_userdata(get_current_user_id());
                        $username = $user->user_login;
                        $preview = $username;
                    } else {
                        $username = '';
                        $preview = 'example';
                    }
                    ?>
                    <input onkeyup="wpapgCheckSubdomain(this);" type="text" name="subdomain"
                        value="<?php echo $username; ?>" placeholder="example" required
                        <?php if (is_user_logged_in() && !current_user_can('manage_options')) {
                                                                                                                                                                    echo 'readonly';
                                                                                                                                                                } ?>
                        ajax="<?php echo admin_url('admin-ajax.php'); ?>?action=check-subdomain&nonce=<?php echo wp_create_nonce('noncenonce'); ?>&page_id=<?php echo $page_id; ?>" />
                    <span></span>
                </div>
                <div class="desc">
                    <?php echo is_ssl() ? 'https://' : 'http://'; ?><span><?php echo $preview; ?></span>.<?php echo $page_domain; ?>
                </div>
            </div>
        </div>
        <?php foreach ((array) $global_shortcode as $key => $val) :

            if (empty($key)) continue;

            $type = isset($val['type']) ? $val['type'] : 'text';
            $value = isset($current_global_shortcode[$key]['value']) ? $current_global_shortcode[$key]['value'] : '';

            $req = $type != 'fb_pixel_id' ? 'required' : '';
        ?>
        <div class="wpapg-box wpapg-clearfix wpapg-box-<?php echo $style; ?>">
            <div class="labele">
                <label><?php echo $val['label']; ?></label>
            </div>
            <div class="wpapg-input-box">
                <div class="inpute">
                    <?php if ($type == 'image') : ?>
                    <input type="file" name="global_value[<?php echo $key; ?>]" accept="image/jpeg, image/png"
                        onchange="wpapgUploader(this);" style="display:none" />
                    <input type="text" name="global_defvalue[<?php echo $key; ?>]" value="<?php echo $value; ?>" />
                    <div class="wpapg-upload-button" onclick="wpapgUploaderButton(this);">
                        <svg enable-background="new 0 0 612 612" version="1.1" viewBox="0 0 612 612"
                            xml:space="preserve" xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M494.7,255C476.85,168.3,400.35,102,306,102c-73.95,0-137.7,40.8-168.3,102C58.65,214.2,0,277.95,0,357    c0,84.15,68.85,153,153,153h331.5c71.4,0,127.5-56.1,127.5-127.5C612,316.2,558.45,260.1,494.7,255z M357,331.5v102H255v-102    h-76.5L306,204l127.5,127.5H357z" />
                        </svg>
                    </div>
                    <?php else : ?>
                    <?php
                            $onkeyup = '';
                            if ($key == 'link_affiliate' || $val['type'] == 'link_affiliate') {
                                $onkeyup = 'onkeyup="wpapgCheckLinkAffiliate(this);" ajax="' . admin_url('admin-ajax.php') . '?action=check-link-affiliate&nonce=' . wp_create_nonce('noncenonce') . '"';
                            }
                            ?>
                    <input type="text" name="global_value[<?php echo $key; ?>]" value="<?php echo $value; ?>"
                        <?php echo $onkeyup; ?> <?php echo $req; ?> />
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <?php endforeach; ?>
        <?php foreach ((array) $shortcode as $key => $val) :

            if (isset($global_shortcode[$key])) continue;

            $type = isset($val['type']) ? $val['type'] : 'text';
            $value = isset($current_shortcode[$key]['value']) ? $current_shortcode[$key]['value'] : '';

            $req = $type != 'fb_pixel_id' ? 'required' : '';
        ?>
        <div class="wpapg-box wpapg-clearfix wpapg-box-<?php echo $style; ?>">
            <div class="labele">
                <label><?php echo $val['label']; ?></label>
            </div>
            <div class="wpapg-input-box">
                <div class="inpute">
                    <?php if ($type == 'image') : ?>
                    <input type="file" name="value[<?php echo $key; ?>]" accept="image/jpeg, image/png"
                        onchange="wpapgUploader(this);" style="display:none" />
                    <input type="text" name="defvalue[<?php echo $key; ?>]" value="<?php echo $value; ?>" />
                    <div class="wpapg-upload-button" onclick="wpapgUploaderButton(this);">
                        <svg enable-background="new 0 0 612 612" version="1.1" viewBox="0 0 612 612"
                            xml:space="preserve" xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M494.7,255C476.85,168.3,400.35,102,306,102c-73.95,0-137.7,40.8-168.3,102C58.65,214.2,0,277.95,0,357    c0,84.15,68.85,153,153,153h331.5c71.4,0,127.5-56.1,127.5-127.5C612,316.2,558.45,260.1,494.7,255z M357,331.5v102H255v-102    h-76.5L306,204l127.5,127.5H357z" />
                        </svg>
                    </div>
                    <?php else : ?>
                    <?php
                            $onkeyup = '';
                            if ($key == 'link_affiliate' || $val['type'] == 'link_affiliate') {
                                $onkeyup = 'onkeyup="wpapgCheckLinkAffiliate(this);" ajax="' . admin_url('admin-ajax.php') . '?action=check-link-affiliate&nonce=' . wp_create_nonce('noncenonce') . '"';
                            }
                            ?>
                    <input type="text" name="value[<?php echo $key; ?>]" value="<?php echo $value; ?>"
                        <?php echo $onkeyup; ?> <?php echo $req; ?> /><span></span>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <?php endforeach; ?>
        <div class="wpapg-box wpapg-clearfix wpapg-submit-box-<?php echo $style; ?>">
            <div class="wpapg-submit-box">
                <input type="hidden" name="__nonce" value="<?php echo wp_create_nonce('noncenonce'); ?>">
                <input type="hidden" name="ID" value="<?php echo $current_shortcode_id; ?>">
                <input type="hidden" name="page_id" value="<?php echo $page_id; ?>">
                <?php
                if (isset($styles['hover_animation'])) :
                    $button_class = 'elementor-animation-' . $styles['hover_animation'];
                else :
                    $button_class = '';
                endif;
                ?>
                <button class="<?php echo $button_class; ?>" type="submit" name="submito"
                    value="1"><?php echo wpapg_get_option('generator_form_submit_text', 'Generate Page'); ?></button>
            </div>
        </div>
    </form>
    <div class="wpapg-result">
        <div class="wpapg-result-action">
            <a>
                <button class="wpapgcopylink" onclick="wpapgCopyToClipboard(this);" data-copy="">Copy Link</button>
            </a>
            <a href="" target="_blank" class="wpapgfollowlink">
                <button>Follow Link</button>
            </a>
        </div>
    </div>
</div>
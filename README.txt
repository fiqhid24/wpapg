=== Affiliate Page Generator ===
Contributors: fiqhid24
Donate link:
Tags: affiliate, page generator
Requires at least: 5.0
Tested up to: 5.5.1
Stable tag: 1.0.0
Requires PHP: 7+
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Generate affiliate page with easly

== Description ==

Plugin wordpress to generate subdomain affiliate page on easy way.

== Installation ==

1. Upload the plugin files to the `/wp-content/plugins/plugin-name` directory, or install the plugin through the WordPress plugins screen directly.
2. Activate the plugin through the 'Plugins' screen in WordPress
3. Use the Settings->Affiliate Page Generator screen to configure the plugin


== Frequently Asked Questions ==

== Screenshots ==

1. This screen shot description corresponds to screenshot-1.(png|jpg|jpeg|gif). Note that the screenshot is taken from
the /assets directory or the directory that contains the stable readme.txt (tags or trunk). Screenshots in the /assets
directory take precedence. For example, `/assets/screenshot-1.png` would win over `/tags/4.3/screenshot-1.png`
(or jpg, jpeg, gif).
2. This is the second screen shot

== Changelog ==
= 1.8.5 =
* [FIX] Fixing various bug crahsed with elementor version 3.6.0

= 1.8.4 =
* [UPD] Update sanitizing data to make working wit space encode on url string

= 1.8.3 =
* [FIX] Fixing bug error when addon domain added

= 1.8.2 =
* [FIX] Fixing APG image elementor widget can't change align

= 1.8.1 =
* [FIX] bug on update sistem

= 1.8.0 =
* [FIX] small bug

= 1.2.7 =
* [IMP] Improve leaderboard conver shortcode
* [FIX] fixing addon domain query

= 1.2.6 =
* [FIX] Fixed Image uploader error in wp admin

= 1.2.5 =
* [FIX] Fixed white space on top when link_affiliate shortcode enable

= 1.2.4 =
* [FIX] Improve shortcode replacer

= 1.2.3 =
* [FIX] Fixing bug generate shortcode on all active pages

= 1.2.2 =
* [FIX] Fixing small bug on import template

= 1.2.1 =
* [FIX] Fixing bug php notice in admin area
* [ADD] Import template processing loader
* [IMP] No require field for pixel id field

= 1.2.0 =
* [IMP] Improve leaderboard feature
* [ADD] Input facebook pixel id

= 1.1.7 =
* [FIX] Fixing bug on check domain ajax

= 1.1.6 =
* [FIX] Fixing bug on elementor saved.

= 1.1.5 =
* [FIX] Fixing undefined page id in leaderboard Builder
* [FIX] Fixing not working pagination on subdomain list

= 1.1.4 =
* [IMP] Improve leaderboard shortcode

= 1.1.3 =
* [DEL] Delete filter strtolower on add shortcode data

= 1.1.2 =
* [FIX] Fixing bug check connected addon domain

= 1.1.1 =
* [FIX] Fixing bug set page on addon domain

= 1.1.0 =
* [FIX] Make addon addon domain allow on all page
* [FIX] Disable filter username if wuoymembership active
* [IMP] Improve some various feature
* [ADD] apply remote elementor template source

= 1.0.6 =
* [FIX] Fixing bug error page list on add new or edit addon domain
* [IMP] Improve some various feature

= 1.0.5 =
* [FIX] Fixing bug filter content wp-admin

= 1.0.4 =
* [FIX] Fixing bug beberapa opsi pengaturan tidak muncul di elemntor widget form generator
* [FIX] Leaderboard metabox bug
* [IMP] Improve design style form generator
* [FIX] Fixing various small bug

= 1.0.0 =
* First Public Release

== Upgrade Notice ==

= 1.0.0 =
-

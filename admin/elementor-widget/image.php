<?php

if (!defined('ABSPATH')) exit;

use \Elementor\Widget_Base;
use \Elementor\Controls_Manager;
use \Elementor\Utils;
use \Elementor\Group_Control_Box_Shadow;
use \Elementor\Plugin;
use \Elementor\Group_Control_Typography;
use \Elementor\Group_Control_Border;

class Wpapg_Image_Shortcode_Widget extends Widget_Base
{

	/**
	 * Get widget name.
	 *
	 * Retrieve image widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name()
	{
		return 'apgimaheshortcode';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve image widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title()
	{
		return __('Image From Shortcode', 'elementor');
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve image widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon()
	{
		return 'eicon-image';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the image widget belongs to.
	 *
	 * Used to determine where to display the widget in the editor.
	 *
	 * @since 2.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories()
	{
		return ['apg'];
	}

	private function get_post_shortcode()
	{
		global $wpapg;

		$lb = isset($wpapg['leaderboard']) ? $wpapg['leaderboard'] : array();

		$shortcode = get_option('wpapg_global_shortcode');
		if (get_post_meta(get_the_ID(), 'wpapg_shortcode_enable', true) == 1) :
			$shortcode = wp_parse_args(get_post_meta(get_the_ID(), 'wpapg_shortcode', true), $shortcode);
		endif;
		$image_shortcode = array();

		foreach ((array)$shortcode as $key => $val) :
			if (isset($val['type']) && $val['type'] != 'image') continue;
			if (!isset($val['value'])) continue;
			$image_shortcode[$key] = $val['value'];
		endforeach;

		if (get_post_meta(get_the_ID(), 'wpapg_leaderboard_enable', true) == 1) :
			$rank = 1;
			while ($rank < 11) :
				$key = 'rank_' . $rank . '_photo';
				$image_shortcode[$key] = isset($lb[$key]) && $lb[$key] ? $lb[$key] : Utils::get_placeholder_image_src();
				$rank++;
			endwhile;
		endif;

		return $image_shortcode;
	}

	/**
	 * Register image widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function register_controls()
	{

		$shortcode_options = array();
		foreach ((array) $this->get_post_shortcode() as $key => $val) :
			$shortcode_options[$key] = '{{' . $key . '}}';
		endforeach;

		$this->start_controls_section(
			'section_image',
			[
				'label' => __('Image', 'elementor'),
			]
		);

		if (empty($shortcode_options)) :
			$this->add_control(
				'image_shortcode_note',
				[
					'type' => Controls_Manager::RAW_HTML,
					'raw' => 'No Image shortcode available in your post please create it first!',
				]
			);
			$this->add_control(
				'image_shortcode',
				[
					'type' => Controls_Manager::HIDDEN,
					'default' => '',
				]
			);
		else :
			$this->add_control(
				'image_shortcode',
				[
					'label' => __('Image Shortcode', 'elementor'),
					'type' => Controls_Manager::SELECT,
					'default' => '',
					'label_block' => true,
					'options' => $shortcode_options,
				]
			);
		endif;

		$this->add_control(
			'image_default',
			[
				'type' => Controls_Manager::HIDDEN,
				'default' => Utils::get_placeholder_image_src(),
			]
		);

		$this->add_control(
			'shortcode',
			[
				'type' => Controls_Manager::HIDDEN,
				'default' => $this->get_post_shortcode(),
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_style_image',
			[
				'label' => __('Image', 'elementor'),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'width',
			[
				'label' => __('Width', 'elementor'),
				'type' => Controls_Manager::SLIDER,
				'default' => [
					'unit' => 'px',
				],
				'tablet_default' => [
					'unit' => 'px',
				],
				'mobile_default' => [
					'unit' => 'px',
				],
				'size_units' => ['px'],
				'range' => [
					'px' => [
						'min' => 1,
						'max' => 1000,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .elementor-image img' => 'width: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'height',
			[
				'label' => __('Height', 'elementor'),
				'type' => Controls_Manager::SLIDER,
				'default' => [
					'unit' => 'px',
				],
				'tablet_default' => [
					'unit' => 'px',
				],
				'mobile_default' => [
					'unit' => 'px',
				],
				'size_units' => ['px'],
				'range' => [
					'px' => [
						'min' => 1,
						'max' => 1000,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .elementor-image img' => 'height: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'align',
			[
				'label' => __('Alignment', 'elementor'),
				'type' => Controls_Manager::CHOOSE,
				'options' => [
					'left' => [
						'title' => __('Left', 'elementor'),
						'icon' => 'fa fa-align-left',
					],
					'center' => [
						'title' => __('Center', 'elementor'),
						'icon' => 'fa fa-align-center',
					],
					'right' => [
						'title' => __('Right', 'elementor'),
						'icon' => 'fa fa-align-right',
					],
				],
				'selectors' => [
					'{{WRAPPER}}' => 'text-align: {{VALUE}};',
				],
			]
		);

		// $this->add_control(
		// 	'separator_panel_style',
		// 	[
		// 		'type' => Controls_Manager::DIVIDER,
		// 		'style' => 'thick',
		// 	]
		// );

		$this->start_controls_tabs('image_effects');

		$this->start_controls_tab(
			'normal',
			[
				'label' => __('Normal', 'elementor'),
			]
		);

		$this->add_control(
			'opacity',
			[
				'label' => __('Opacity', 'elementor'),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'max' => 1,
						'min' => 0.10,
						'step' => 0.01,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .elementor-image img' => 'opacity: {{SIZE}};',
				],
			]
		);

		// $this->add_group_control(
		// 	Group_Control_Css_Filter::get_type(),
		// 	[
		// 		'name' => 'css_filters',
		// 		'selector' => '{{WRAPPER}} .elementor-image img',
		// 	]
		// );

		$this->end_controls_tab();

		$this->start_controls_tab(
			'hover',
			[
				'label' => __('Hover', 'elementor'),
			]
		);

		$this->add_control(
			'opacity_hover',
			[
				'label' => __('Opacity', 'elementor'),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'max' => 1,
						'min' => 0.10,
						'step' => 0.01,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .elementor-image:hover img' => 'opacity: {{SIZE}};',
				],
			]
		);

		// $this->add_group_control(
		// 	Group_Control_Css_Filter::get_type(),
		// 	[
		// 		'name' => 'css_filters_hover',
		// 		'selector' => '{{WRAPPER}} .elementor-image:hover img',
		// 	]
		// );

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name' => 'image_border',
				'selector' => '{{WRAPPER}} .elementor-image img',
				'separator' => 'before',
			]
		);

		$this->add_responsive_control(
			'image_border_radius',
			[
				'label' => __('Border Radius', 'elementor'),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => ['px', '%'],
				'selectors' => [
					'{{WRAPPER}} .elementor-image img' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'image_box_shadow',
				'exclude' => [
					'box_shadow_position',
				],
				'selector' => '{{WRAPPER}} .elementor-image img',
			]
		);

		$this->end_controls_section();
	}

	/**
	 * Render image widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render()
	{
		$settings = $this->get_settings();

		if (Plugin::$instance->editor->is_edit_mode()) {

			if ($settings['image_shortcode']) :

				$code = $settings['image_shortcode'];
				$shortcode = $settings['shortcode'];

				$image_url = isset($shortcode[$code]) ? $shortcode[$code] : false;

				if (!$image_url) :
					$image_url = $settings['image_default'];
				endif;

			else :
				$image_url = $settings['image_default'];
			endif;
		} else {
			$image_url = '{{' . $settings['image_shortcode'] . '}}';
		}

		$this->add_render_attribute('wrapper', 'class', 'elementor-image');

?>
		<div <?php echo $this->get_render_attribute_string('wrapper'); ?>>
			<img src="<?php echo $image_url; ?>" title="" alt="" style="object-fit: cover;display:inline-block">
		</div>
	<?php
	}

	/**
	 * Render image widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function content_template()
	{
	?>
		<# if ( settings.image_default ) { var image_url=settings.image_default; var shortcode=settings.shortcode; var image_shortcode=settings.image_shortcode; console.log(shortcode); console.log(shortcode[image_shortcode]); if ( typeof shortcode[image_shortcode] !='undefined' ) { image_url=shortcode[image_shortcode]; } #>
			<div class="elementor-image">
				<# #><img src="{{ image_url }}" title="" alt="" style="object-fit: cover;display:inline-block" />
					<# #>
			</div>
			<# } #>
		<?php
	}
}

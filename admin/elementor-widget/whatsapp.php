<?php


if (!defined('ABSPATH')) exit; // Exit if accessed directly

use \Elementor\Widget_Base;
use \Elementor\Controls_Manager;
use \Elementor\Utils;
use \Elementor\Group_Control_Box_Shadow;
use \Elementor\Plugin;
use \Elementor\Group_Control_Typography;
use \Elementor\Group_Control_Border;

class Wpapg_Whatsapp_Shortcode_Widget extends Widget_Base
{

	public function get_name()
	{
		return 'apgwhatsappshortcode';
	}

	public function get_title()
	{
		return __('Whatsapp From Shortcode', 'elementor');
	}

	public function get_icon()
	{
		return 'eicon-button';
	}

	public function get_categories()
	{
		return ['apg'];
	}

	public static function get_button_sizes()
	{
		return [
			'xs' => __('Extra Small', 'elementor'),
			'sm' => __('Small', 'elementor'),
			'md' => __('Medium', 'elementor'),
			'lg' => __('Large', 'elementor'),
			'xl' => __('Extra Large', 'elementor'),
		];
	}

	private function get_post_shortcode()
	{
		$global_shortcode = get_option('wpapg_global_shortcode');
		$shortcode = wp_parse_args(get_post_meta(get_the_ID(), 'wpapg_shortcode', true), $global_shortcode);
		$image_shortcode = array();

		foreach ((array)$shortcode as $key => $val) :
			if ($val['type'] != 'text') continue;
			$image_shortcode[$key] = $val['value'];
		endforeach;

		return $image_shortcode;
	}

	protected function register_controls()
	{

		$shortcode_options = array();
		foreach ((array) $this->get_post_shortcode() as $key => $val) :
			$shortcode_options[$key] = '{{' . $key . '}}';
		endforeach;

		$this->start_controls_section(
			'section_button',
			[
				'label' => __('Button', 'elementor'),
			]
		);

		$this->add_control(
			'text',
			[
				'label' => __('Text', 'elementor'),
				'type' => Controls_Manager::TEXT,
				'default' => __('Message Me On Whatsapp', 'elementor'),
				'placeholder' => __('Message Me On Whatsapp', 'elementor'),
				'label_block' => true,
			]
		);

		if (empty($shortcode_options)) :
			$this->add_control(
				'phone_shortcode_note',
				[
					'type' => Controls_Manager::RAW_HTML,
					'raw' => 'No Text shortcode available in your post please create it first!',
				]
			);
			$this->add_control(
				'phone',
				[
					'type' => Controls_Manager::HIDDEN,
					'default' => '',
				]
			);
		else :
			$this->add_control(
				'phone',
				[
					'label' => __('Phone Shortcode', 'elementor'),
					'type' => Controls_Manager::SELECT,
					'default' => '',
					'label_block' => true,
					'options' => $shortcode_options,
				]
			);
		endif;

		$this->add_control(
			'target',
			[
				'label' => __('Open in new window', 'elementor'),
				'type' => Controls_Manager::SWITCHER,
				'label_on' => __('Yes', 'elementor'),
				'label_off' => __('No', 'elementor'),
				'return_value' => 'yes',
				'default' => '',
			]
		);

		$this->add_control(
			'message',
			[
				'label' => __('Message', 'elementor'),
				'type' => Controls_Manager::TEXTAREA,
				'rows' => '2',
				'default' => 'Halo, saya tertarik dengan produk ini. Terimakasih.',
				'placeholder' => 'Halo, saya tertarik dengan produk ini. Terimakasih.',
				'label_block' => true,
			]
		);

		$this->add_responsive_control(
			'align',
			[
				'label' => __('Alignment', 'elementor'),
				'type' => Controls_Manager::CHOOSE,
				'options' => [
					'left'    => [
						'title' => __('Left', 'elementor'),
						'icon' => 'fa fa-align-left',
					],
					'center' => [
						'title' => __('Center', 'elementor'),
						'icon' => 'fa fa-align-center',
					],
					'right' => [
						'title' => __('Right', 'elementor'),
						'icon' => 'fa fa-align-right',
					],
					'justify' => [
						'title' => __('Justified', 'elementor'),
						'icon' => 'fa fa-align-justify',
					],
				],
				'prefix_class' => 'elementor%s-align-',
				'default' => '',
			]
		);

		$this->add_control(
			'size',
			[
				'label' => __('Size', 'elementor'),
				'type' => Controls_Manager::SELECT,
				'default' => 'sm',
				'options' => self::get_button_sizes(),
			]
		);

		$this->add_control(
			'icon_show',
			[
				'label' => __('Show Whatsapp Icon', 'elementor'),
				'type' => Controls_Manager::SWITCHER,
				'label_on' => __('Show', 'elementor'),
				'label_off' => __('Hide', 'elementor'),
				'return_value' => 'yes',
				'default' => 'yes',
				'separator' => 'before',
			]
		);

		$this->add_control(
			'icon_size',
			[
				'label' => __('Icon Size', 'elementor'),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 10,
						'max' => 200,
					],
				],
				'condition' => [
					'icon_show!' => '',
				],
				'selectors' => [
					'{{WRAPPER}} .elementor-button .elementor-button-icon svg' => 'width: {{SIZE}}{{UNIT}}; height: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'icon_align',
			[
				'label' => __('Icon Position', 'elementor'),
				'type' => Controls_Manager::SELECT,
				'default' => 'left',
				'options' => [
					'left' => __('Before', 'elementor'),
					'right' => __('After', 'elementor'),
				],
				'condition' => [
					'icon_show!' => '',
				],
			]
		);

		$this->add_control(
			'icon_indent',
			[
				'label' => __('Icon Spacing', 'elementor'),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'max' => 50,
					],
				],
				'condition' => [
					'icon_show!' => '',
				],
				'selectors' => [
					'{{WRAPPER}} .elementor-button .elementor-align-icon-right' => 'margin-left: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .elementor-button .elementor-align-icon-left' => 'margin-right: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'view',
			[
				'label' => __('View', 'elementor'),
				'type' => Controls_Manager::HIDDEN,
				'default' => 'traditional',
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_sticky',
			[
				'label' => __('Sticky / Floating Button', 'elementor'),
			]
		);

		$this->add_control(
			'floating',
			[
				'label' => __('Sticky / Floating', 'elementor'),
				'type' => Controls_Manager::SWITCHER,
				'default' => '',
				'label_on' => __('Yes', 'elementor'),
				'label_off' => __('No', 'elementor'),
				'return_value' => 'yes',
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_sticky_style',
			[
				'label' => __('Sticky / Floating Button Container', 'elementor'),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [
					'floating!' => '',
				],
			]
		);

		$this->add_control(
			'floating_bg_color',
			[
				'label' => __('Background Color', 'elementor'),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .elementor-button-sticky-yes' => 'background-color: {{VALUE}};',
				],
				'condition' => [
					'floating!' => '',
				],
			]
		);

		$this->add_control(
			'floating_padding',
			[
				'label' => __('Padding', 'elementor'),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => ['px', 'em', '%'],
				'selectors' => [
					'{{WRAPPER}} .elementor-button-sticky-yes' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'condition' => [
					'floating!' => '',
				],
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_style',
			[
				'label' => __('Button', 'elementor'),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'typography',
				'label' => __('Typography', 'elementor'),
				'selector' => '{{WRAPPER}} .elementor-button',
			]
		);

		$this->start_controls_tabs('tabs_button_style');

		$this->start_controls_tab(
			'tab_button_normal',
			[
				'label' => __('Normal', 'elementor'),
			]
		);

		$this->add_control(
			'button_text_color',
			[
				'label' => __('Text Color', 'elementor'),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .elementor-button' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'background_color',
			[
				'label' => __('Background Color', 'elementor'),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .elementor-button' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab(
			'tab_button_hover',
			[
				'label' => __('Hover', 'elementor'),
			]
		);

		$this->add_control(
			'hover_color',
			[
				'label' => __('Text Color', 'elementor'),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .elementor-button:hover' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'button_background_hover_color',
			[
				'label' => __('Background Color', 'elementor'),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .elementor-button:hover' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'button_hover_border_color',
			[
				'label' => __('Border Color', 'elementor'),
				'type' => Controls_Manager::COLOR,
				'condition' => [
					'border_border!' => '',
				],
				'selectors' => [
					'{{WRAPPER}} .elementor-button:hover' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'hover_animation',
			[
				'label' => __('Animation', 'elementor'),
				'type' => Controls_Manager::HOVER_ANIMATION,
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name' => 'border',
				'label' => __('Border', 'elementor'),
				'placeholder' => '1px',
				'default' => '1px',
				'selector' => '{{WRAPPER}} .elementor-button',
			]
		);

		$this->add_control(
			'border_radius',
			[
				'label' => __('Border Radius', 'elementor'),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => ['px', '%'],
				'selectors' => [
					'{{WRAPPER}} .elementor-button' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'text_padding',
			[
				'label' => __('Text Padding', 'elementor'),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => ['px', 'em', '%'],
				'selectors' => [
					'{{WRAPPER}} .elementor-button' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->end_controls_section();
	}

	protected function render()
	{
		$settings = $this->get_settings();

		$this->add_render_attribute('wrapper', 'class', 'elementor-button-wrapper');

		if (!empty($settings['phone'])) {
			$phone = $settings['phone'];
			$link = 'https://api.whatsapp.com/send?phone={{' . $phone . '}}';
			if (!empty($settings['message'])) {
				$link .= '&text=' . rawurlencode($settings['message']);
			}

			$this->add_render_attribute('button', 'href', $link);

			if ($settings['target'] == 'yes') {
				$this->add_render_attribute('button', 'target', '_blank');
			}
			$this->add_render_attribute('button', 'class', 'elementor-button-link');
		}

		$this->add_render_attribute('button', 'class', 'elementor-button');

		if (!empty($settings['size'])) {
			$size_to_replace = [
				'small' => 'xs',
				'medium' => 'sm',
				'large' => 'md',
				'xl' => 'lg',
				'xxl' => 'xl',
			];
			$old_size = $settings['size'];
			if (isset($size_to_replace[$old_size])) {
				$settings['size'] = $size_to_replace[$old_size];
			}
			$this->add_render_attribute('button', 'class', 'elementor-size-' . $settings['size']);
		}


		if ($settings['hover_animation']) {
			$this->add_render_attribute('button', 'class', 'elementor-animation-' . $settings['hover_animation']);
		}

		if ($settings['floating']) {
			$this->add_render_attribute('wrapper', 'class', 'elementor-button-sticky-' . $settings['floating']);
		}

		$this->add_render_attribute('content-wrapper', 'class', 'elementor-button-content-wrapper');
		$this->add_render_attribute('icon-align', 'class', 'elementor-align-icon-' . $settings['icon_align']);
		$this->add_render_attribute('icon-align', 'class', 'elementor-button-icon');
?>
		<div <?php echo $this->get_render_attribute_string('wrapper'); ?>>
			<a <?php echo $this->get_render_attribute_string('button'); ?>>
				<span <?php echo $this->get_render_attribute_string('content-wrapper'); ?>>
					<?php if ($settings['icon_show']) : ?>
						<span <?php echo $this->get_render_attribute_string('icon-align'); ?>>
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="20" height="21" viewBox="0 0 1219.547 1225.016" style="margin-top:-4px">
								<path fill="#E0E0E0" d="M1041.858 178.02C927.206 63.289 774.753.07 612.325 0 277.617 0 5.232 272.298 5.098 606.991c-.039 106.986 27.915 211.42 81.048 303.476L0 1225.016l321.898-84.406c88.689 48.368 188.547 73.855 290.166 73.896h.258.003c334.654 0 607.08-272.346 607.222-607.023.056-162.208-63.052-314.724-177.689-429.463zm-429.533 933.963h-.197c-90.578-.048-179.402-24.366-256.878-70.339l-18.438-10.93-191.021 50.083 51-186.176-12.013-19.087c-50.525-80.336-77.198-173.175-77.16-268.504.111-278.186 226.507-504.503 504.898-504.503 134.812.056 261.519 52.604 356.814 147.965 95.289 95.36 147.728 222.128 147.688 356.948-.118 278.195-226.522 504.543-504.693 504.543z" />
								<linearGradient id="a" gradientUnits="userSpaceOnUse" x1="609.77" y1="1190.114" x2="609.77" y2="21.084">
									<stop offset="0" stop-color="#20b038" />
									<stop offset="1" stop-color="#60d66a" />
								</linearGradient>
								<path fill="url(#a)" d="M27.875 1190.114l82.211-300.18c-50.719-87.852-77.391-187.523-77.359-289.602.133-319.398 260.078-579.25 579.469-579.25 155.016.07 300.508 60.398 409.898 169.891 109.414 109.492 169.633 255.031 169.57 409.812-.133 319.406-260.094 579.281-579.445 579.281-.023 0 .016 0 0 0h-.258c-96.977-.031-192.266-24.375-276.898-70.5l-307.188 80.548z" />
								<image overflow="visible" opacity=".08" width="682" height="639" xlink:href="FCC0802E2AF8A915.png" transform="translate(270.984 291.372)" />
								<path fill-rule="evenodd" clip-rule="evenodd" fill="#FFF" d="M462.273 349.294c-11.234-24.977-23.062-25.477-33.75-25.914-8.742-.375-18.75-.352-28.742-.352-10 0-26.25 3.758-39.992 18.766-13.75 15.008-52.5 51.289-52.5 125.078 0 73.797 53.75 145.102 61.242 155.117 7.5 10 103.758 166.266 256.203 226.383 126.695 49.961 152.477 40.023 179.977 37.523s88.734-36.273 101.234-71.297c12.5-35.016 12.5-65.031 8.75-71.305-3.75-6.25-13.75-10-28.75-17.5s-88.734-43.789-102.484-48.789-23.75-7.5-33.75 7.516c-10 15-38.727 48.773-47.477 58.773-8.75 10.023-17.5 11.273-32.5 3.773-15-7.523-63.305-23.344-120.609-74.438-44.586-39.75-74.688-88.844-83.438-103.859-8.75-15-.938-23.125 6.586-30.602 6.734-6.719 15-17.508 22.5-26.266 7.484-8.758 9.984-15.008 14.984-25.008 5-10.016 2.5-18.773-1.25-26.273s-32.898-81.67-46.234-111.326z" />
								<path fill="#FFF" d="M1036.898 176.091C923.562 62.677 772.859.185 612.297.114 281.43.114 12.172 269.286 12.039 600.137 12 705.896 39.633 809.13 92.156 900.13L7 1211.067l318.203-83.438c87.672 47.812 186.383 73.008 286.836 73.047h.255.003c330.812 0 600.109-269.219 600.25-600.055.055-160.343-62.328-311.108-175.649-424.53zm-424.601 923.242h-.195c-89.539-.047-177.344-24.086-253.93-69.531l-18.227-10.805-188.828 49.508 50.414-184.039-11.875-18.867c-49.945-79.414-76.312-171.188-76.273-265.422.109-274.992 223.906-498.711 499.102-498.711 133.266.055 258.516 52 352.719 146.266 94.195 94.266 146.031 219.578 145.992 352.852-.118 274.999-223.923 498.749-498.899 498.749z" />
							</svg>
						</span>
					<?php endif; ?>
					<span class="elementor-button-text"><?php echo $settings['text']; ?></span>
				</span>
			</a>
		</div>
<?php
	}

	protected function content_template()
	{
	}
}

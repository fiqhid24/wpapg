<?php

if (!defined('ABSPATH')) exit; // Exit if accessed directly

use \Elementor\Widget_Base;
use \Elementor\Controls_Manager;
use \Elementor\Utils;
use \Elementor\Group_Control_Box_Shadow;
use \Elementor\Plugin;
use \Elementor\Group_Control_Typography;

class Wpapg_Form_Generator_Widget extends Widget_Base
{

	public function get_name()
	{
		return 'apgformgenerator';
	}

	public function get_title()
	{
		return __('Form Generator', 'elementor');
	}

	public function get_icon()
	{
		return 'eicon-form-vertical';
	}

	public function get_categories()
	{
		return ['apg'];
	}

	private function list_active_pages()
	{
		//list active pages
		$wpapgpages = \Wpapg_Subdomain::active_pages();
		$active_pages = array(
			'0' => 'List active Landingpages',
		);
		foreach ((array)$wpapgpages as $page) :
			if (!isset($page['ID'])) continue;
			$active_pages[$page['ID']] = $page['title'];
		endforeach;

		return $active_pages;
	}

	protected function register_controls()
	{

		$this->start_controls_section(
			'section_form',
			[
				'label' => __('Form', 'elementor')
			]
		);

		$this->add_control(
			'form_page',
			[
				'label'   => __('Landingpage', 'elementor'),
				'type'    => Controls_Manager::SELECT,
				'options' => $this->list_active_pages(),
				'default' => '0',

			]
		);

		$this->add_control(
			'form_design',
			[
				'label' => __('Design', 'elementor'),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'block'  => __('Block', 'elementor'),
					'inline' => __('Inline', 'elementor'),
					'table'  => __('Table', 'elementor'),
				],
				'default' => 'table',
				// 'condition' => [
				// 	'form_page!' => '0',
				// ]

			]
		);

		$this->end_controls_section();

		// $this->start_controls_section(
		// 	'section_form_list_style',
		// 	[
		// 		'label' => __( 'List', 'elementor' ),
		// 		'tab' => Controls_Manager::TAB_STYLE,
		//         'condition' => [
		// 			'form_page' => '0',
		// 		],
		// 	]
		// );
		//
		// $this->add_control(
		// 	'form_list_color',
		// 	[
		// 		'label' => __( 'Text Color', 'elementor' ),
		// 		'type' => Controls_Manager::COLOR,
		// 		'default' => '#ffffff',
		// 		'selectors' => [
		// 			'{{WRAPPER}} .mte-optin-1 input[type=text]' => 'color: {{VALUE}} !important;',
		// 			'{{WRAPPER}} .mte-optin-2 input[type=text]' => 'color: {{VALUE}} !important;',
		// 			'{{WRAPPER}} .mte-optin-3 input[type=text]' => 'color: {{VALUE}} !important;',
		// 			'.mte-optin-'.$this->get_id().' .mte-optin-1 input[type=text]' => 'color: {{VALUE}} !important;',
		// 			'.mte-optin-'.$this->get_id().' .mte-optin-2 input[type=text]' => 'color: {{VALUE}} !important;',
		// 			'.mte-optin-'.$this->get_id().' .mte-optin-3 input[type=text]' => 'color: {{VALUE}} !important;',
		// 		]
		// 	]
		// );
		//
		// $this->add_group_control(
		// 	Group_Control_Typography::get_type(),
		// 	[
		// 		'name' => 'form_list_typography',
		// 		'selector' => '{{WRAPPER}} .mte-optin-1 input[type=text], {{WRAPPER}} .mte-optin-2 input[type=text], {{WRAPPER}} .mte-optin-3 input[type=text], .mte-optin-'.$this->get_id().' .mte-optin-1 input[type=text], .mte-optin-'.$this->get_id().' .mte-optin-2 input[type=text], .mte-optin-'.$this->get_id().' .mte-optin-3 input[type=text]'
		// 	]
		// );
		//
		// $this->end_controls_section();

		$this->start_controls_section(
			'section_field_box_style',
			[
				'label' => __('Field Box', 'elementor'),
				'tab' => Controls_Manager::TAB_STYLE,
				// 'condition' => [
				// 	'form_page!' => '0',
				// ],
			]
		);

		$this->add_responsive_control(
			'form_field_box_spacing',
			[
				'label' => __('Spacing', 'elementor'),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .wpapg-box' => 'margin-bottom: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_field_input_style',
			[
				'label' => __('Field Input', 'elementor'),
				'tab' => Controls_Manager::TAB_STYLE,
				// 'condition' => [
				// 	'form_page!' => '0',
				// ],
			]
		);

		$this->add_control(
			'form_field_input_color',
			[
				'label' => __('Text Color', 'elementor'),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .wpapg-box .wpapg-input-box .inpute input' => 'color: {{VALUE}} !important;',
				]
			]
		);

		$this->add_control(
			'form_field_input_font_size',
			[
				'label' => __('Font Size', 'elementor'),
				'type' => Controls_Manager::SLIDER,
				'selectors' => [
					'{{WRAPPER}} .wpapg-box .wpapg-input-box .inpute input' => 'font-size: {{SIZE}}{{UNIT}} !important;',
				],
				'size_units' => ['px'],
				'range' => [
					'px' => [
						'min' => 10,
						'max' => 30,
						'step' => 1,
					]
				]
			]
		);

		$this->add_control(
			'form_field_input_background_color',
			[
				'label' => __('Background Color', 'elementor'),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .wpapg-box .wpapg-input-box .inpute input' => 'background-color: {{VALUE}} !important;',
				],
				'separator' => 'before',
			]
		);

		$this->add_control(
			'form_field_input_border_color',
			[
				'label' => __('Border Color', 'elementor'),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .wpapg-box .wpapg-input-box .inpute input' => 'border-color: {{VALUE}} !important;',
				],
				'separator' => 'before',
			]
		);

		$this->add_control(
			'form_field_input_border_width',
			[
				'label' => __('Border  Width', 'elementor'),
				'type' => Controls_Manager::DIMENSIONS,
				'placeholder' => '0',
				'size_units' => ['px'],
				'selectors' => [
					'{{WRAPPER}} .wpapg-box .wpapg-input-box .inpute input' => 'border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}} !important;',
				],
			]
		);

		$this->add_control(
			'form_field_input_border_radius',
			[
				'label' => __('Border Radius', 'elementor'),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => ['px', '%'],
				'selectors' => [
					'{{WRAPPER}} .wpapg-box .wpapg-input-box .inpute input' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}} !important;',
					'{{WRAPPER}} .wpapg-box .wpapg-input-box .inpute .wpapg-upload-button' => 'border-radius: 0 {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} 0 !important;',
				],
			]
		);

		$this->add_control(
			'form_field_input_padding',
			[
				'label' => __('Padding', 'elementor'),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => ['px'],
				'selectors' => [
					'{{WRAPPER}} .wpapg-box .wpapg-input-box .inpute input' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}} !important;',
				],
			]
		);

		$this->add_responsive_control(
			'form_field_input_spacing',
			[
				'label' => __('Spacing', 'elementor'),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .wpapg-box .labele' => 'margin-bottom: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_form_label_style',
			[
				'label' => __('Label', 'elementor'),
				'tab' => Controls_Manager::TAB_STYLE,
				// 'condition' => [
				// 	'form_page!' => '0',
				// ],
			]
		);

		$this->add_control(
			'form_label_text_color',
			[
				'label' => __('Text Color', 'elementor'),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .wpapg-box .labele label' => 'color: {{VALUE}} !important;',
				]
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'form_label_typography',
				'selector' => '{{WRAPPER}} .wpapg-box .labele label'
			]
		);

		$this->add_control(
			'form_label_align',
			[
				'label' => __('Alignment', 'elementor'),
				'type' => Controls_Manager::CHOOSE,
				'options' => [
					'left' => [
						'title' => __('Left', 'elementor'),
						'icon' => 'fa fa-align-left',
					],
					'center' => [
						'title' => __('Center', 'elementor'),
						'icon' => 'fa fa-align-center',
					],
					'right' => [
						'title' => __('Right', 'elementor'),
						'icon' => 'fa fa-align-right',
					],
					'justify' => [
						'title' => __('Justified', 'elementor'),
						'icon' => 'fa fa-align-justify',
					],
				],
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .wpapg-box .labele' => 'text-align: {{VALUE}};',
				],
			]
		);

		$this->add_responsive_control(
			'form_label_spacing',
			[
				'label' => __('Spacing', 'elementor'),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .wpapg-box .labele' => 'margin-bottom: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_form_desc_style',
			[
				'label' => __('Description', 'elementor'),
				'tab' => Controls_Manager::TAB_STYLE,
				// 'condition' => [
				// 	'form_page!' => '0',
				// ],
			]
		);

		$this->add_control(
			'form_desc_text_color',
			[
				'label' => __('Text Color', 'elementor'),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .wpapg-box .wpapg-input-box .desc' => 'color: {{VALUE}} !important;',
				]
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'form_desc_typography',
				'selector' => '{{WRAPPER}} .wpapg-box .wpapg-input-box .desc'
			]
		);

		$this->add_control(
			'form_desc_align',
			[
				'label' => __('Alignment', 'elementor'),
				'type' => Controls_Manager::CHOOSE,
				'options' => [
					'left' => [
						'title' => __('Left', 'elementor'),
						'icon' => 'fa fa-align-left',
					],
					'center' => [
						'title' => __('Center', 'elementor'),
						'icon' => 'fa fa-align-center',
					],
					'right' => [
						'title' => __('Right', 'elementor'),
						'icon' => 'fa fa-align-right',
					],
					'justify' => [
						'title' => __('Justified', 'elementor'),
						'icon' => 'fa fa-align-justify',
					],
				],
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .wpapg-box .wpapg-input-box .desc' => 'text-align: {{VALUE}};',
				],
			]
		);

		$this->add_responsive_control(
			'form_desc_spacing',
			[
				'label' => __('Spacing', 'elementor'),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .wpapg-box .wpapg-input-box .desc' => 'margin-top: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_form_submit_style',
			[
				'label' => __('Submit Button', 'elementor'),
				'tab' => Controls_Manager::TAB_STYLE,
				// 'condition' => [
				// 	'form_page!' => '0',
				// ],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'form_submit_typography',
				'selector' => '{{WRAPPER}} .wpapg-box .wpapg-submit-box button'
			]
		);

		$this->start_controls_tabs('tabs_form_submit_button_style');

		$this->start_controls_tab(
			'tab_form_submit_button_normal',
			[
				'label' => __('Normal', 'elementor'),
			]
		);

		$this->add_control(
			'form_submit_text_color',
			[
				'label' => __('Text Color', 'elementor'),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .wpapg-box .wpapg-submit-box button' => 'color: {{VALUE}} !important;',
					'{{WRAPPER}} .wpapg-result-action a button' => 'color: {{VALUE}} !important;'
				]

			]
		);

		$this->add_control(
			'form_submit_background_color',
			[
				'label' => __('Background Color', 'elementor'),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .wpapg-box .wpapg-submit-box button' => 'background-color: {{VALUE}} !important;',
					'{{WRAPPER}} .wpapg-result-action a button' => 'background-color: {{VALUE}} !important;',
				],
				'separator' => 'before',
			]
		);

		$this->add_control(
			'form_submit_border_color',
			[
				'label' => __('Border Color', 'elementor'),
				'type' => Controls_Manager::COLOR,
				// 'condition' => [
				// 	'border_border!' => '',
				// ],
				'selectors' => [
					'{{WRAPPER}} .wpapg-box .wpapg-submit-box button' => 'border-color: {{VALUE}} !important;',
					'{{WRAPPER}} .wpapg-result-action a button' => 'border-color: {{VALUE}} !important;',
				],
				'separator' => 'before',
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab(
			'tab_form_submit_button_hover',
			[
				'label' => __('Hover', 'elementor'),
			]
		);

		$this->add_control(
			'form_submit_text_color_hover',
			[
				'label' => __('Text Color', 'elementor'),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .wpapg-box .wpapg-submit-box button:hover' => 'color: {{VALUE}} !important;',
					'{{WRAPPER}} .wpapg-result-action a button:hover' => 'color: {{VALUE}} !important;'
				]

			]
		);

		$this->add_control(
			'form_submit_background_color_hover',
			[
				'label' => __('Background Color', 'elementor'),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .wpapg-box .wpapg-submit-box button:hover' => 'background-color: {{VALUE}} !important;',
					'{{WRAPPER}} .wpapg-result-action a button:hover' => 'background-color: {{VALUE}} !important;'
				],
				'separator' => 'before',
			]
		);

		$this->add_control(
			'form_submit_border_color_hover',
			[
				'label' => __('Border Color', 'elementor'),
				'type' => Controls_Manager::COLOR,
				// 'condition' => [
				// 	'border_border!' => '',
				// ],
				'selectors' => [
					'{{WRAPPER}} .wpapg-box .wpapg-submit-box button:hover' => 'border-color: {{VALUE}} !important;',
					'{{WRAPPER}} .wpapg-result-action a button:hover' => 'border-color: {{VALUE}} !important;'
				],
				'separator' => 'before',
			]
		);

		$this->add_control(
			'form_submit_hover_animation',
			[
				'label' => __('Hover Animation', 'elementor'),
				'type' => Controls_Manager::HOVER_ANIMATION,
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->add_control(
			'form_submit_border_width',
			[
				'label' => __('Border  Width', 'elementor'),
				'type' => Controls_Manager::DIMENSIONS,
				'placeholder' => '0',
				'size_units' => ['px'],
				'selectors' => [
					'{{WRAPPER}} .wpapg-box .wpapg-submit-box button' => 'border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}} !important;',
					'{{WRAPPER}} .wpapg-result-action a button' => 'border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}} !important;',
				],
			]
		);

		$this->add_control(
			'form_submit_border_radius',
			[
				'label' => __('Border Radius', 'elementor'),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => ['px', '%'],
				'selectors' => [
					'{{WRAPPER}} .wpapg-box .wpapg-submit-box button' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}} !important;',
					'{{WRAPPER}} .wpapg-result-action a button' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}} !important;',
				],
			]
		);

		$this->add_control(
			'form_submit_padding',
			[
				'label' => __('Padding', 'elementor'),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => ['px'],
				'selectors' => [
					'{{WRAPPER}} .wpapg-box .wpapg-submit-box button' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}} !important;',
					'{{WRAPPER}} .wpapg-result-action a button' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}} !important;',
				],
			]
		);

		$this->end_controls_section();
	}

	protected function render()
	{
		global $post;

		$selector = $this->get_id();
		$settings = $this->get_settings();

		$page_id = isset($_GET['lp']) && $_GET['lp'] ? intval($_GET['lp']) : false;
		$page_id = isset($settings['form_page']) && $settings['form_page'] ? intval($settings['form_page']) : $page_id;
		$style = array(
			'style' => $settings['form_design'] ? $settings['form_design'] : false,
			'hover_animation' => $settings['form_submit_hover_animation'] ? $settings['form_submit_hover_animation'] : false,
		);

		$form = wpapg_shortcode_form($page_id, $style);
		echo $form;
	}

	protected function content_template()
	{
	}
}

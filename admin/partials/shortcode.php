<?php
$fields = get_post_meta($post->ID, 'wpapg_shortcode', true);
$enable = get_post_meta($post->ID, 'wpapg_shortcode_enable', true);?>
<div class="wpapg">
    <input type="hidden" name="wpapgnonce" value="<?php echo wp_create_nonce('wpapgnonce'); ?>">
    <div class="wpapg-shortcode-enable">
        <label class="switch">
            <input type="hidden" value="0" name="wpapg_shortcode_enable">
            <input type="checkbox" value="1" name="wpapg_shortcode_enable" id="wpapg_shortcode_onoff" <?php if($enable == 1){ echo'checked'; }?>>
            <span class="slider"></span>
        </label>&nbsp;&nbsp; <strong>Slide ON to enable shortcode</strong>
    </div>
    <table class="wpapg-input-box wp-list-table widefat fixed striped" <?php if($enable != 1 ){ echo 'style="display: none"'; }; ?>>
        <thead>
            <tr class="wpapg-shortcoe-field">
                <td style="width:150px;position:relative">
                    Shortcode
                </td>
                <td style="width:150px;position:relative">
                    Label
                </td>
                <td style="width:110px;position:relative">
                    Type
                </td>
                <td>Default Value</td>
                <td style="width:30px">
                </td>
            </tr>
        </thead>
        <tbody class="wpapgshortcode">
            <?php if( $fields ): ?>
                <?php foreach( (array) $fields as $key=>$val ): ?>
                    <?php

                    $type = isset($val['type']) ? $val['type'] : 'text';
                    ?>
                    <tr class="wpapg-shortcoe-field">
                        <td style="width:150px;position:relative">
                            <input type="text" name="codes[]" placeholder="{{shortcode}}" value="<?php echo wpapg_shortcode_out($key); ?>">
                            <span class="wpapg-copy" onclick="wpapgCopy(this);">COPY</span>
                        </td>
                        <td style="width:150px">
                            <input type="text" name="labels[]" value="<?php echo $val['label']; ?>" placeholder="Label">
                        </td>
                        <td style="width:110px">
                            <select name="types[]" onchange="wpapgSelectType(this);" style="width:100%">
                                <option value="text" <?php if( $type == 'text' ){echo 'selected="selected"';}?>>Text</option>
                                <option value="image" <?php if( $type == 'image' ){echo 'selected="selected"';}?>>Image Upload</option>
                                <option value="link_affiliate" <?php if( $type == 'link_affiliate' ){echo 'selected="selected"';}?>>Link Affiliate</option>
                                <option value="fb_pixel_id" <?php if( $type == 'fb_pixel_id' ){echo 'selected="selected"';}?>>FB Pixel ID</option>
                            </select>
                        </td>
                        <td>
                            <?php if( $type == 'image' ): ?>
                                <input class="wpapgvalue" type="text" name="values[]" placeholder="Default Shortcode Value" value="<?php echo $val['value']; ?>" style="width: 70%">
                                <button class="button wpapgupload" type="button" onclick="wpapgUploader(this)">Upload</button>
                            <?php else : ?>
                                <input class="wpapgvalue" type="text" name="values[]" placeholder="Default Shortcode Value" value="<?php echo $val['value']; ?>">
                            <?php endif; ?>
                        </td>
                        <td style="width:30px">
                            <div style="text-align:center;height: 30px;line-height: 30px;">
                                <span class="dashicons dashicons-trash wpapg-shortcoe-field-remove" style="cursor:pointer;margin-top: 9px;"></span>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php endif; ?>
        </tbody>
        <tfoot>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td style="text-align:right;width: 100%">
                    <button class="button add-more" type="button">Add field</button>
                </td>
                <td></td>
            <tr>
        </tfoot>
    </table>

    <script type="text/javascript">
        jQuery(document).ready(function($) {

            jQuery(".add-more").click(function(){
                let field = jQuery('.wpapg-input-box').find('tr.wpapgfield').length;

                let html = '<tr class="wpapg-shortcoe-field wpapgfield">';
                html += '<td style="width:150px"><input type="text" name="codes[]" placeholder="{{shortcode}}"><span class="wpapg-copy" onclick="wpapgCopy(this)">COPY</span></td>';
                html += '<td style="width:150px"><input type="text" name="labels[]" placeholder="Label"></td>';
                html += '<td style="width:110px"><select name="types[]" style="width:100%" onchange="wpapgSelectType(this);"><option value="text">Text</option><option value="image">Image Upload</option><option value="link_affiliate">Link Affiliate</option><option value="fb_pixel_id">FB Pixel ID</option></select></td>'
                html += '<td><input class="wpapgvalue" type="text" name="values[]" placeholder="Default Shortcode Value"></td>';
                html += '<td style="width:30px">';
                html += '<div style="text-align:center;height: 30px;line-height: 30px;">';
                html += '<span class="dashicons dashicons-trash wpapg-shortcoe-field-remove" style="cursor:pointer;margin-top: 9px;"></span>';
                html += '</div></td>';
                html += '</tr>';
                jQuery('tbody.wpapgshortcode').append(html);
            });

            jQuery("body").on("click",".wpapg-shortcoe-field-remove",function(){
                jQuery(this).parents(".wpapg-shortcoe-field").remove();
            });

            jQuery('#wpapg_shortcode_onoff').on('change', function(){
                if( this.checked ){
                    jQuery('.wpapg-input-box').show();
                }else{
                    jQuery('.wpapg-input-box').hide();
                }
            })

        });

        function wpapgCopy(ini){
            let input = jQuery(ini).parent().find('input');
            input.select();
            document.execCommand('copy');
        }

        function wpapgSelectType(ini){
            let val = jQuery(ini).val();
            let tr = jQuery(ini).parent().parent();
            let valueField = tr.find('.wpapgvalue');
            let tdValueField = valueField.parent();
            if( val == 'image'){
                valueField.css('width', '70%');
                tdValueField.append('<button class="button wpapgupload" type="button" onclick="wpapgUploader(this)">Upload</button>');
            }else{
                tdValueField.empty();
                tdValueField.append('<input class="wpapgvalue" type="text" name="values[]" placeholder="Default Shortcode Value">')
            }
        }
    </script>
</div>

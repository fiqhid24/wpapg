<?php
$args = array();
$domain = new Wpapg_Addon_Domain_Query($args);
$domains = $domain->results();
$pages = get_pages();
add_thickbox();
?>
<div class="wpapg">
    <table class="wpapg-addon-domain-box wp-list-table widefat fixed striped">
        <thead>
            <tr class="wpapg-shortcoe-field">
                <td style="width:200px">
                    Domain
                </td>
                <td>Page Destination</td>
                <td style="width:200px;position:relative">
                    Status
                </td>
                <td style="width:100px">
                </td>
            </tr>
        </thead>
        <tbody>
            <?php if( $domains ): ?>
                <?php foreach( (array) $domains as $addon ): ?>
                    <tr class="wpapg-shortcoe-field">
                        <td style="width:200px">
                            <a href="http://<?php echo $addon->domain; ?>" target="blank"><?php echo $addon->domain; ?></a>
                        </td>
                        <td style="width:200px;position:relative">
                            <?php
                            if( $addon->page_id != NULL && $page = get_post($addon->page_id)):
                                echo '<a href="'.get_the_permalink($page->ID).'" target="_blank">'.$page->post_title.'</a>';
                            else:
                                echo 'Homepage';
                            endif;
                            ?>
                        </td>
                        <td style="position:relative">
                            <?php
                            if( $addon->status == 'connected' ):
                                echo '<span style="background: green;color:#fff;padding: 1px 5px 5px;border-radius:8px;">'.$addon->status.'</span>';
                            else:
                                echo $addon->status;
                            endif;
                            ?>
                        </td>
                        <td style="width:100px">
                            <div style="text-align:center;height: 30px;line-height: 30px;">
                                <span class="dashicons dashicons-update-alt wpapg-shortcoe-field-check" style="cursor:pointer;margin-top: 0px;" data="<?php echo $addon->domain; ?>"></span>
                                <span class="dashicons dashicons-edit wpapg-shortcoe-field-edit" style="cursor:pointer;margin-top: 0px;" data="<?php echo $addon->ID; ?>"></span>
                                <span class="dashicons dashicons-trash wpapg-shortcoe-field-remove <?php echo $addon->ID; ?>" style="cursor:pointer;margin-top: 0px;" data="<?php echo $addon->ID; ?>"></span>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php else: ?>
                <tr class="wpapg-shortcoe-field">
                    <td style="width:200px">
                    </td>
                    <td style="width:200px;position:relative">
                        No Data Found
                    </td>
                    <td>
                    </td>
                    <td style="width:30px">
                    </td>
                </tr>
            <?php endif; ?>
        </tbody>
        <tfoot>
            <tr>
                <td style="width: 100%">
                    <a href="#TB_inline?&width=600&height=250&inlineId=domainbox" class="thickbox">
                        <button class="button button-primary" type="button" name="submit">Add New Domain</button>
                    </a>
                </td>
                <td></td>
                <td></td>
                <td></td>
            <tr>
        </tfoot>
    </table>
    <div id="domainbox" style="display:none;">
        <div id="tbdomain" class="wpapg-domain">
            <p>
                <label>Domain</label>
                <input type="text" style="width: 100%" id="wpapgdomain" name="wpapgdomain" placeholder="ex: example.com, www.example.com or sub.example.com">
            </p>
            <p>
                <label>Page</label>
                <select id="wpapgpageid" name="wpapgpageid" style="width: 100%">
                    <option value="">Homepage</option>
                    <?php foreach( (array)$pages as $page ): ?>
                        <option value="<?php echo $page->ID; ?>"><?php echo $page->post_title; ?></option>
                    <?php endforeach; ?>
                </select>
            </p>
            <p>
                <input type="hidden" id="wpapgid" name="wpapgid" value="">
                <button class="button button-primary wpapg-submit" type="button">Save Domain</button>
                <div id="wpapgprocess" style="text-align:center;display:none">Processing ......</div>
            </p>
        </div>
    </div>
    <script type="text/javascript">

        jQuery(document).ready(function() {

            jQuery('.wpapg-copy').click(function(){
                let input = jQuery(this).parent().find('input');
                input.select();
                document.execCommand('copy');
            })

            jQuery(".add-more").click(function(){
                let html = '<tr class="wpapg-shortcoe-field">';
                html += '<td style="width:200px"><input type="text" name="label[]" placeholder="Label"></td>';
                html += '<td style="width:200px"><input type="text" name="code[]" placeholder="{{shortcode}}"></td>';
                html += '<td><input type="text" name="value[]" placeholder="Default Shortcode Value"></td>';
                html += '<td style="width:30px">';
                html += '<div style="text-align:center;height: 30px;line-height: 30px;">';
                html += '<span class="dashicons dashicons-trash wpapg-shortcoe-field-remove" style="cursor:pointer;margin-top: 9px;"></span>';
                html += '</div></td>';
                html += '</tr>';
                console.log(html);
                jQuery('tbody').append(html);
            });

            jQuery("body").on("click",".wpapg-shortcoe-field-remove",function(){
                let domain_id = jQuery(this).attr('data');
                let r = confirm("Are You sure!");
                if( r == true ){
                    jQuery.ajax({
                        type: 'POST',
                        url : '<?php echo admin_url('admin-ajax.php'); ?>',
                        data: {
                            action: 'addon_domain',
                            id: domain_id,
                            deleted: 1,
                            nonce: jQuery('#noncenonce').val(),
                        },
                        success: function(result){
                            if( result == 'success' ){
                                jQuery('.wpapg-shortcoe-field-remove.'+domain_id).parents(".wpapg-shortcoe-field").remove();
                            }else{
                                alert(result);
                            }
                        }
                    })
                }
            });

            jQuery("body").on("click",".wpapg-shortcoe-field-edit",function(){
                let domain_id = jQuery(this).attr('data');
                jQuery.ajax({
                    type: 'GET',
                    url : '<?php echo admin_url('admin-ajax.php'); ?>',
                    dataType: 'json',
                    data: {
                        action: 'get_addon_domain',
                        domain_id: domain_id,
                        nonce: jQuery('#noncenonce').val(),
                    },
                    success: function(result){
                        jQuery('#wpapgdomain').val(result.domain).attr('readonly', true);
                        jQuery('#wpapgid').val(result.ID);
                        jQuery('#wpapgpageid').val(result.page_id);
                        tb_show("Update Domain", "#TB_inline?&width=600&height=250&inlineId=domainbox");
                    }
                });
            });

            jQuery("body").on("click",".wpapg-shortcoe-field-check",function(){
                jQuery(this).addClass('spin');
                let domain = jQuery(this).attr('data');

                jQuery.ajax({
                    type: 'GET',
                    url : '<?php echo admin_url('admin-ajax.php'); ?>',
                    data: {
                        action: 'check_addon_domain',
                        domain: domain,
                        nonce: jQuery('#noncenonce').val(),
                    },
                    success: function(result){
                        if( result == 'connected' ){
                            window.location.href = '<?php echo admin_url(); ?>admin.php?page=wpapg&tab=addon-domain'
                        }else{
                            jQuery('.wpapg-shortcoe-field-check').removeClass('spin');
                            alert(result);
                        }
                    }
                })
            });

            jQuery("body").on("click",".wpapg-submit",function(){
                jQuery('.wpapg-submit').toggle();
                jQuery('#wpapgprocess').show();
                let domain = jQuery('#wpapgdomain').val();
                let page_id = jQuery('#wpapgpageid').val();

                let regex = new RegExp('^([a-zA-Z0-9][a-zA-Z0-9-_]*\.)*[a-zA-Z0-9]*[a-zA-Z0-9-_]*[[a-zA-Z0-9]+$');

                if( regex.test(domain) ){
                    jQuery.ajax({
                        type: 'POST',
                        url : '<?php echo admin_url('admin-ajax.php'); ?>',
                        data: {
                            action: 'addon_domain',
                            id: jQuery('#wpapgid').val(),
                            domain: domain,
                            page_id: page_id,
                            nonce: jQuery('#noncenonce').val(),
                        },
                        success: function(result){
                            if( result == 'success' ){
                                window.location.href = '<?php echo admin_url(); ?>admin.php?page=wpapg&tab=addon-domain'
                            }else{
                                jQuery('.wpapg-submit').toggle();
                                jQuery('#wpapgprocess').hide();
                                alert(result);
                            }
                        }
                    })
                }else{
                    jQuery('.wpapg-submit').toggle();
                    jQuery('#wpapgprocess').hide();
                    alert('Invalid domain name');
                }

                // if( !domain ){
                //     alert('Doamin cant be empty');
                // }else{
                //     jQuery.ajax({
                //         url : '</?php echo admin_url('admin-ajax.php'); ?>',
                //         data: {
                //             action: 'addon_domain',
                //             domain: domain,
                //             page_id: page_id,
                //         },
                //         success: function(result){
                //             alert(1);
                //         }
                //     })
                // }
            });

        });


    </script>
</div>

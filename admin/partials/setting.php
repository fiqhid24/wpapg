<table class="form-table">
    <tbody>
        <!---<tr>
            <th scope="row">
                <label for="default_role">Rest Api Auth Token</label>
            </th>
            <td>
                <input type="text" id="wpapgApiAuth" name="api_auth" value="<//?php echo wpapg_get_option('api_auth'); ?>" class="regular-text" readonly><button type="button" class="button" id="wpapgGenerateAuth">Generate</button>
            </td>
        </tr>--->
        <tr>
            <th scope="row">
                <label for="default_role">Blacklist Subdomain</label>
            </th>
            <td>
                <p>Sparate subdomain by comma</p>
                <p>Example: test,test1,test2</p>
                <textarea name="blacklist_subdomain" rows="5" cols="30"
                    class="large-text"><?php echo wpapg_get_option('blacklist_subdomain'); ?></textarea>
            </td>
        </tr>
        <tr>
            <th scope="row">
                <label for="default_role">Limit Subdomain</label>
            </th>
            <td>
                Min<input type="number" name="limit_min_subdomain" min="1" max="100"
                    value="<?php echo wpapg_get_option('limit_min_subdomain', 5); ?>"> Character &nbsp;&nbsp; Max<input
                    type="number" name="limit_max_subdomain" min="1" max="100"
                    value="<?php echo wpapg_get_option('limit_max_subdomain', 20); ?>"> Character
            </td>
        </tr>
        <tr>
            <th scope="row">
                <label for="default_role">&nbsp;</label>
            </th>
            <td>
                <input type="hidden" name="wpapg_key" value="options" />
                <input type="submit" name="submit" class="button button-primary" value="Save Changes">
            </td>
        </tr>
    </tbody>
</table>
<script type="text/javascript">
jQuery(document).ready(function() {
    jQuery('#wpapgGenerateAuth').on('click', function() {
        let key = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2,
            15);
        jQuery('#wpapgApiAuth').val(key);
    })
})
</script>
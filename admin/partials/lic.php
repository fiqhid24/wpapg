<div class="wrap">
    <h2><?php _e('Affiliate Page Generator', 'wpapg'); ?></h2>
    <?php if( $this->check ): ?>
        <div class="notice notice-warning is-dismissible">
            <p><?php echo $this->check; ?></p>
        </div>
    <?php endif; ?>
    <?php if( $curl_check ): ?>
        <div class="notice notice-warning">
            <p><?php _e('Modul curl tidak active, silahkan kontak provider hosting Anda untuk mengaktifkan Curl !', 'wpapg'); ?></p>
        </div>
        <?php return; ?>
    <?php endif; ?>

    <form action="" method="post" enctype="multipart/form-data">

        <table class="form-table">
            <tbody>
                <tr>
                    <th scope="row">
                        <label for="default_role">Email</label>
                    </th>
                    <td>
                        <input type="email" name="wpapgemail" value=""  class="regular-text" required/>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        <label for="default_role">License Code</label>
                    </th>
                    <td>
                        <input type="text" name="wpapgcode" value=""  class="regular-text" required/>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        <label for="default_role">&nbsp;</label>
                    </th>
                    <td>
                        <input type="hidden" name="wpapg_key" value="activate"/>
                        <input type="submit" name="submit" class="button button-primary" value="Aktifkan Lisensi">
                    </td>
                </tr>
            </tbody>
        </table>
        <?php wp_nonce_field('wpapg_nonce', 'noncenonce'); ?>
    </form>
</div>

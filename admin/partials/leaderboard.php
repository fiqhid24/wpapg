<?php
$args = array(
    'page_id' => $post->ID,
    'page' => isset($_GET['paged']) && $_GET['paged'] ? $_GET['paged'] : 1,
    'limit' => 10,
);
$q = new Wpapg_Leaderboard_Query($args);

$fields = $q->get_list();
$found = $q->found();
$total_page = ceil($found / $args['limit']);
$next = $args['page'] + 1;
$prev = $args['page'] - 1;

$enable = get_post_meta($post->ID, 'wpapg_leaderboard_enable', true);?>
<div class="wpapg">
    <div class="wpapg-shortcode-enable">
        <label class="switch">
            <input type="hidden" value="0" name="wpapg_leaderboard_enable">
            <input type="checkbox" value="1" name="wpapg_leaderboard_enable" id="wpapg_leaderboard_onoff" <?php if($enable == 1){ echo'checked'; }?>>
            <span class="slider"></span>
        </label>&nbsp;&nbsp; <strong>Slide ON to enable Leaderboard Builder</strong>
    </div>
    <h3 <?php if($enable != 1 ){ echo 'style="display: none"'; }; ?>>Leaderboard Shortcode</h3>
    <table class="wpapg-leaderboard-shortcode wp-list-table widefat fixed striped" <?php if($enable != 1 ){ echo 'style="display: none"'; }; ?>>
        <?php $rank = 1; while( $rank < 11 ): ?>
            <tr>
                <td style="width:30px;text-align:center;font-weight: bold;">#<?php echo $rank; ?></td>
                <td><input type="text" style="width: 100%;text-align:center" readonly value="{{rank_<?php echo $rank; ?>_name}}"/></td>
                <td><input type="text" style="width: 100%;text-align:center" readonly value="{{rank_<?php echo $rank; ?>_photo}}"/></td>
                <td><input type="text" style="width: 100%;text-align:center" readonly value="{{rank_<?php echo $rank; ?>_value}}"/></td>
            </tr>
        <?php $rank++; endwhile; ?>
    </table>
    <h3 <?php if($enable != 1 ){ echo 'style="display: none"'; }; ?>>Leaderboard Participant</h3>
    <?php if( $found > $args['limit'] ): ?>
        <div class="tablenav-pages" style="text-align:center;margin-bottom:10px;margin-top:20px;">
            <span class="pagination-links">
                <?php if( $args['page'] > 1 ): ?>
                    <a class="next-page button" href="<?php echo admin_url(); ?>post.php?post=<?php echo get_the_ID(); ?>&action=edit&paged=<?php echo $prev; ?>">
                        <span aria-hidden="true"><</span>
                    </a>
                <?php else : ?>
                    <span class="tablenav-pages-navspan button disabled" aria-hidden="true"><</span>
                <?php endif; ?>
                <span id="table-paging" class="paging-input">
                    <span class="tablenav-paging-text"><?php echo $args['page']; ?> of <span class="total-pages"><?php echo $total_page; ?></span></span>
                </span>
                <?php if( $total_page > $args['page'] ): ?>
                    <a class="next-page button" href="<?php echo admin_url(); ?>post.php?post=<?php echo get_the_ID(); ?>&action=edit&paged=<?php echo $next; ?>">
                        <span aria-hidden="true">›</span>
                    </a>
                <?php else : ?>
                    <span class="tablenav-pages-navspan button disabled" aria-hidden="true">></span>
                <?php endif; ?>
            </span>
        </div>
    <?php endif; ?>
    <table class="wpapg-input-boxx wp-list-table widefat fixed striped" <?php if($enable != 1 ){ echo 'style="display: none"'; }; ?>>
        <thead>
            <tr class="wpapg-shortcoe-field">
                <td style="position:relative">
                    Full Name
                </td>
                <td style="position:relative">
                    Team Up Name
                </td>
                <td style="position:relative">
                    Photo
                </td>
                <td style="width:100px">Value</td>
                <td style="width:30px">
                </td>
            </tr>
        </thead>
        <tbody class="wpapgleaderboard">
            <?php if( $fields ): ?>
                <?php foreach( (array) $fields as $f ): ?>
                    <?php

                    $type = isset($val['type']) ? $val['type'] : 'text';
                    ?>
                    <tr class="wpapg-leaderboard-field">
                        <td>
                            <input type="text" style="width:100%" name="leaderboard_full_names[]" value="<?php echo $f->full_name; ?>">
                        </td>
                        <td>
                            <?php
                            $teamup_name = $f->teamup_name == $f->full_name ? '' : $f->teamup_name;
                            ?>
                            <input type="text" style="width:100%" name="leaderboard_teamup_names[]" value="<?php echo $teamup_name; ?>">
                        </td>
                        <td>
                            <input style="width:70%" type="text" name="leaderboard_photos[]" value="<?php echo $f->photo; ?>">
                            <button class="button wpapgupload" type="button" onclick="wpapgUploader(this)"><span class="dashicons dashicons-upload"></span></button>
                        </td>
                        <td style="width:100px">
                            <input style="width:100%" type="number" min="0" name="leaderboard_values[]" value="<?php echo $f->value; ?>">
                        </td>
                        <td style="width:30px">
                            <div style="text-align:center;height: 30px;line-height: 30px;">
                                <span class="dashicons dashicons-trash wpapg-leaderboard-field-remove" style="cursor:pointer;margin-top: 9px;"></span>
                                <input type="hidden" name="leaderboard_ids[]" value="<?php echo $f->ID; ?>"/>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php endif; ?>
        </tbody>
        <tfoot>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td style="text-align:right;width: 100%">
                    <button class="button add-moree" type="button">Add New Participant</button>
                </td>
                <td></td>
            <tr>
        </tfoot>
    </table>
    <?php if( $found > $args['limit'] ): ?>
        <div class="tablenav-pages" style="text-align:center;margin-top:10px;">
            <span class="pagination-links">
                <?php if( $args['page'] > 1 ): ?>
                    <a class="next-page button" href="<?php echo admin_url(); ?>post.php?post=<?php echo get_the_ID(); ?>&action=edit&paged=<?php echo $prev; ?>">
                        <span aria-hidden="true"><</span>
                    </a>
                <?php else : ?>
                    <span class="tablenav-pages-navspan button disabled" aria-hidden="true"><</span>
                <?php endif; ?>
                <span id="table-paging" class="paging-input">
                    <span class="tablenav-paging-text"><?php echo $args['page']; ?> of <span class="total-pages"><?php echo $total_page; ?></span></span>
                </span>
                <?php if( $total_page > $args['page'] ): ?>
                    <a class="next-page button" href="<?php echo admin_url(); ?>post.php?post=<?php echo get_the_ID(); ?>&action=edit&paged=<?php echo $next; ?>">
                        <span aria-hidden="true">›</span>
                    </a>
                <?php else : ?>
                    <span class="tablenav-pages-navspan button disabled" aria-hidden="true">></span>
                <?php endif; ?>
            </span>
        </div>
    <?php endif; ?>

    <script type="text/javascript">
        jQuery(document).ready(function($) {

            $(".add-moree").click(function(){

                let html = '<tr class="wpapg-leaderboard-field wpapgfield">';
                html += '<td><input style="width:100%" type="text" name="leaderboard_full_names[]"></td>';
                html += '<td><input style="width:100%" type="text" name="leaderboard_teamup_names[]"></td>';
                html += '<td><input style="width:70%" type="text" name="leaderboard_photos[]"><button class="button wpapgupload" type="button" onclick="wpapgUploader(this)"><span class="dashicons dashicons-upload"></span></button></td>';
                html += '<td style="width:100px"><input style="width:100%" type="number" min="0" name="leaderboard_values[]"></td>';
                html += '<td style="width:30px">';
                html += '<div style="text-align:center;height: 30px;line-height: 30px;">';
                html += '<span class="dashicons dashicons-trash wpapg-leaderboard-field-remove" style="cursor:pointer;margin-top: 9px;"></span>';
                html += '<input type="hidden" name="leaderboard_ids[]" value="0"/></div></td>';
                html += '</tr>';
                $('tbody.wpapgleaderboard').append(html);
            });

            $("body").on("click",".wpapg-leaderboard-field-remove",function(){
                let id = $(this).parent().find('input').val();

                if( id == 0 ){
                    $(this).parents(".wpapg-leaderboard-field").remove();
                }else{
                    let data = {
                        leaderboard_id:id,
                        action: 'delete_leaderboard',
                        nonce: '<?php echo wp_create_nonce('wpapg_nonce'); ?>',
                    }
                    $.post(ajaxurl,data, function(result){
                        if( result == 1 ){

                        }
                    });
                    $(this).parents(".wpapg-leaderboard-field").remove();
                }
            });

            $('#wpapg_leaderboard_onoff').on('change', function(){
                if( this.checked ){
                    $('.wpapg-input-boxx').show();
                    $('.wpapg-leaderboard-shortcode').show();
                    $('.wpapg h3').show();
                }else{
                    $('.wpapg-input-boxx').hide();
                    $('.wpapg-leaderboard-shortcode').hide();
                    $('.wpapg h3').hide();
                }
            })

        });
    </script>
</div>

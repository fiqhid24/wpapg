<?php add_thickbox(); ?>
<?php wp_enqueue_media(); ?>
<form id="posts-filter" method="GET">

    <p class="search-box">
        <label class="screen-reader-text" for="post-search-input">Search Subdomain:</label>
        <input type="hidden" name="page" value="wpapg-subdomain-list">
        <input type="search" id="post-search-input" name="s" value="">
        <input type="submit" id="search-submit" class="button" value="Search Subdomain">
    </p>
</form>
<form id="posts-filter" method="POST">

    <?php wp_nonce_field('wpapg_nonce', 'noncenonce'); ?>
    <div class="tablenav top">

        <div class="alignleft actions bulkactions">
            <button type="submit" class="button" name="delete_bulk" value="1">Delete Selected Subdomain</button>
            <button type="submit" class="button" name="blocked_bulk" value="1">Blocked Selected Subdomain</button>
            <button type="submit" class="button" name="unblocked_bulk" value="1">Unblocked Selected Subdomain</button>
        </div>
        <br class="clear">
    </div>
    <h2 class="screen-reader-text">Posts list</h2>
    <table class="wp-list-table widefat fixed striped posts">
        <thead>
            <tr>
                <td id="cb" class="manage-column column-cb check-column">
                    <input id="cb-select-all-1" type="checkbox">
                </td>
                <th scope="col" class="manage-column">Sub Domain</th>
                <th scope="col" class="manage-column">User</th>
                <th scope="col" class="manage-column" style="text-align:right">Status</th>
                <th scope="col" class="manage-column" style="text-align:right">&nbsp;</th>
            </tr>
        </thead>

        <tbody id="the-list">
            <?php if( $list ): ?>
                <?php foreach( (array) $list as $val ): ?>
                    <?php
                    if( $val->user_id != NULL && $get_user = get_userdata($val->user_id) ){
                        $user = $get_user->first_name ? $get_user->first_name.' '.$get_user->last_name : $get_user->user_login;
                    }else{
                        $user = '-';
                    }
                    $global_shortcode = get_option('wpapg_global_shortcode_'.$val->subdomain);
                    ?>
                    <tr class="iedit">
                        <th scope="row" class="check-column">
                            <input type="checkbox" name="subdomains[]" value="<?php echo $val->ID; ?>">
                        </th>
                        <td>
                            <a target="_blank" href="<?php echo wpapg_get_page_permalink($val->page_id,$val->subdomain);?>">
                                <strong><?php echo wpapg_get_page_permalink($val->page_id,$val->subdomain);?></strong>
                            </a>
                        </td>
                        <td>
                            <a target="_blank" href="<?php echo admin_url(); ?>user-edit.php?user_id=<?php echo $val->user_id; ?>">
                                <strong><?php echo $user; ?></strong>
                            </a>
                        </td>
                        <td style="text-align:right">
                            <strong style="color:<?php if($val->status == 'active'){echo 'green';}else{echo 'red';}; ?>"><?php echo ucfirst($val->status);?></strong>
                        </td>
                        <td style="text-align:right">
                            <button type="button" class="button button-edit" data-gsh='<?php echo json_encode($global_shortcode); ?>' data-sh='<?php echo json_encode($val); ?>' data-gdf='<?php echo json_encode(get_option('wpapg_global_shortcode')); ?>' data-df='<?php echo json_encode(get_post_meta($val->page_id, 'wpapg_shortcode', true)); ?>' data-username="<?php echo $user; ?>">Edit</button>
                            <button type="submit" class="button" name="delete_single" value="<?php echo $val->ID; ?>">Delete</button>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php else: ?>
                <tr>
                    <th scope="row" class="check-column">
                    </th>
                    <td>
                        No Subdomain Found
                    </td>
                    <td style="text-align:right">
                    </td>
                </tr>
            <?php endif; ?>
        </tbody>

        <tfoot>
            <tr>
                <td id="cb" class="manage-column column-cb check-column">
                    <label class="screen-reader-text" for="cb-select-all-1">Select All</label>
                    <input id="cb-select-all-1" type="checkbox">
                </td>
                <th scope="col" class="manage-column">Sub Domain</th>
                <th scope="col" class="manage-column">User</th>
                <th scope="col" class="manage-column">Status</th>
                <th scope="col" class="manage-column" style="text-align:right">&nbsp;</th>
            </tr>
        </tfoot>

    </table>
    <div class="tablenav bottom">
        <?php
        $max_page = $total > $args['limit'] ? $total / $args['limit'] : 1;
        $max_page = ceil($max_page);
        $current_page = isset($_GET['paged']) ? intval($_GET['paged']) : 1;
        $prev = $current_page - 1;
        $next = $current_page + 1;

        if( isset($_GET['s']) ){
            $next .= '&s='.sanitize_text_field($_GET['s']);
        }
        ?>
        <div class="tablenav-pages"><span class="displaying-num">Total <?php echo $total; ?> Items</span>
            <?php if( $current_page <= 1 ): ?>
                <span class="tablenav-pages-navspan button disabled" aria-hidden="true">‹</span>
            <?php else : ?>
                <a class="next-page button" href="<?php echo admin_url(); ?>admin.php?page=wpapg-subdomain-list&paged=<?php echo $prev; ?>">
                    <span class="screen-reader-text">Next page</span><span aria-hidden="true"><</span>
                </a>
            <?php endif; ?>
            <span id="table-paging" class="paging-input"><span class="tablenav-paging-text"><?php echo $args['page']; ?> of <span class="total-pages"><?php echo $max_page; ?></span></span>
            </span>
            <?php if( $next <= $max_page ): ?>
                <a class="next-page button" href="<?php echo admin_url(); ?>admin.php?page=wpapg-subdomain-list&paged=<?php echo $next; ?>">
                    <span class="screen-reader-text">Next page</span><span aria-hidden="true">></span>
                </a>
            <?php else : ?>
                <span class="tablenav-pages-navspan button disabled" aria-hidden="true">></span>
            <?php endif; ?>
        </div>
        <br class="clear">
    </div>

</form>
<div id="shortcodebox" style="display:none;">
    <form class="wpapg-domain" method="POST">
        <?php wp_nonce_field('wpapg_nonce', 'noncenonce'); ?>
        <input type="hidden" id="shtid" name="ID" value="" />
        <input type="hidden" id="shtuserid" name="user_id" value="" />
        <input type="hidden" id="shtpageid" name="page_id" value="" />
        <p>
            <label>Subdomain</label>
            <input type="text" style="width: 100%" id="shtsubdomain" name="subdomain"></select>
        </p>
        <p>
            <label>User</label>
            <select style="width: 100%" id="selectuserid">
            </select>
        </p>
        <p>
            <label>Status</label>
            <select id="shtstatus" name="status" style="width: 100%">
                <option value="active">Active</option>
                <option value="blocked">Blocked</option>
            </select>
        </p>
        <p id="shtdata">
        </p>
        <p>
            <button class="button button-primary" type="submit" name="updatesubdomain" value="1">Save Subdomain</button>
        </p>
    </form>
</div>
<script>
jQuery("body").on("click",".button-edit",function(){
    jQuery('#shtdata').empty();

    let ini = jQuery(this);
    let gdata = JSON.parse(ini.attr('data-gsh'));
    let data = JSON.parse(ini.attr('data-sh'));
    let gdf = JSON.parse(ini.attr('data-gdf'));
    let df = JSON.parse(ini.attr('data-df'));

    let username = ini.attr('data-username');

    if( data.user_id && username ){
        jQuery('#selectuserid').append('<option value="'+data.user_id+'" selected disabled>'+username+'</option>');
    }else{
        jQuery('#selectuserid').append('<option value="" selected disabled>Choose User</option>');
    }

    let dataFields = '';

    Object.keys(gdf).forEach(function(key){
        let gvalval = '';

        if (typeof gdata[key] !== 'undefined' ){
            gvalval = gdata[key].value;
        }
        dataFields += '<p><label>'+gdf[key].label+'</label>';
        if( typeof gdf[key].type !== 'undefined' && gdf[key].type == 'image' ){
            dataFields += '<input class="wpapgvalue" style="width: 100%" type="text" name="global_value['+key+']" value="'+gvalval+'" required/><button class="button wpapgupload" type="button" onclick="wpapgUploader(this)">Upload</button>';
        }else {
            dataFields += '<input style="width: 100%" type="text" name="global_value['+key+']" value="'+gvalval+'" required/>';
        }
        dataFields += '</p>';
    });

    Object.keys(df).forEach(function(key){

        if( typeof gdata[key] !== 'undefined' ){ return; }

        let valval = '';

        if (typeof data.data[key] !== 'undefined' ){
            valval = data.data[key].value;
        }
        dataFields += '<p><label>'+df[key].label+'</label>';
        if( typeof df[key].type !== 'undefined' && df[key].type == 'image' ){
            dataFields += '<input class="wpapgvalue" style="width: 100%" type="text" name="value['+key+']" value="'+valval+'" required/><button class="button wpapgupload" type="button" onclick="wpapgUploader(this)">Upload</button>';
        }else {
            dataFields += '<input style="width: 100%" type="text" name="value['+key+']" value="'+valval+'" required/>';
        }
        dataFields += '</p>';
    });

    jQuery('#shtid').val(data.ID);
    jQuery('#shtuserid').val(data.user_id);
    jQuery('#shtpageid').val(data.page_id);
    jQuery('#shtsubdomain').val(data.subdomain);
    jQuery('#shtstatus').val(data.status);
    jQuery('#shtdata').append(dataFields);
    jQuery('#selectuserid').select2({
        ajax: {
            url: ajaxurl,
            dataType: "json",
            data: function(params){
                var query = {
                    key: params.term,
                    action: 'search_user',
                    page: params.page || 1
                }
                return query;
            },
        }
    });
    jQuery('#selectuserid').on('select2:select', function (e) {
        let data = e.params.data.id;
        jQuery('#shtuserid').val(data);
    });
    tb_show("Update Subdomain", "#TB_inline?&width=600&height=450&inlineId=shortcodebox");
});

let metaImageFrame;
function wpapgUploader(ini){

    let td = jQuery(ini).parent();
    let field = td.find('.wpapgvalue');

    // Sets up the media library frame
    metaImageFrame = wp.media.frames.metaImageFrame = wp.media({
        title: 'Upload Image',
        button: { text:  'Use this file' },
    });

    // Runs when an image is selected.
    metaImageFrame.on('select', function() {

        // Grabs the attachment selection and creates a JSON representation of the model.
        var media_attachment = metaImageFrame.state().get('selection').first().toJSON();

        // Sends the attachment URL to our custom image input field.
        field.val(media_attachment.url);

    });

    // Opens the media library frame.
    metaImageFrame.open();
}
</script>

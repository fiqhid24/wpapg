<table class="form-table">
    <tbody>
        <tr>
            <th scope="row">
                <label for="default_role">Page</label>
            </th>
            <td>
                <select name="salespage" style="width: 100%">
                    <option value="">Homepage</option>
                    <?php foreach( (array)$pages as $page ): ?>
                        <option <?php if(wpapg_get_option('salespage') == $page->ID){ echo 'selected="selected"';}?> value="<?php echo $page->ID; ?>"><?php echo $page->post_title; ?></option>
                    <?php endforeach; ?>
                </select>
            </td>
        </tr>
        <tr>
            <th scope="row">
                <label for="default_role">Blacklist Subdomain</label>
            </th>
            <td>
                <p>Sparate subdomain by comma</p>
                <p>Example: test,test1,test2</p>
                <textarea name="blacklist_subdomain" rows="5" cols="30"  class="large-text" ><?php echo wpapg_get_option('blacklist_subdomain');?></textarea>
            </td>
        </tr>
        <tr>
            <th scope="row">
                <label for="default_role">Limit Subdomain</label>
            </th>
            <td>
                Min<input type="number" name="limit_min_subdomain" min="1" max="100" value="<?php echo wpapg_get_option('limit_min_subdomain', 5);?>"> Character &nbsp;&nbsp; Max<input type="number" name="limit_max_subdomain" min="1" max="100" value="<?php echo wpapg_get_option('limit_max_subdomain', 20); ?>"> Character
            </td>
        </tr>
        <tr>
            <th scope="row">
                <label for="default_role">&nbsp;</label>
            </th>
            <td>
                <input type="hidden" name="wpapg_key" value="options"/>
                <input type="submit" name="submit" class="button button-primary" value="Save Changes">
            </td>
        </tr>
    </tbody>
</table>

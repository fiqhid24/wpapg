<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://www.fiqhidayat.com
 * @since      1.0.0
 *
 * @package    Wpapg
 * @subpackage Wpapg/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Wpapg
 * @subpackage Wpapg/admin
 * @author     Fiq Hidayat <taufik@fiqhidayat.com>
 */
class Wpapg_Admin
{

    private $check;

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $plugin_name    The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string    $plugin_name       The name of this plugin.
     * @param      string    $version    The version of this plugin.
     */
    public function __construct($plugin_name, $version)
    {

        $this->plugin_name = $plugin_name;
        $this->version = time();

        $this->tabs = array(
            'setting' => __('Settings', 'wpapg'),
            'global-shortcode'        => __('Global Shortcode', 'wpapg'),
            'addon-domain'     => __('Addon Domain', 'wpapg'),
            //'api'     => __('Api Documentation', 'wpapg'),
        );

        $this->current_tab = (isset($_GET['tab']) && array_key_exists($_GET['tab'], $this->tabs)) ? trim($_GET['tab']) : 'setting';

        $this->check = Wpapg::lislis();
    }

    /**
     * init called on admin init wp hook
     * @return [type] [description]
     */
    public function init()
    {

        /**
         * upgraded database action
         */
        if (isset($_GET['action']) && $_GET['action'] == 'wpapg-upgrade') :
            require_once WPAPG_DIR . 'includes/class-wpapg-activator.php';
            Wpapg_Activator::create_leaderboard_table();
            update_option('wpapg_upgrade_database', '1.0.0');

            $redirect = admin_url('admin.php?page=wpapg');
            wp_redirect($redirect);
            exit;
        endif;
    }

    /**
     * admin notice
     * @return [type] [description]
     */
    public function notice()
    {

        $installed_plugins = get_plugins();

        $upgrade_database_version = get_option('wpapg_upgrade_database');
        if ($upgrade_database_version != '1.0.0') :
?>
            <div class="notice notice-warning">
                <h4><?php _e('Affiliate Page Generator Upgrade Notice', 'wpapg'); ?></h4>
                <p><?php _e('You use a Affiliate Page Generator plugin wit old database version.', 'wpapg'); ?></p>
                <p><?php _e('To make all feature on Affiliate Page Generator plugin working properly, please upgrade database first ! <a href="' . admin_url('admin.php?action=wpapg-upgrade') . '" class="button">Upgrade Now</a>', 'wpapg'); ?>
                </p>
                <p><?php _e('*All your previous data is safe wih this database upgrade. InshaAllah', 'wpapg'); ?></p>
            </div>
        <?php
        endif;

        if (did_action('elementor/loaded')) :

            $plugin_fse = 'floating-section-elementor/floating-section-elementor.php';

            if (isset($installed_plugins[$plugin_fse])) {
                if (current_user_can('activate_plugins') && !is_plugin_active($plugin_fse)) {
                    $activation_url = wp_nonce_url('plugins.php?action=activate&amp;plugin=' . $plugin_fse . '&amp;plugin_status=all&amp;paged=1&amp;s', 'activate-plugin_' . $plugin_fse);

                    $message = '<p>' . __('Beberapa predesigned elementor template yang tersedia di plugin Affiliate Page Generator memerlukan plugin <a href="https://wordpress.org/plugins/floating-section-elementor/" target="_blank">Floating Section Elementor</a>', 'wpapg') . '</p>';
                    $message .= '<p>' . sprintf('<a href="%s" class="button-primary">%s</a>', $activation_url, __('Activate Floating Section Elementor Now', 'wpapg')) . '</p>';

                    echo '<div class="notice notice-warning"><p>' . $message . '</p></div>';
                }
            } else {
                if (current_user_can('install_plugins')) {
                    $install_url = wp_nonce_url(self_admin_url('update.php?action=install-plugin&plugin=floating-section-elementor'), 'install-plugin_floating-section-elementor');

                    $message = '<p>' . __('Beberapa predesigned elementor template yang tersedia di plugin Affiliate Page Generator memerlukan plugin <a href="https://wordpress.org/plugins/floating-section-elementor/" target="_blank">Floating Section Elementor</a>', 'wpapg') . '</p>';
                    $message .= '<p>' . sprintf('<a href="%s" class="button-primary">%s</a>', $install_url, __('Install Floating Section Elementor Now', 'wpapg')) . '</p>';

                    echo '<div class="notice notice-warning"><p>' . $message . '</p></div>';
                }
            }
        endif;
    }

    /**
     * plugins loaded hook
     * @return [type] [description]
     */
    public function after_setup_theme()
    {

        if (did_action('elementor/loaded')) :
            add_action('elementor/init', [$this, 'elementor_init']);
            //add_action( 'elementor/controls/controls_registered', [$this, 'elementor_control'], 1);
            add_action('elementor/widgets/register', [$this, 'elementor_widget']);
        endif;
    }

    /**
     * elementor init
     * @return [type] [description]
     */
    public function elementor_init()
    {
        Elementor\Plugin::instance()->elements_manager->add_category(
            'apg',
            [
                'title'  => 'Affiliate Page Generator',
                'icon' => 'font'
            ],
            1
        );
    }

    /**
     * elementor widget
     * @return [type] [description]
     */
    public function elementor_control($control_manager)
    {
        require_once WPAPG_DIR . 'admin/elementor-control/image-shortcode.php';

        $control_manager->register_control('image_shortcode', new \Elementor\Wpapg_Control_Image_Shortcode());
    }

    /**
     * elementor widget
     * @return [type] [description]
     */
    public function elementor_widget($widget_manager)
    {
        require_once WPAPG_DIR . 'admin/elementor-widget/frame-image.php';
        require_once WPAPG_DIR . 'admin/elementor-widget/generator-form.php';
        require_once WPAPG_DIR . 'admin/elementor-widget/image.php';
        require_once WPAPG_DIR . 'admin/elementor-widget/whatsapp.php';

        $widget_manager->register(new Wpapg_Frame_Image_Shortcode_Widget());
        $widget_manager->register(new Wpapg_Form_Generator_Widget());
        $widget_manager->register(new Wpapg_Image_Shortcode_Widget());
        $widget_manager->register(new Wpapg_Whatsapp_Shortcode_Widget());
    }

    /**
     * Register the stylesheets for the admin area.
     *
     * @since    1.0.0
     */
    public function enqueue_styles()
    {

        wp_enqueue_style('select2', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css', array(), NULL, 'all');
        wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/wpapg-admin.css', array(), $this->version, 'all');
    }

    /**
     * Register the JavaScript for the admin area.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts()
    {

        wp_enqueue_script('select2', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js', array('jquery'), NULL, false);
        wp_enqueue_media();
        wp_enqueue_script($this->plugin_name, plugin_dir_url(__FILE__) . 'js/wpapg-admin.js', array('jquery'), $this->version, false);
    }

    public function footer()
    {
        global $pagenow;

        if ($this->check != 'active') return;

        if ($pagenow == 'post-new.php' && isset($_GET['post_type']) && $_GET['post_type'] == 'page' || $pagenow == 'post.php' && get_post_type() == 'page') :
            if (did_action('elementor/loaded')) :
            //Wpapg_Elementor_Template::content();
            endif;
        endif;
    }

    /**
     * ajax import template
     * @return [type] [description]
     */
    public function ajax_import_template()
    {
        $nonce = $_REQUEST['nonce'];
        if (!wp_verify_nonce($nonce, 'wpapg_nonce')) exit;

        $post_id = intval($_POST['post_id']);
        $source = sanitize_text_field($_POST['source']);
        $global = sanitize_text_field($_POST['globals']);
        $meta = sanitize_text_field($_POST['meta']);

        $elementor = new Wpapg_Elementor_Template();
        $imported = $elementor->import_single_template($post_id, $source, $global, $meta);

        if (is_wp_error($imported)) {
            echo 'error';
        } else {
            echo admin_url() . 'post.php?post=' . $post_id . '&action=edit';
        }
        exit;
    }

    /**
     * add metabox
     */
    public function add_post_meta()
    {
        if ($this->check == 'active') {
            add_meta_box(
                'wpapg_shortcode',
                'Shortcode',
                [$this, 'shortcode_metabox'],
                'page',
                'normal',
                'high'
            );

            // add_meta_box(
            //     'wpapg_leaderboard',
            //     'Leaderboard Builder',
            //     [$this, 'leaderboard_metabox'],
            //     'page',
            //     'normal',
            //     'high'
            // );
        }
    }

    /**
     * view shortcode metabox
     * @return [type] [description]
     */
    public function shortcode_metabox()
    {
        global $post;

        include(WPAPG_DIR . 'admin/partials/shortcode.php');
    }

    /**
     * view shortcode metabox
     * @return [type] [description]
     */
    public function leaderboard_metabox()
    {
        global $post;

        $upgrade_database_version = get_option('wpapg_upgrade_database');
        if ($upgrade_database_version != '1.0.0') :
        ?>
            <h4 style="color:#ffb900"><?php _e('Affiliate Page Generator Upgrade Notice', 'wpapg'); ?></h4>
            <p><?php _e('You use a Affiliate Page Generator plugin wit old database version.', 'wpapg'); ?></p>
            <p><?php _e('To make all feature on Affiliate Page Generator plugin working properly, please upgrade database first ! <a href="' . admin_url('admin.php?action=wpapg-upgrade') . '" class="button">Upgrade Now</a>', 'wpapg'); ?>
            </p>
            <p><?php _e('*All your previous data is safe wih this database upgrade. InshaAllah', 'wpapg'); ?></p>
            <?php
        else :
            include(WPAPG_DIR . 'admin/partials/leaderboard.php');
        endif;
    }

    /**
     * wpapg metabox save
     * @param  [type] $post_id [description]
     * @param  [type] $post    [description]
     * @return [type]          [description]
     */
    public function post_meta_save($post_id, $post)
    {

        // return if autosave
        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
            return;
        }

        // Return if the user doesn't have edit permissions.
        if (!current_user_can('edit_post', $post_id)) {
            return $post_id;
        }

        if (isset($_POST['wpapg_shortcode_enable'])) :

            update_post_meta($post_id, 'wpapg_shortcode_enable', intval($_POST['wpapg_shortcode_enable']));
            delete_transient('wpapg_active_pages');

            $shortcode_enable = get_post_meta($post_id, 'wpapg_shortcode_enable', true);

            if ($shortcode_enable == 1) :

                $codes  = isset($_POST['codes']) ? $_POST['codes'] : array();
                $labels = isset($_POST['labels']) ? $_POST['labels'] : array();
                $values = isset($_POST['values']) ? $_POST['values'] : array();
                $types  = isset($_POST['types']) ? $_POST['types'] : array();

                $field = array();
                foreach ((array) $codes as $key => $val) :
                    if (!$val) continue;
                    $field[wpapg_shortcode_in($val)] = array(
                        'label' => sanitize_text_field($labels[$key]),
                        'type'  => sanitize_text_field($types[$key]),
                        'value' => filter_var($values[$key], FILTER_VALIDATE_URL) === FALSE ? sanitize_text_field($values[$key]) : esc_url_raw($values[$key]),
                    );
                endforeach;

                update_post_meta($post_id, 'wpapg_shortcode', $field);

            endif;

        endif;

        if (isset($_POST['wpapg_leaderboard_enable'])) :

            update_post_meta($post_id, 'wpapg_leaderboard_enable', intval($_POST['wpapg_leaderboard_enable']));

            $leaderboard_enable = get_post_meta($post_id, 'wpapg_leaderboard_enable', true);

            if ($leaderboard_enable == 1) :

                $full_names   = isset($_POST['leaderboard_full_names']) ? $_POST['leaderboard_full_names'] : array();
                $teamup_names = isset($_POST['leaderboard_teamup_names']) ? $_POST['leaderboard_teamup_names'] : array();
                $photos       = isset($_POST['leaderboard_photos']) ? $_POST['leaderboard_photos'] : array();
                $values       = isset($_POST['leaderboard_values']) ? $_POST['leaderboard_values'] : array();
                $ids          = isset($_POST['leaderboard_ids']) ? $_POST['leaderboard_ids'] : array();

                $d = array();
                foreach ($full_names as $key => $val) {
                    $d = array(
                        'id'          => isset($ids[$key]) ? intval($ids[$key]) : 0,
                        'full_name'   => sanitize_text_field($val),
                        'teamup_name' => isset($teamup_names[$key]) ? sanitize_text_field($teamup_names[$key]) : '',
                        'photo'       => isset($photos[$key]) ? esc_url($photos[$key]) : '',
                        'value'       => isset($values[$key]) ? intval($values[$key]) : 0,
                    );

                    $leaderboard = new Wpapg_Leaderboard(intval($d['id']));
                    $leaderboard->set_page($post_id);
                    $leaderboard->set_full_name($d['full_name']);
                    $leaderboard->set_teamup_name($d['teamup_name']);
                    $leaderboard->set_photo($d['photo']);
                    $leaderboard->set_value($d['value']);

                    if ($leaderboard->data) :
                        $leaderboard->update();
                    else :
                        $leaderboard->insert();
                    endif;
                }

                $args = array(
                    'page_id' => $post_id,
                );

                $q = new Wpapg_Leaderboard_Query($args);

                $results = $q->get_leaderboard();

                $rank = 1;
                $l = array();
                foreach ((array)$results as $r) :
                    $l['rank_' . $rank . '_name'] = $r->name;
                    $l['rank_' . $rank . '_photo'] = $r->photo;
                    $l['rank_' . $rank . '_value'] = $r->value;
                    $rank++;
                endforeach;

                update_post_meta($post_id, 'wpapg_leaderboard_cache', $l);

            endif;

        endif;
    }

    public function page_columns($columns)
    {
        $columns['wpapg_form_shortcode'] = 'Shortcode Generator Form';

        return $columns;
    }

    /**
     * magae page cusom column
     * @param  [type] $column  [description]
     * @param  [type] $post_id [description]
     * @return [type]          [description]
     */
    public function manage_page_columns($column, $post_id)
    {
        global $post;

        switch ($column):
            case 'wpapg_form_shortcode':
                $en = get_post_meta($post_id, 'wpapg_shortcode_enable', true);
                $sh = get_post_meta($post_id, 'wpapg_shortcode', true);
                if ($en == 1 && $sh) {
                    echo '<strong>[wpapg_form_generator page_id="' . $post_id . '"]</strong>';
                } else {
                    echo '-';
                }
                break;
            default:
                break;
        endswitch;
    }

    /**
     * admin menu
     * @return [type] [description]
     */
    public function admin_menu()
    {

        add_menu_page(
            __('Affiliate Page Generator', 'wpapg'),
            __('Affiliate Page Generator', 'wpapg'),
            'manage_options',
            'wpapg',
            [$this, 'settings']
        );

        if ($this->check == 'active') :
            add_submenu_page(
                'wpapg',
                __('Subdomain List', 'wpapg'),
                __('Subdomain List', 'wpapg'),
                'manage_options',
                'wpapg-subdomain-list',
                [$this, 'subdomain_list']
            );
        endif;
    }

    private function get_site_url()
    {
        global $wpdb;

        $table = $wpdb->prefix . 'options';
        $o = $wpdb->get_row($wpdb->prepare("SELECT * FROM $table WHERE option_name = %d LIMIT 1", 'siteurl'));

        return $o->option_value;
    }

    private function get_home_url()
    {
        global $wpdb;

        $table = $wpdb->prefix . 'options';
        $o = $wpdb->get_row($wpdb->prepare("SELECT * FROM $table WHERE option_name = %d LIMIT 1", 'home'));

        return $o->option_value;
    }

    /**
     * wpapg settings page view
     * @return [type] [description]
     */
    public function settings()
    {
        global $wpapg;

        if (isset($_POST['submit'])) :
            if (check_admin_referer('wpapg_nonce', 'noncenonce') && isset($_POST['wpapg_key'])) :

                if ($_POST['wpapg_key'] == 'options') :
                    $settings = $_POST;
                    unset($settings['wpapg_key']);
                    unset($settings['submit']);
                    unset($settings['noncenonce']);
                    unset($settings['_wp_http_referer']);

                    $options = array(
                        'siteurl' => $this->get_site_url(),
                        'home' => $this->get_home_url(),
                    );
                    foreach ((array) $settings as $key => $val) :
                        $options[$key] = sanitize_text_field($val);
                    endforeach;

                    update_option('wpapg_options', $options);
            ?>
                    <script type="text/javascript">
                        window.location.href = '<?php echo admin_url(); ?>admin.php?page=wpapg&saved=true'
                    </script>
                <?php
                endif;
                if ($_POST['wpapg_key'] == 'global_shortcode') :

                    $codes  = isset($_POST['codes']) ? $_POST['codes'] : array();
                    $labels = isset($_POST['labels']) ? $_POST['labels'] : array();
                    $values = isset($_POST['values']) ? $_POST['values'] : array();
                    $types  = isset($_POST['types']) ? $_POST['types'] : array();

                    $field = array();
                    foreach ((array) $codes as $key => $val) :
                        if (!$val) continue;
                        $field[wpapg_shortcode_in($val)] = array(
                            'label' => sanitize_text_field($labels[$key]),
                            'type'  => sanitize_text_field($types[$key]),
                            'value' => sanitize_text_field($values[$key]),
                        );
                    endforeach;

                    update_option('wpapg_global_shortcode', $field);
                ?>
                    <script type="text/javascript">
                        window.location.href = '<?php echo admin_url(); ?>admin.php?page=wpapg&tab=global-shortcode'
                    </script>
        <?php
                endif;
                if ($_POST['wpapg_key'] == 'activate') :
                    $data = array(
                        'email' => sanitize_email($_POST['wpapgemail']),
                        'code' => sanitize_text_field($_POST['wpapgcode']),
                    );
                    update_option('wpapglislisdata', $data);
                    $this->check = Wpapg::lislis();
                endif;
            endif;

        endif;
        if ($this->check != 'active') {
            $curl_check = Wpapg::check_curl();
            return include(WPAPG_DIR . 'admin/partials/lic.php');
        }
        ?>
        <div class="wrap">
            <h2><?php _e('Affiliate Page Generator', 'wpapg'); ?></h2>

            <h2 class="nav-tab-wrapper wp-clearfix">
                <?php foreach ($this->tabs as $url => $title) : ?>
                    <a href="<?php echo add_query_arg('tab', $url); ?>" class="nav-tab <?php echo ($this->current_tab === $url) ? 'nav-tab-active' : '' ?>">
                        <?php echo $title ?>
                    </a>
                <?php endforeach; ?>
            </h2>

            <form action="" method="post" enctype="multipart/form-data">

                <?php include(WPAPG_DIR . 'admin/partials/' . $this->current_tab . '.php'); ?>
                <?php wp_nonce_field('wpapg_nonce', 'noncenonce'); ?>
            </form>
        </div>
    <?php
    }

    /**
     * subdomain list page
     * @return [type] [description]
     */
    public function subdomain_list()
    {

        if (!empty($_POST) && check_admin_referer('wpapg_nonce', 'noncenonce')) :

            if (wp_verify_nonce($_REQUEST['noncenonce'], 'wpapg_nonce')) :

                if (isset($_POST['delete_single']) && $_POST['delete_single']) :

                    $id = intval($_POST['delete_single']);

                    $subdomain = new Wpapg_Subdomain($id);
                    $subdomain->delete();

                endif;

                if (isset($_POST['delete_bulk']) && $_POST['delete_bulk']) :

                    $subdomains = $_POST['subdomains'];

                    foreach ((array) $subdomains as $key => $val) :
                        $subdomain = new Wpapg_Subdomain(intval($val));
                        $subdomain->delete();
                    endforeach;

                endif;

                if (isset($_POST['blocked_bulk']) && $_POST['blocked_bulk']) :

                    $subdomains = $_POST['subdomains'];

                    foreach ((array) $subdomains as $key => $val) :
                        $subdomain = new Wpapg_Subdomain(intval($val));
                        $subdomain->set_status(false);
                        $subdomain->update();
                    endforeach;

                endif;

                if (isset($_POST['unblocked_bulk']) && $_POST['unblocked_bulk']) :

                    $subdomains = $_POST['subdomains'];

                    foreach ((array) $subdomains as $key => $val) :
                        $subdomain = new Wpapg_Subdomain(intval($val));
                        $subdomain->set_status(true);
                        $subdomain->update();
                    endforeach;

                endif;

                if (isset($_POST['updatesubdomain']) && $_POST['updatesubdomain'] == 1) :

                    $post_global_value = isset($_POST['global_value']) ? $_POST['global_value'] : array();
                    $post_global_defvalue = isset($_POST['global_defvalue']) ? $_POST['global_defvalue'] : array();
                    $global_value = array_merge($post_global_value, $post_global_defvalue);

                    $global_files = array();
                    if (isset($_FILES) && isset($_FILES['global_value'])) :
                        foreach ((array)$_FILES['global_value']['name'] as $key => $val) :
                            if (empty($val)) continue;

                            $global_files[$key] = array(
                                'name'     => $val,
                                'type'     => $_FILES['global_value']['type'][$key],
                                'tmp_name' => $_FILES['global_value']['tmp_name'][$key],
                                'error'    => $_FILES['global_value']['error'][$key],
                                'size'     => $_FILES['global_value']['size'][$key],
                            );
                        endforeach;
                    endif;

                    $global_value = array_merge($global_value, $global_files);

                    $post_value = isset($_POST['value']) ? $_POST['value'] : array();
                    $post_defvalue = isset($_POST['defvalue']) ? $_POST['defvalue'] : array();
                    $value = array_merge($post_value, $post_defvalue);

                    $files = array();
                    if (isset($_FILES) && isset($_FILES['value'])) :
                        foreach ((array)$_FILES['value']['name'] as $key => $val) :
                            if (empty($val)) continue;

                            $global_files[$key] = array(
                                'name'     => $val,
                                'type'     => $_FILES['value']['type'][$key],
                                'tmp_name' => $_FILES['value']['tmp_name'][$key],
                                'error'    => $_FILES['value']['error'][$key],
                                'size'     => $_FILES['value']['size'][$key],
                            );
                        endforeach;
                    endif;

                    $value = array_merge($value, $files);

                    $sht = new Wpapg_Subdomain(intval($_POST['ID']));

                    $status = $_POST['status'] == 'active' ? true : false;

                    $sht->set_status($status);
                    $sht->set_page($sht->page_id);
                    $sht->set_data($value);

                    if (isset($_POST['user_id']) && $_POST['user_id']) :
                        $sht->set_user_id(intval($_POST['user_id']));
                    else :
                        $sht->set_subdomain($_POST['subdomain']);
                    endif;

                    $save = $sht->update();
                    $current_shortcode = wpapg_get_subdomain($save);
                    wpapg_global_shortcode_save($current_shortcode->subdomain, $global_value);

                endif;
            endif;

        endif;

        $args = array(
            'limit' => 20,
            'page' => isset($_GET['paged']) ? intval($_GET['paged']) : 1,
        );

        if (isset($_GET['s'])) :
            $args['s'] = sanitize_text_field($_GET['s']);
        endif;

        $query = new Wpapg_Subdomain_Query($args);

        $list = $query->results();
        $total = $query->found();

    ?>
        <div class="wrap">
            <h2><?php _e('Sub Domain List', 'wpapg'); ?></h2>

            <?php include(WPAPG_DIR . 'admin/partials/subdomain-list.php'); ?>
        </div>
<?php
    }

    public function ajax_search_user()
    {

        $key   = sanitize_text_field($_GET['key']);

        $page = isset($_GET['page']) && $_GET['page'] ? intval($_GET['page']) : 1;
        $paged = $page - 1;

        $offset = $paged >= 1 ? $paged * 20 : 0;

        $args = array(
            'search'         => '*' . $key . '*',
            'search_columns' => array('user_login', 'user_email', 'first_name', 'last_name'),
            'number'         => 20,
            'offset'         => $offset,
        );

        $query     = new WP_User_Query($args);
        $get_users = $query->get_results();
        $users = array();
        if ($get_users) :
            foreach ($get_users as $user) :
                $users[] = array(
                    'id' => $user->ID,
                    'text' => $user->user_login . ' -> ' . $user->first_name . ' ' . $user->last_name,
                );
            endforeach;
        endif;

        $total_page = $query->get_total() / 20;

        $response['results'] = $users;
        $response['pagination'] = array(
            'more' => $total_page > $page ? true : false,
        );

        echo json_encode($response);
        exit;
    }

    /**
     * ajax insert or update addon domain
     * @return [type] [description]
     */
    public function ajax_addon_domain()
    {

        $nonce = $_POST['nonce'];
        if (!wp_verify_nonce($nonce, 'wpapg_nonce')) exit;

        $addon_domain_id = isset($_POST['id']) && $_POST['id'] ? intval($_POST['id']) : false;

        $addon_domain = new Wpapg_Addon_Domain($addon_domain_id);

        if (isset($_POST['domain']) && $_POST['domain']) :
            $addon_domain->set_domain(sanitize_text_field($_POST['domain']));
        endif;

        if (isset($_POST['page_id'])) :
            $addon_domain->set_page(intval($_POST['page_id']));
        endif;

        if ($addon_domain_id) {
            if (isset($_POST['deleted']) && $_POST['deleted']) :
                $saved = $addon_domain->delete();
            else :
                $saved = $addon_domain->update();
            endif;
        } else {
            $saved = $addon_domain->insert();
        }

        if (is_wp_error($saved)) {
            $respons = $saved->get_error_message();
        } elseif ($saved) {
            $respons = 'success';
        } else {
            $respons = 'Error, Please try again';
        }

        echo $respons;
        exit;
    }

    /**
     * ajax check addon domain pointing
     * @return [type] [description]
     */
    public function ajax_check_addon_domain()
    {

        global $wp_version;

        $nonce = $_GET['nonce'];
        if (!wp_verify_nonce($nonce, 'wpapg_nonce')) exit;

        if (!function_exists('curl_version')) {
            echo 'Curl disabled on your server, Please enabled it.';
            exit;
        }

        $url = 'http://' . sanitize_text_field($_GET['domain']) . '/wpapg-addon-domain-check';

        $curl = wp_remote_get(
            $url,
            array(
                'user-agent'  =>  'WordPress/' . $wp_version . '; ' . get_bloginfo('url'),
            )
        );

        if (is_wp_error($curl)) {
            $respons = $curl->get_error_message();
        } else if ($curl['body'] == 'valid') {

            $args = array(
                'domain' => sanitize_text_field($_GET['domain']),
            );

            $d = new Wpapg_Addon_Domain_Query($args);

            $result = $d->results();

            if (isset($result[0])) :

                $addon = new Wpapg_Addon_Domain($result[0]);
                $addon->set_status('connected');
                $update = $addon->update();

                if (is_wp_error($update)) {
                    $respons = $update->get_error_message();
                } else {
                    $respons = 'connected';
                }
            else :
                $respons = 'Unknown domain';
            endif;
        } else {
            $respons = 'Unconnected or maybe on propagation, please check back later';
        }

        echo $respons;
        exit;
    }

    /**
     * ajax check addon domain pointing
     * @return [type] [description]
     */
    public function ajax_get_addon_domain()
    {

        $nonce = $_GET['nonce'];
        if (!wp_verify_nonce($nonce, 'wpapg_nonce')) exit;

        $d = wpapg_get_addon_domain(intval($_GET['domain_id']));

        $d_array = get_object_vars($d);

        echo json_encode($d_array);
        exit;
    }

    /**
     * delete leaderboard ajax
     * @return [type] [description]
     */
    public function ajax_delete_leaderboard()
    {

        $nonce = $_REQUEST['nonce'];
        if (!wp_verify_nonce($nonce, 'wpapg_nonce')) exit;

        $d = new Wpapg_Leaderboard(intval($_POST['leaderboard_id']));

        $d->delete();

        echo 1;
        exit;
    }
}

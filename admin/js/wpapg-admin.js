(function ($) {
	'use strict';

	$(function () {
		setTimeout(function () {
			let wpapgGutenbergButton = $('#wpapg-guttenberg-button').html();

			$('#editor').find('.edit-post-header-toolbar').append(wpapgGutenbergButton);
		}, 1);

		let wpapgClassicButton = $('#wpapg-classic-button').html();
		console.log(wpapgClassicButton);
		console.log('spadadad');

		$("body.post-type-page .wrap h1").append(wpapgClassicButton);

	});



})(jQuery);

let wpapgImageFrame;

function wpapgUploader(ini) {

	let td = jQuery(ini).parent();
	let field = td.find('input');

	// Sets up the media library frame
	wpapgImageFrame = wp.media.frames.wpapgImageFrame = wp.media({
		title: 'Upload Image',
		button: { text: 'Use this file' },
	});

	// Runs when an image is selected.
	wpapgImageFrame.on('select', function () {

		// Grabs the attachment selection and creates a JSON representation of the model.
		var media_attachment = wpapgImageFrame.state().get('selection').first().toJSON();

		// Sends the attachment URL to our custom image input field.
		field.val(media_attachment.url);

	});

	// Opens the media library frame.
	wpapgImageFrame.open();
}
